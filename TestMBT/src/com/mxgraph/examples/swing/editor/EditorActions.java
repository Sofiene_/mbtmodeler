/*
 * Copyright (c) 2001-2012, JGraph Ltd
 */
package com.mxgraph.examples.swing.editor;

import java.awt.Color;
import java.awt.Component;
import java.awt.Frame;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JColorChooser;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JSplitPane;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;

import org.w3c.dom.Document;

import com.focus.enicar2017.DB.DatabaseGUI;
import com.focus.enicar2017.excel.WriteExcel;
import com.focus.enicar2017.input.InputTransition;
import com.focus.enicar2017.testCase.IndiceRep;
import com.focus.enicar2017.testCase.LigneTest;
import com.focus.enicar2017.testCase.Test;
import com.focus.enicar2017.testCase.TestRow;
import com.focus.enicar2017.testCase.VarValeurs;
import com.mxgraph.canvas.mxGraphics2DCanvas;
import com.mxgraph.canvas.mxICanvas;
import com.mxgraph.canvas.mxSvgCanvas;
import com.mxgraph.io.mxCodec;
import com.mxgraph.io.mxGdCodec;
import com.mxgraph.model.mxCell;
import com.mxgraph.model.mxGraphModel;
import com.mxgraph.model.mxIGraphModel;
import com.mxgraph.shape.mxStencilShape;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.swing.mxGraphOutline;
import com.mxgraph.swing.handler.mxConnectionHandler;
import com.mxgraph.swing.util.mxGraphActions;
import com.mxgraph.swing.view.mxCellEditor;
import com.mxgraph.util.mxCellRenderer;
import com.mxgraph.util.mxCellRenderer.CanvasFactory;
import com.mxgraph.util.mxConstants;
import com.mxgraph.util.mxDomUtils;
import com.mxgraph.util.mxResources;
import com.mxgraph.util.mxUtils;
import com.mxgraph.util.mxXmlUtils;
import com.mxgraph.util.png.mxPngEncodeParam;
import com.mxgraph.util.png.mxPngImageEncoder;
import com.mxgraph.util.png.mxPngTextDecoder;
import com.mxgraph.view.mxGraph;

/**
 *
 */
public class EditorActions {
	/**
	 * 
	 * @param e
	 * @return Returns the graph for the given action event.
	 */
	public static final BasicGraphEditor getEditor(ActionEvent e) {
		if (e.getSource() instanceof Component) {
			Component component = (Component) e.getSource();

			while (component != null && !(component instanceof BasicGraphEditor)) {
				component = component.getParent();
			}

			return (BasicGraphEditor) component;
		}

		return null;
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class ToggleRulersItem extends JCheckBoxMenuItem {
		/**
		 * 
		 */
		public ToggleRulersItem(final BasicGraphEditor editor, String name) {
			super(name);
			setSelected(editor.getGraphComponent().getColumnHeader() != null);

			addActionListener(new ActionListener() {
				/**
				 * 
				 */
				public void actionPerformed(ActionEvent e) {
					mxGraphComponent graphComponent = editor.getGraphComponent();

					if (graphComponent.getColumnHeader() != null) {
						graphComponent.setColumnHeader(null);
						graphComponent.setRowHeader(null);
					} else {
						graphComponent.setColumnHeaderView(
								new EditorRuler(graphComponent, EditorRuler.ORIENTATION_HORIZONTAL));
						graphComponent
								.setRowHeaderView(new EditorRuler(graphComponent, EditorRuler.ORIENTATION_VERTICAL));
					}
				}
			});
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class ToggleGridItem extends JCheckBoxMenuItem {
		/**
		 * 
		 */
		public ToggleGridItem(final BasicGraphEditor editor, String name) {
			super(name);
			setSelected(true);

			addActionListener(new ActionListener() {
				/**
				 * 
				 */
				public void actionPerformed(ActionEvent e) {
					mxGraphComponent graphComponent = editor.getGraphComponent();
					mxGraph graph = graphComponent.getGraph();
					boolean enabled = !graph.isGridEnabled();

					graph.setGridEnabled(enabled);
					graphComponent.setGridVisible(enabled);
					graphComponent.repaint();
					setSelected(enabled);
				}
			});
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class ToggleOutlineItem extends JCheckBoxMenuItem {
		/**
		 * 
		 */
		public ToggleOutlineItem(final BasicGraphEditor editor, String name) {
			super(name);
			setSelected(true);

			addActionListener(new ActionListener() {
				/**
				 * 
				 */
				public void actionPerformed(ActionEvent e) {
					final mxGraphOutline outline = editor.getGraphOutline();
					outline.setVisible(!outline.isVisible());
					outline.revalidate();

					SwingUtilities.invokeLater(new Runnable() {
						/*
						 * (non-Javadoc)
						 * 
						 * @see java.lang.Runnable#run()
						 */
						public void run() {
							if (outline.getParent() instanceof JSplitPane) {
								if (outline.isVisible()) {
									((JSplitPane) outline.getParent()).setDividerLocation(editor.getHeight() - 300);
									((JSplitPane) outline.getParent()).setDividerSize(6);
								} else {
									((JSplitPane) outline.getParent()).setDividerSize(0);
								}
							}
						}
					});
				}
			});
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class ExitAction extends AbstractAction {
		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e) {
			BasicGraphEditor editor = getEditor(e);

			if (editor != null) {
				editor.exit();
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class StylesheetAction extends AbstractAction {
		/**
		 * 
		 */
		protected String stylesheet;

		/**
		 * 
		 */
		public StylesheetAction(String stylesheet) {
			this.stylesheet = stylesheet;
		}

		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() instanceof mxGraphComponent) {
				mxGraphComponent graphComponent = (mxGraphComponent) e.getSource();
				mxGraph graph = graphComponent.getGraph();
				mxCodec codec = new mxCodec();
				Document doc = mxUtils.loadDocument(EditorActions.class.getResource(stylesheet).toString());

				if (doc != null) {
					codec.decode(doc.getDocumentElement(), graph.getStylesheet());
					graph.refresh();
				}
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class ZoomPolicyAction extends AbstractAction {
		/**
		 * 
		 */
		protected int zoomPolicy;

		/**
		 * 
		 */
		public ZoomPolicyAction(int zoomPolicy) {
			this.zoomPolicy = zoomPolicy;
		}

		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() instanceof mxGraphComponent) {
				mxGraphComponent graphComponent = (mxGraphComponent) e.getSource();
				graphComponent.setPageVisible(true);
				graphComponent.setZoomPolicy(zoomPolicy);
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class GridStyleAction extends AbstractAction {
		/**
		 * 
		 */
		protected int style;

		/**
		 * 
		 */
		public GridStyleAction(int style) {
			this.style = style;
		}

		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() instanceof mxGraphComponent) {
				mxGraphComponent graphComponent = (mxGraphComponent) e.getSource();
				graphComponent.setGridStyle(style);
				graphComponent.repaint();
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class GridColorAction extends AbstractAction {
		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() instanceof mxGraphComponent) {
				mxGraphComponent graphComponent = (mxGraphComponent) e.getSource();
				Color newColor = JColorChooser.showDialog(graphComponent, mxResources.get("gridColor"),
						graphComponent.getGridColor());

				if (newColor != null) {
					graphComponent.setGridColor(newColor);
					graphComponent.repaint();
				}
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class ScaleAction extends AbstractAction {
		/**
		 * 
		 */
		protected double scale;

		/**
		 * 
		 */
		public ScaleAction(double scale) {
			this.scale = scale;
		}

		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() instanceof mxGraphComponent) {
				mxGraphComponent graphComponent = (mxGraphComponent) e.getSource();
				double scale = this.scale;

				if (scale == 0) {
					String value = (String) JOptionPane.showInputDialog(graphComponent, mxResources.get("value"),
							mxResources.get("scale") + " (%)", JOptionPane.PLAIN_MESSAGE, null, null, "");

					if (value != null) {
						scale = Double.parseDouble(value.replace("%", "")) / 100;
					}
				}

				if (scale > 0) {
					graphComponent.zoomTo(scale, graphComponent.isCenterZoom());
				}
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class PageSetupAction extends AbstractAction {
		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() instanceof mxGraphComponent) {
				mxGraphComponent graphComponent = (mxGraphComponent) e.getSource();
				PrinterJob pj = PrinterJob.getPrinterJob();
				PageFormat format = pj.pageDialog(graphComponent.getPageFormat());

				if (format != null) {
					graphComponent.setPageFormat(format);
					graphComponent.zoomAndCenter();
				}
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class PrintAction extends AbstractAction {
		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() instanceof mxGraphComponent) {
				mxGraphComponent graphComponent = (mxGraphComponent) e.getSource();
				PrinterJob pj = PrinterJob.getPrinterJob();

				if (pj.printDialog()) {
					PageFormat pf = graphComponent.getPageFormat();
					Paper paper = new Paper();
					double margin = 36;
					paper.setImageableArea(margin, margin, paper.getWidth() - margin * 2,
							paper.getHeight() - margin * 2);
					pf.setPaper(paper);
					pj.setPrintable(graphComponent, pf);

					try {
						pj.print();
					} catch (PrinterException e2) {
						System.out.println(e2);
					}
				}
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class SaveAction extends AbstractAction {
		/**
		 * 
		 */
		protected boolean showDialog;

		/**
		 * 
		 */
		protected String lastDir = null;

		/**
		 * 
		 */
		public SaveAction(boolean showDialog) {
			this.showDialog = showDialog;
		}

		/**
		 * Saves XML+PNG format.
		 */
		protected void saveXmlPng(BasicGraphEditor editor, String filename, Color bg) throws IOException {
			mxGraphComponent graphComponent = editor.getGraphComponent();
			mxGraph graph = graphComponent.getGraph();

			// Creates the image for the PNG file
			BufferedImage image = mxCellRenderer.createBufferedImage(graph, null, 1, bg, graphComponent.isAntiAlias(),
					null, graphComponent.getCanvas());

			// Creates the URL-encoded XML data
			mxCodec codec = new mxCodec();
			String xml = URLEncoder.encode(mxXmlUtils.getXml(codec.encode(graph.getModel())), "UTF-8");
			mxPngEncodeParam param = mxPngEncodeParam.getDefaultEncodeParam(image);
			param.setCompressedText(new String[] { "mxGraphModel", xml });

			// Saves as a PNG file
			FileOutputStream outputStream = new FileOutputStream(new File(filename));
			try {
				mxPngImageEncoder encoder = new mxPngImageEncoder(outputStream, param);

				if (image != null) {
					encoder.encode(image);

					editor.setModified(false);
					editor.setCurrentFile(new File(filename));
				} else {
					JOptionPane.showMessageDialog(graphComponent, mxResources.get("noImageData"));
				}
			} finally {
				outputStream.close();
			}
		}

		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e) {
			BasicGraphEditor editor = getEditor(e);

			if (editor != null) {
				mxGraphComponent graphComponent = editor.getGraphComponent();
				mxGraph graph = graphComponent.getGraph();
				FileFilter selectedFilter = null;
				DefaultFileFilter xmlPngFilter = new DefaultFileFilter(".png",
						"PNG+XML " + mxResources.get("file") + " (.png)");
				FileFilter vmlFileFilter = new DefaultFileFilter(".html",
						"VML " + mxResources.get("file") + " (.html)");
				String filename = null;
				boolean dialogShown = false;

				if (showDialog || editor.getCurrentFile() == null) {
					String wd;

					if (lastDir != null) {
						wd = lastDir;
					} else if (editor.getCurrentFile() != null) {
						wd = editor.getCurrentFile().getParent();
					} else {
						wd = System.getProperty("user.dir");
					}

					JFileChooser fc = new JFileChooser(wd);

					// Adds the default file format
					FileFilter defaultFilter = xmlPngFilter;
					fc.addChoosableFileFilter(defaultFilter);

					// Adds special vector graphics formats and HTML
					fc.addChoosableFileFilter(
							new DefaultFileFilter(".mxe", "mxGraph Editor " + mxResources.get("file") + " (.mxe)"));
					fc.addChoosableFileFilter(
							new DefaultFileFilter(".txt", "Graph Drawing " + mxResources.get("file") + " (.txt)"));
					fc.addChoosableFileFilter(
							new DefaultFileFilter(".svg", "SVG " + mxResources.get("file") + " (.svg)"));
					fc.addChoosableFileFilter(vmlFileFilter);
					fc.addChoosableFileFilter(
							new DefaultFileFilter(".html", "HTML " + mxResources.get("file") + " (.html)"));

					// Adds a filter for each supported image format
					Object[] imageFormats = ImageIO.getReaderFormatNames();

					// Finds all distinct extensions
					HashSet<String> formats = new HashSet<String>();

					for (int i = 0; i < imageFormats.length; i++) {
						String ext = imageFormats[i].toString().toLowerCase();
						formats.add(ext);
					}

					imageFormats = formats.toArray();

					for (int i = 0; i < imageFormats.length; i++) {
						String ext = imageFormats[i].toString();
						fc.addChoosableFileFilter(new DefaultFileFilter("." + ext,
								ext.toUpperCase() + " " + mxResources.get("file") + " (." + ext + ")"));
					}

					// Adds filter that accepts all supported image formats
					fc.addChoosableFileFilter(new DefaultFileFilter.ImageFileFilter(mxResources.get("allImages")));
					fc.setFileFilter(defaultFilter);
					int rc = fc.showDialog(null, mxResources.get("save"));
					dialogShown = true;

					if (rc != JFileChooser.APPROVE_OPTION) {
						return;
					} else {
						lastDir = fc.getSelectedFile().getParent();
					}

					filename = fc.getSelectedFile().getAbsolutePath();
					selectedFilter = fc.getFileFilter();

					if (selectedFilter instanceof DefaultFileFilter) {
						String ext = ((DefaultFileFilter) selectedFilter).getExtension();

						if (!filename.toLowerCase().endsWith(ext)) {
							filename += ext;
						}
					}

					if (new File(filename).exists() && JOptionPane.showConfirmDialog(graphComponent,
							mxResources.get("overwriteExistingFile")) != JOptionPane.YES_OPTION) {
						return;
					}
				} else {
					filename = editor.getCurrentFile().getAbsolutePath();
				}

				try {
					String ext = filename.substring(filename.lastIndexOf('.') + 1);

					if (ext.equalsIgnoreCase("svg")) {
						mxSvgCanvas canvas = (mxSvgCanvas) mxCellRenderer.drawCells(graph, null, 1, null,
								new CanvasFactory() {
									public mxICanvas createCanvas(int width, int height) {
										mxSvgCanvas canvas = new mxSvgCanvas(
												mxDomUtils.createSvgDocument(width, height));
										canvas.setEmbedded(true);

										return canvas;
									}

								});

						mxUtils.writeFile(mxXmlUtils.getXml(canvas.getDocument()), filename);
					} else if (selectedFilter == vmlFileFilter) {
						mxUtils.writeFile(mxXmlUtils.getXml(
								mxCellRenderer.createVmlDocument(graph, null, 1, null, null).getDocumentElement()),
								filename);
					} else if (ext.equalsIgnoreCase("html")) {
						mxUtils.writeFile(mxXmlUtils.getXml(
								mxCellRenderer.createHtmlDocument(graph, null, 1, null, null).getDocumentElement()),
								filename);
					} else if (ext.equalsIgnoreCase("mxe") || ext.equalsIgnoreCase("xml")) {
						mxCodec codec = new mxCodec();
						String xml = mxXmlUtils.getXml(codec.encode(graph.getModel()));

						mxUtils.writeFile(xml, filename);

						editor.setModified(false);
						editor.setCurrentFile(new File(filename));
					} else if (ext.equalsIgnoreCase("txt")) {
						String content = mxGdCodec.encode(graph);

						mxUtils.writeFile(content, filename);
					} else {
						Color bg = null;

						if ((!ext.equalsIgnoreCase("gif") && !ext.equalsIgnoreCase("png"))
								|| JOptionPane.showConfirmDialog(graphComponent,
										mxResources.get("transparentBackground")) != JOptionPane.YES_OPTION) {
							bg = graphComponent.getBackground();
						}

						if (selectedFilter == xmlPngFilter
								|| (editor.getCurrentFile() != null && ext.equalsIgnoreCase("png") && !dialogShown)) {
							saveXmlPng(editor, filename, bg);
						} else {
							BufferedImage image = mxCellRenderer.createBufferedImage(graph, null, 1, bg,
									graphComponent.isAntiAlias(), null, graphComponent.getCanvas());

							if (image != null) {
								ImageIO.write(image, ext, new File(filename));
							} else {
								JOptionPane.showMessageDialog(graphComponent, mxResources.get("noImageData"));
							}
						}
					}
				} catch (Throwable ex) {
					ex.printStackTrace();
					JOptionPane.showMessageDialog(graphComponent, ex.toString(), mxResources.get("error"),
							JOptionPane.ERROR_MESSAGE);
				}
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class SelectShortestPathAction extends AbstractAction {
		/**
		 * 
		 */
		protected boolean directed;

		/**
		 * 
		 */
		public SelectShortestPathAction(boolean directed) {
			this.directed = directed;
		}

		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() instanceof mxGraphComponent) {
				mxGraphComponent graphComponent = (mxGraphComponent) e.getSource();
				mxGraph graph = graphComponent.getGraph();
				mxIGraphModel model = graph.getModel();

				Object source = null;
				Object target = null;

				Object[] cells = graph.getSelectionCells();

				for (int i = 0; i < cells.length; i++) {
					if (model.isVertex(cells[i])) {
						if (source == null) {
							source = cells[i];
						} else if (target == null) {
							target = cells[i];
						}
					}

					if (source != null && target != null) {
						break;
					}
				}

				if (source != null && target != null) {
					int steps = graph.getChildEdges(graph.getDefaultParent()).length;
				} else {
					JOptionPane.showMessageDialog(graphComponent, mxResources.get("noSourceAndTargetSelected"));
				}
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class SelectSpanningTreeAction extends AbstractAction {
		/**
		 * 
		 */
		protected boolean directed;

		/**
		 * 
		 */
		public SelectSpanningTreeAction(boolean directed) {
			this.directed = directed;
		}

		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() instanceof mxGraphComponent) {
				mxGraphComponent graphComponent = (mxGraphComponent) e.getSource();
				mxGraph graph = graphComponent.getGraph();
				mxIGraphModel model = graph.getModel();

				Object parent = graph.getDefaultParent();
				Object[] cells = graph.getSelectionCells();

				for (int i = 0; i < cells.length; i++) {
					if (model.getChildCount(cells[i]) > 0) {
						parent = cells[i];
						break;
					}
				}

				Object[] v = graph.getChildVertices(parent);
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class ToggleDirtyAction extends AbstractAction {
		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() instanceof mxGraphComponent) {
				mxGraphComponent graphComponent = (mxGraphComponent) e.getSource();
				graphComponent.showDirtyRectangle = !graphComponent.showDirtyRectangle;
			}
		}

	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class ToggleConnectModeAction extends AbstractAction {
		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() instanceof mxGraphComponent) {
				mxGraphComponent graphComponent = (mxGraphComponent) e.getSource();
				mxConnectionHandler handler = graphComponent.getConnectionHandler();
				handler.setHandleEnabled(!handler.isHandleEnabled());
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class ToggleCreateTargetItem extends JCheckBoxMenuItem {
		/**
		 * 
		 */
		public ToggleCreateTargetItem(final BasicGraphEditor editor, String name) {
			super(name);
			setSelected(true);

			addActionListener(new ActionListener() {
				/**
				 * 
				 */
				public void actionPerformed(ActionEvent e) {
					mxGraphComponent graphComponent = editor.getGraphComponent();

					if (graphComponent != null) {
						mxConnectionHandler handler = graphComponent.getConnectionHandler();
						handler.setCreateTarget(!handler.isCreateTarget());
						setSelected(handler.isCreateTarget());
					}
				}
			});
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class PromptPropertyAction extends AbstractAction {
		/**
		 * 
		 */
		protected Object target;

		/**
		 * 
		 */
		protected String fieldname, message;

		/**
		 * 
		 */
		public PromptPropertyAction(Object target, String message) {
			this(target, message, message);
		}

		/**
		 * 
		 */
		public PromptPropertyAction(Object target, String message, String fieldname) {
			this.target = target;
			this.message = message;
			this.fieldname = fieldname;
		}

		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() instanceof Component) {
				try {
					Method getter = target.getClass().getMethod("get" + fieldname);
					Object current = getter.invoke(target);

					// TODO: Support other atomic types
					if (current instanceof Integer) {
						Method setter = target.getClass().getMethod("set" + fieldname, new Class[] { int.class });

						String value = (String) JOptionPane.showInputDialog((Component) e.getSource(), "Value", message,
								JOptionPane.PLAIN_MESSAGE, null, null, current);

						if (value != null) {
							setter.invoke(target, Integer.parseInt(value));
						}
					}
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}

			// Repaints the graph component
			if (e.getSource() instanceof mxGraphComponent) {
				mxGraphComponent graphComponent = (mxGraphComponent) e.getSource();
				graphComponent.repaint();
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class TogglePropertyItem extends JCheckBoxMenuItem {
		/**
		 * 
		 */
		public TogglePropertyItem(Object target, String name, String fieldname) {
			this(target, name, fieldname, false);
		}

		/**
		 * 
		 */
		public TogglePropertyItem(Object target, String name, String fieldname, boolean refresh) {
			this(target, name, fieldname, refresh, null);
		}

		/**
		 * 
		 */
		public TogglePropertyItem(final Object target, String name, final String fieldname, final boolean refresh,
				ActionListener listener) {
			super(name);

			// Since action listeners are processed last to first we add the
			// given
			// listener here which means it will be processed after the one
			// below
			if (listener != null) {
				addActionListener(listener);
			}

			addActionListener(new ActionListener() {
				/**
				 * 
				 */
				public void actionPerformed(ActionEvent e) {
					execute(target, fieldname, refresh);
				}
			});

			PropertyChangeListener propertyChangeListener = new PropertyChangeListener() {

				/*
				 * (non-Javadoc)
				 * 
				 * @see
				 * java.beans.PropertyChangeListener#propertyChange(java.beans.
				 * PropertyChangeEvent)
				 */
				public void propertyChange(PropertyChangeEvent evt) {
					if (evt.getPropertyName().equalsIgnoreCase(fieldname)) {
						update(target, fieldname);
					}
				}
			};

			if (target instanceof mxGraphComponent) {
				((mxGraphComponent) target).addPropertyChangeListener(propertyChangeListener);
			} else if (target instanceof mxGraph) {
				((mxGraph) target).addPropertyChangeListener(propertyChangeListener);
			}

			update(target, fieldname);
		}

		/**
		 * 
		 */
		public void update(Object target, String fieldname) {
			if (target != null && fieldname != null) {
				try {
					Method getter = target.getClass().getMethod("is" + fieldname);

					if (getter != null) {
						Object current = getter.invoke(target);

						if (current instanceof Boolean) {
							setSelected(((Boolean) current).booleanValue());
						}
					}
				} catch (Exception e) {
					// ignore
				}
			}
		}

		/**
		 * 
		 */
		public void execute(Object target, String fieldname, boolean refresh) {
			if (target != null && fieldname != null) {
				try {
					Method getter = target.getClass().getMethod("is" + fieldname);
					Method setter = target.getClass().getMethod("set" + fieldname, new Class[] { boolean.class });

					Object current = getter.invoke(target);

					if (current instanceof Boolean) {
						boolean value = !((Boolean) current).booleanValue();
						setter.invoke(target, value);
						setSelected(value);
					}

					if (refresh) {
						mxGraph graph = null;

						if (target instanceof mxGraph) {
							graph = (mxGraph) target;
						} else if (target instanceof mxGraphComponent) {
							graph = ((mxGraphComponent) target).getGraph();
						}

						graph.refresh();
					}
				} catch (Exception e) {
					// ignore
				}
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class HistoryAction extends AbstractAction {
		/**
		 * 
		 */
		protected boolean undo;

		/**
		 * 
		 */
		public HistoryAction(boolean undo) {
			this.undo = undo;
		}

		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e) {
			BasicGraphEditor editor = getEditor(e);

			if (editor != null) {
				if (undo) {
					editor.getUndoManager().undo();
				} else {
					editor.getUndoManager().redo();
				}
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class FontStyleAction extends AbstractAction {
		/**
		 * 
		 */
		protected boolean bold;

		/**
		 * 
		 */
		public FontStyleAction(boolean bold) {
			this.bold = bold;
		}

		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() instanceof mxGraphComponent) {
				mxGraphComponent graphComponent = (mxGraphComponent) e.getSource();
				Component editorComponent = null;

				if (graphComponent.getCellEditor() instanceof mxCellEditor) {
					editorComponent = ((mxCellEditor) graphComponent.getCellEditor()).getEditor();
				}

				if (editorComponent instanceof JEditorPane) {
					JEditorPane editorPane = (JEditorPane) editorComponent;
					int start = editorPane.getSelectionStart();
					int ende = editorPane.getSelectionEnd();
					String text = editorPane.getSelectedText();

					if (text == null) {
						text = "";
					}

					try {
						HTMLEditorKit editorKit = new HTMLEditorKit();
						HTMLDocument document = (HTMLDocument) editorPane.getDocument();
						document.remove(start, (ende - start));
						editorKit.insertHTML(document, start,
								((bold) ? "<b>" : "<i>") + text + ((bold) ? "</b>" : "</i>"), 0, 0,
								(bold) ? HTML.Tag.B : HTML.Tag.I);
					} catch (Exception ex) {
						ex.printStackTrace();
					}

					editorPane.requestFocus();
					editorPane.select(start, ende);
				} else {
					mxIGraphModel model = graphComponent.getGraph().getModel();
					model.beginUpdate();
					try {
						graphComponent.stopEditing(false);
						graphComponent.getGraph().toggleCellStyleFlags(mxConstants.STYLE_FONTSTYLE,
								(bold) ? mxConstants.FONT_BOLD : mxConstants.FONT_ITALIC);
					} finally {
						model.endUpdate();
					}
				}
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class WarningAction extends AbstractAction {
		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() instanceof mxGraphComponent) {
				mxGraphComponent graphComponent = (mxGraphComponent) e.getSource();
				Object[] cells = graphComponent.getGraph().getSelectionCells();

				if (cells != null && cells.length > 0) {
					String warning = JOptionPane.showInputDialog(mxResources.get("enterWarningMessage"));

					for (int i = 0; i < cells.length; i++) {
						graphComponent.setCellWarning(cells[i], warning);
					}
				} else {
					JOptionPane.showMessageDialog(graphComponent, mxResources.get("noCellSelected"));
				}
			}
		}
	}

	// @SuppressWarnings("serial")
	// public static class TestTransitions extends AbstractAction
	// {
	// /**
	// *
	// */
	// public void actionPerformed(ActionEvent e)
	// {
	// BasicGraphEditor editor = getEditor(e);
	//
	//
	//
	// mxGraph graph = editor.getGraphComponent().getGraph();
	// System.out.println(graph.getModel().toString());
	// //Selectionne le graph
	// graph.selectAll();
	// Object[] cells = graph.getSelectionCells();
	//
	// String type;
	// List<Test> transitions = new ArrayList<Test>() ;
	//
	// mxIGraphModel model = graph.getModel();
	// int lastCellIndex=0;
	// for (int i = 0; i < cells.length; i++)
	// {
	// type="source";
	// if(graph.getModel().isEdge(cells[i]))
	// { Test interVar = new Test();
	// type="Edge";
	// interVar.setEdgeValue((String) model.getValue(cells[i]));
	// interVar.setSource((String) model.getValue(model.getTerminal(cells[i],
	// true)));
	// interVar.setTerminal((String) model.getValue(model.getTerminal(cells[i],
	// false)));
	// try {
	// transitions.add(interVar);
	// }
	// catch(Exception a)
	// {
	// System.out.println("Error while adding the edge to the list(Editor Action
	// 1465 approx) :"+a.getMessage());
	// }
	//
	//
	// }
	// else if(graph.getModel().isVertex(cells[i]))
	// {
	// lastCellIndex=i;
	// type ="Vertex";
	// System.out.println(type+" = "+model.getValue(cells[i]));
	// }
	// }
	// System.out.println(transitions);
	// //Enleve la selection sur le graph
	// graph.removeSelectionCells(cells);
	// System.out.println(transitions.size());
	// for (int i=0;i<transitions.size();i++)
	// {
	// transitions.get(i).expandParenthese();
	// transitions.get(i).generate2Values();
	//
	// }
	// System.out.println("+++++++++++++++++++++++ Affichage de tt les transi
	// +++++++++++++++++");
	//
	// System.out.println(transitions);
	// /**
	// * ALL TRANSITIONS COVERAGE
	// */
	// System.out.println("%%%%%%%%%%%%%%%%%%%%%% ALL TRANSITIONS COVERAGE
	// %%%%%%%%%%%%%%%%%%%%%%%%");
	// List<TestRow> t = new ArrayList<>();
	// TestRow test1 = new TestRow();
	// test1.setPre("State1");
	// test1.setPost("State2");
	// test1.setProc("Transition");
	// t.add(test1);
	// for (int i=0;i<transitions.size();i++)
	// {
	// int lengthT = transitions.get(i).getValues().length;
	// System.out.println(i+". Transition "+transitions.get(i).getSource()+" ->
	// "+transitions.get(i).getTerminal()+":");
	//
	// for (int i1=0;i1<=transitions.get(i).getValues().length;i1++)
	// { int nbr_true = lengthT-i1;
	// System.out.println("FALSE = "+i1+"\nTRUE = "+nbr_true);
	// TestRow test = new TestRow();
	// test.setPre(transitions.get(i).getSource());
	// test.setPost(transitions.get(i).getTerminal());
	// for (int j=0;j<i1;j++)
	// {
	// if(transitions.get(i).getValues()[j].contains("=="))
	// {
	// System.out.println(transitions.get(i).getValues()[j].replaceAll("==",
	// "!="));
	// test.setProc(test.getProc()+'\n'+transitions.get(i).getValues()[j].replaceAll("==",
	// "!="));
	// }
	// else
	// System.out.println(transitions.get(i).getValues()[j]+"=0");
	// test.setProc(test.getProc()+'\n'+transitions.get(i).getValues()[j]+"=0");
	// }
	// for (int j=i1;j<lengthT;j++)
	// {
	// System.out.println(transitions.get(i).getValues()[j]);
	// test.setProc(test.getProc()+'\n'+transitions.get(i).getValues()[j]);
	// }t.add(test);
	// }
	// }
	// TestRow[] b = t.stream().toArray(TestRow[]::new);
	// WriteExcel.write("test", b, "testing.xlsx");
	// System.out.println("*********************************************************");
	// for (int i =0 ; i<b.length;i++) {
	// System.out.println(b[i]);
	// }
	//// String c="";
	//// for (int i=0;i<5;i++) //create binary with all 1 digits
	//// {
	//// c+="1";
	//// }
	//// System.out.println("c="+c);
	//// int max = Integer.parseInt(c,2); //contain the loop limit
	//// System.out.println("max="+max);
	//// System.out.println("Printing the number");
	//// for (int i=1; i<=max; i++)
	//// {
	//// String x = Integer.toBinaryString( i);
	//// String binaryString =
	// String.format("%"+Integer.toString(c.length())+"s",new
	// BigInteger(x)).replace(" ","0");//replace with lengthT
	//// System.out.println(binaryString); // contain all true/false
	// combinations
	//// }
	// }
	// }
	public static class Database extends AbstractAction {
		public void actionPerformed(ActionEvent e) {
			// Schedule a job for the event-dispatching thread:
			// creating and showing this application's GUI.
			javax.swing.SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					DatabaseGUI.createAndShowGUI();
				}
			});
		}
	}

	/*
	 * 
	 */
	@SuppressWarnings("serial")
	public static class ConditionCoverage extends AbstractAction {
		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e) {
			BasicGraphEditor editor = getEditor(e);
			TestRow uniqueEdge = new TestRow();

			mxGraph graph = editor.getGraphComponent().getGraph();
			System.out.println(graph.getModel().toString());
			// Selectionne le graph
			graph.selectAll();
			Object[] cells = graph.getSelectionCells();
			String type;
			List<Test> transitions = new ArrayList<Test>();
			Boolean stillDidNotEnter = true;
			Boolean stopEntering = false;
			mxIGraphModel model = graph.getModel();
			int lastCellIndex = 0;
			Boolean youShallPass = true;
			System.out.println("CELLS = "+cells.length);
			for (int i = 0; i < cells.length; i++) {
				
				type = "source";
				if (graph.getModel().isEdge(cells[i])) {
				try{	if(!((String) model.getValue((model.getTerminal(cells[i], true)))).equals("S0: Initial State")) {
						Test interVar = new Test();
						type = "Edge";
						interVar.setEdgeValue((String) model.getValue(cells[i]));
						interVar.setSource((String) model.getValue(model.getTerminal(cells[i], true)));
						interVar.setTerminal((String) model.getValue(model.getTerminal(cells[i], false)));
						//try {System.out.println("ENTERED");
							transitions.add(interVar);
							if (interVar.getEdgeValue() == "") {
								JOptionPane.showMessageDialog(Frame.getFrames()[0],
										"You need to set all Transition Values.");
								youShallPass = false;
								break;
							}
//						} catch (Exception a) {
//							System.out.println("Error while adding the edge to the list(Editor Action 1621 approx) :"
//									+ a.getMessage());
//						}
					}else  {uniqueEdge.setProc("Demarrage du syst�me");
					uniqueEdge.setPre((String) model.getValue(model.getTerminal(cells[i], true)));
					uniqueEdge.setPost((String) model.getValue(model.getTerminal(cells[i], false)));}
				}
				catch(Exception a) {
						type = "Edge";
						//uniqueEdge.setProc((String) model.getValue(cells[i]));
						uniqueEdge.setProc("Demarrage du syst�me");
						uniqueEdge.setPre((String) model.getValue(model.getTerminal(cells[i], true)));
						uniqueEdge.setPost((String) model.getValue(model.getTerminal(cells[i], false)));

					}
				} else if (graph.getModel().isVertex(cells[i])) {
					if (((String) model.getValue(cells[i])).equals("S0: Initial State")
							&& graph.getModel().getEdgeCount(cells[i]) != 1) {
						JOptionPane.showMessageDialog(Frame.getFrames()[0],
								"Initial State Problem, Only 1 Edge allowed.");
						youShallPass = false;
						break;
					}
					if ((model.getValue(cells[i])).equals("")) {
						JOptionPane.showMessageDialog(Frame.getFrames()[0], "You need to set all State .");
						youShallPass = false;
						break;
					}
					// System.out.println(type+" = "+model.getValue(cells[i]));
				}
			}
			// Enleve la selection sur le graph
			graph.removeSelectionCells(cells);
			System.out.println("+++++++++++++++ SIZE = "+transitions.size());
			for (int i = 0; i < transitions.size(); i++) {
				transitions.get(i).expandParenthese();
				transitions.get(i).generate2Values();

			}
			// Contient list des operandes
			List<String[]> listOpList = new ArrayList<String[]>();
			if (youShallPass) {

				List<TestRow> t = new ArrayList<>();
				List<TestRow> tOfCurrentTransition = new ArrayList<>();

				TestRow test1 = new TestRow();
				test1.setPre("Precondition");
				test1.setPost("Expected result");
				test1.setProc("Procedure");
				t.add(test1);
				t.add(uniqueEdge);
				int maxNumberOfVal = 0;
				for (int l = 0; l < transitions.size(); l++) {
					List<TestRow> ta = new ArrayList<>();
					List<TestRow> tDelete = new ArrayList<>();
					List<VarValeurs> posOfMultiVars = new ArrayList<>();
					tOfCurrentTransition = new ArrayList<>();
					// Transition : decoupage
					List<LigneTest> lineTest = new ArrayList<>();

					String[] aa = transitions.get(l).getValues();
					// contient les operandes
					String[] opList = new String[2 + (aa.length - 1) * 2];
					int opPosition = 1;
					for (int j = 0; j < aa.length; j++) {
						LigneTest ligneTest = new LigneTest();
						aa[j] = aa[j].replaceAll(" +", " ");
						String[] b = aa[j].split(" ");
						if (j > 0) {
							int pos = b[0].indexOf('|');
							if (pos < 0)
								pos = b[0].indexOf('&');
							int temp = pos + 2;
							while (b[0].charAt(pos + 2) == ')') {
								temp++;
							}
							ligneTest.setOp(b[0].substring(0, temp));
							ligneTest.setDataBase(b[0].substring(temp) + " " + b[1]);
							opList[opPosition] = b[0].substring(0, temp);
							System.out.println("b[0]=" + b[0].substring(0, temp));
							if (j == aa.length - 1) {
								opPosition += 2;
								if (b[b.length - 1].contains(")"))
									opList[opPosition] = b[b.length - 1].substring(b[b.length - 1].indexOf(')'));
								else
									opList[opPosition] = "";

							}
							opPosition += 2;
						} else {
							ligneTest.setDataBase(b[0] + " " + b[1]);
						}
						ligneTest.setVariable(b[2]);
						ligneTest.setEquals(b[3]);
						List<String> valeurs = new ArrayList<String>();
						for (int k = 4; k < b.length; k = k + 2) {
							String val = b[k];
							// System.out.println("val = "+val);
							valeurs.add(val.replaceAll(" ", ""));
							System.out.println("===-------val--------======" + val);
							if (valeurs.get(k - 4 - (k - 4) / 2).indexOf(")") > 0) {
								valeurs.set(k - 4 - (k - 4) / 2, valeurs.get(k - 4 - (k - 4) / 2).substring(0,
										valeurs.get(k - 4 - (k - 4) / 2).indexOf(")")));
							}
						}
						ligneTest.setValeurs(valeurs);
						if (InputTransition.isBool(ligneTest.getVariable())) {
							ligneTest.setType("bool�en");
						} else {
							ligneTest.setType("int");
						}
						lineTest.add(ligneTest);
						listOpList.add(l, opList);
					}
					String c = "";
					String zeros = "";
					List<String> binaryList = new ArrayList<String>();
					int longTransi = transitions.get(l).getValues().length;
					for (int i = 0; i < longTransi; i++) // create binary with
															// all 1 digits
					{
						c += "1";
						zeros += "0";
					}
					int max = Integer.parseInt(c, 2); // contain the loop limit
					binaryList.add(zeros);
					for (int i = 1; i <= max; i++) {
						String x = Integer.toBinaryString(i);
						String binaryString = String.format("%" + Integer.toString(c.length()) + "s", new BigInteger(x))
								.replace(" ", "0");// replace with lengthT
						binaryList.add(binaryString); // contain all true/false
														// combinations
					}
					// binaryList.add(c);
					TestRow test = new TestRow();
					int n = 0;
					// Contient l'indice des lineTransition
					Map<Integer, List<Integer>> indices = new HashMap<Integer, List<Integer>>();
					for (int j = 0; j < longTransi; j++) {
						n += lineTest.get(j).getValeurs().size() - 1;
						int k = 0;
						if (lineTest.get(j).getValeurs().size() > 1) {
							List<Integer> ab = new ArrayList<Integer>();
							if (k > 0)
								ab = indices.get(k - 1);
							ab.add(j);
							indices.put(k, ab);
						}
					}
					List<IndiceRep> indiceRep = new ArrayList<>();
					for (int i = 0; i <= max; i++) {
						test = new TestRow();
						test.setPre(transitions.get(l).getSource());
						try {
							for (int j = 0; j < binaryList.get(i).length(); j++) {
								String[] tempArray = new String[listOpList.get(l).length];
								boolean in = false;
								tempArray = listOpList.get(l).clone();
								System.out.println(
										"-----------------------------------------------------------------------------");
								System.out.println(listOpList.get(l)[listOpList.get(l).length - 1]);
								if (listOpList.get(l)[listOpList.get(l).length - 1] == null)
									listOpList.get(l)[listOpList.get(l).length - 1] = "";
								if (j * 2 + 1 < tempArray.length - 1) {
									System.out.println("LIST" + i + "=" + tempArray[j * 2 + 1]);
									if (tempArray[j * 2 + 1].contains("(")) {
										in = true;
										String s = "";
										while (tempArray[j * 2 + 1].indexOf('(') >= 0) {
											s += tempArray[j * 2 + 1].charAt(tempArray[j * 2 + 1].indexOf('('));
											System.out.println("BEFORE" + listOpList.get(l)[j * 2 + 1]);
											tempArray[j * 2 + 1] = tempArray[j * 2 + 1].replaceFirst("\\(", "");
											System.out.println("AFTER" + listOpList.get(l)[j * 2 + 1]);
										}
										listOpList.get(l)[j * 2] = s + binaryList.get(i).charAt(j);

									}
									if (tempArray[j * 2 + 1].contains(")")) {
										in = true;
										String s = "";
										while (tempArray[j * 2 + 1].indexOf(')') >= 0) {
											s += tempArray[j * 2 + 1].charAt(tempArray[j * 2 + 1].indexOf(')'));
											tempArray[j * 2 + 1] = tempArray[j * 2 + 1].replaceFirst("\\)", "");
										}

										listOpList.get(l)[j * 2] = binaryList.get(i).charAt(j) + s;
									}
									if (!in) {
										listOpList.get(l)[j * 2] = binaryList.get(i).charAt(j) + "";
									}
								} else {
									System.out.println("WHAAT");
									listOpList.get(l)[j * 2] = binaryList.get(i).charAt(j) + "";
								}
								if (tempArray[tempArray.length - 1] != null
										&& tempArray[tempArray.length - 1].contains(")")) {
									System.out.println("ALL GOOOOOOOooooooooooooD");
								}
							}
						} catch (Exception exception) {
							JOptionPane.showMessageDialog(Frame.getFrames()[0],
									"Error at transition\n" + " try to delete transition if this persist");
							throw exception;
						}
						String[] arrayTemp2 = new String[listOpList.get(l).length];
						arrayTemp2 = listOpList.get(l).clone();
						for (int u = 0; 2 * u + 1 < listOpList.get(l).length - 1; u++) {
							arrayTemp2[u * 2 + 1] = listOpList.get(l)[u * 2 + 1].replace("(", "");
							arrayTemp2[u * 2 + 1] = arrayTemp2[u * 2 + 1].replace(")", "");
						}
						;
						System.out.println("|||||||||||||||||||---" + listOpList.get(l)[1]);
						// Ecriture du Post transition
						try {
							ScriptEngineManager sem = new ScriptEngineManager();
							ScriptEngine se = sem.getEngineByName("JavaScript");
							String myExpression = generateString(arrayTemp2);
							for (int m = 0; m < listOpList.get(l).length; m++) {
								System.out.println("********" + arrayTemp2[m]);
								System.out.println(m);
							}
							System.out.println("****ALL EXPRESSION***" + myExpression);
							// se.eval(myExpression))

							if ((int) se.eval(myExpression) == 1) {
								test.setPost(transitions.get(l).getTerminal());
							} else
								test.setPost(transitions.get(l).getSource());

						} catch (ScriptException exept) {

							System.out.println("Invalid Expression");
							exept.printStackTrace();

						}
						Boolean add = false;
						for (int j = 0; j < longTransi; j++) {
							String a = "";
							LigneTest b = lineTest.get(j);
							if (stillDidNotEnter && b.getValeurs().size() > 1) {
								VarValeurs v = new VarValeurs();
								v.setPos(j);
								List<String> valeurs = new ArrayList<>();
								for (int temp = 1; temp < b.getValeurs().size(); temp++)
									valeurs.add(b.getValeurs().get(temp));
								v.setValeurs(valeurs);
								v.setValInit(b.getValeurs().get(0));
								posOfMultiVars.add(v);
								stopEntering = true;
							}
							if (binaryList.get(i).charAt(j) == '1') {
								if (b.getValeurs().size() > 1) {
									add = true;
								}
								a = b.getDataBase() + " " + b.getVariable() + " " + b.getEquals() + " "
										+ b.getValeurs().get(0);
							} else {

								if (b.getType().contains("int")) {
									int[] minMax = new int[2];
									minMax = InputTransition.getMinMax(b.getVariable());
									if (b.getEquals().contains("<=")) {
										// TODO: IF YOU WANT TO ADD SPECIAL
										// TREATMENT ADD IT HERE
										if (b.getVariable().contains("wait"))
											a = b.getDataBase() + " " + b.getVariable() + " > " + b.getValeurs().get(0);
										else
											a = b.getDataBase() + " " + b.getVariable() + " > " + b.getValeurs().get(0);
										// a=b.getDataBase()+"
										// "+b.getVariable()+" == "+"99";
									} else if (b.getEquals().contains(">=")) {
										a = b.getDataBase() + " " + b.getVariable() + " < " + b.getValeurs().get(0);
									} else {
										int x = 0;
										if (b.getVariable().equals("wait")) {
											x = (int) (Math.random()
													* (Integer.parseInt(b.getValeurs().get(0)) - minMax[0])
													+ minMax[0]);
											while (b.getValeurs().contains(x + "")) {
												x = (int) (Math.random()
														* (Integer.parseInt(b.getValeurs().get(0)) - minMax[0])
														+ minMax[0]);
											}
										} else {
											x = (int) (Math.random() * (minMax[1] - minMax[0]) + minMax[0]);
											while (b.getValeurs().contains(x + "")) {
												x = (int) (Math.random() * (minMax[1] - minMax[0]) + minMax[0]);
											}
										}
										a = b.getDataBase() + " " + b.getVariable() + " " + b.getEquals() + " " + x;
									}
								} else {
									a = b.getDataBase() + " " + b.getVariable() + " " + b.getEquals() + " "
											+ (1 - Integer.parseInt(b.getValeurs().get(0)));
								}
								// System.out.println("b.getEquals =
								// "+b.getEquals());
							}
							if (j == 0) {
								test.setProc(a);
							} else
								test.setProc(test.getProc() + '\n' + a);
						}
						if (stopEntering) {
							stillDidNotEnter = false;
						}

						if (add) {
							tOfCurrentTransition.add(test);
						}
						t.add(test);
					}
					if (ta.size() < 1) {
						ta.addAll(tOfCurrentTransition);
						tDelete.clear();
						tDelete.addAll(tOfCurrentTransition);
					}
					for (int h = 0; h < posOfMultiVars.size(); h++) {
						System.out.println("values=" + posOfMultiVars.get(h).getValeurs());
						for (int k = 0; k < posOfMultiVars.get(h).getValeurs().size(); k++) {
							List<TestRow> tb = new ArrayList<>();
							for (int p = 0; p < ta.size(); p++) {
								Boolean notAdd1 = false;
								Boolean notAdd2 = true;
								TestRow te = new TestRow(ta.get(p));
								String[] s = te.getProc().split("\n");
								String[] sa = s[posOfMultiVars.get(h).getPos()].split(" +");
								String chaineModif = "";
								if (sa[sa.length - 1].contains(posOfMultiVars.get(h).getValInit())) {
									notAdd1 = true;
								}
								for (int ko = 0; ko < posOfMultiVars.get(h).getValeurs().size(); ko++) {
									if (sa[sa.length - 1].contains(posOfMultiVars.get(h).getValeurs().get(ko))) {
										notAdd2 = false;
									}
								}
								sa[sa.length - 1] = posOfMultiVars.get(h).getValeurs().get(k);
								for (int j = 0; j < sa.length; j++) {
									chaineModif += sa[j];
								}
								s[posOfMultiVars.get(h).getPos()] = chaineModif;
								// initialisation
								te.setProc(s[0]);
								for (int j = 1; j < s.length; j++) {
									te.setProc(te.getProc() + '\n' + s[j]);
								}
								// end set
								if (notAdd1 && notAdd2) {
									tb.add(te);
								}
							}
							ta.addAll(tb);
						}
					}
					ta.removeAll(tDelete);
					t.addAll(ta);
				}

				TestRow[] b = t.stream().toArray(TestRow[]::new);
				for (int m = 0; m < b.length; m++) {
					System.out.println(b[m]);
				}
				JFileChooser chooser = new JFileChooser();
				FileNameExtensionFilter filter = new FileNameExtensionFilter("Excel", "xlsx", "xls");
				chooser.setFileFilter(filter);
				int returnVal = chooser.showSaveDialog(Frame.getFrames()[0]);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					if (!chooser.getSelectedFile().getPath().contains(".")) {
						WriteExcel.write("Condition Coverage", b, chooser.getSelectedFile().getPath() + ".xlsx");
					} else {
						WriteExcel.write("Condition Coverage", b, chooser.getSelectedFile().getPath());
					}
				}
			}
		}
	}

	/*
	 * 
	 */
	@SuppressWarnings("serial")
	public static class StateCoverage extends AbstractAction {
		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e) {
			BasicGraphEditor editor = getEditor(e);
			TestRow uniqueEdge = new TestRow();
			mxGraph graph = editor.getGraphComponent().getGraph();
			System.out.println(graph.getModel().toString());
			// Selectionne le graph
			graph.selectAll();
			Object[] cells = graph.getSelectionCells();
			List<Test> transitions = new ArrayList<Test>();
			Boolean stillDidNotEnter = true;
			Boolean stopEntering = false;
			mxIGraphModel model = graph.getModel();
			List<String> States = new ArrayList<String>();
			Boolean youShallPass = true;
			for (int i = 0; i < cells.length; i++) {
				if (graph.getModel().isEdge(cells[i])) {				
					try{	if(!((String) model.getValue((model.getTerminal(cells[i], true)))).equals("S0: Initial State")) {
					Test interVar = new Test();
					//type = "Edge";
					interVar.setEdgeValue((String) model.getValue(cells[i]));
					interVar.setSource((String) model.getValue(model.getTerminal(cells[i], true)));
					interVar.setTerminal((String) model.getValue(model.getTerminal(cells[i], false)));
					//try {System.out.println("ENTERED");
						transitions.add(interVar);
						if (interVar.getEdgeValue() == "") {
							JOptionPane.showMessageDialog(Frame.getFrames()[0],
									"You need to set all Transition Values.");
							youShallPass = false;
							break;
						}
//					} catch (Exception a) {
//						System.out.println("Error while adding the edge to the list(Editor Action 1621 approx) :"
//								+ a.getMessage());
//					}
				} else  {uniqueEdge.setProc("Demarrage du syst�me");
				uniqueEdge.setPre((String) model.getValue(model.getTerminal(cells[i], true)));
				uniqueEdge.setPost((String) model.getValue(model.getTerminal(cells[i], false)));}}catch(Exception a) {

					//type = "Edge";
					//uniqueEdge.setProc((String) model.getValue(cells[i]));
					uniqueEdge.setProc("Demarrage du syst�me");
					uniqueEdge.setPre((String) model.getValue(model.getTerminal(cells[i], true)));
					uniqueEdge.setPost((String) model.getValue(model.getTerminal(cells[i], false)));

				}}
					else if (graph.getModel().isVertex(cells[i])) {
					States.add((String) model.getValue(cells[i]));
					// System.out.println(type+" = "+model.getValue(cells[i]));
					if ((model.getValue(cells[i])).equals("")) {
						JOptionPane.showMessageDialog(Frame.getFrames()[0], "You need to set all State Values.");
						youShallPass = false;
						break;
					}
				}
			}
			// Enleve la selection sur le graph
			graph.removeSelectionCells(cells);
			for (int i = 0; i < transitions.size(); i++) {
				transitions.get(i).expandParenthese();
				transitions.get(i).generate2Values();

			}

			// Aucun interet d'executer le reste si les state/transi sont vides
			if (youShallPass) {
				// Contient list des operandes
				List<String[]> listOpList = new ArrayList<String[]>();

				List<TestRow> t = new ArrayList<>();

				TestRow test1 = new TestRow();
				test1.setPre("Precondition");
				test1.setPost("Expected result");
				test1.setProc("Procedure");
				t.add(test1);
				t.add(uniqueEdge);
				List<String> StateTrans = new ArrayList<>();
				for (int l = 0; l < States.size(); l++) {
					for (int p = 0; p < States.size(); p++) {
						StateTrans.add(States.get(l) + " " + States.get(p));
					}

				}
				for (int l = 0; l < transitions.size(); l++) {
					List<VarValeurs> posOfMultiVars = new ArrayList<>();
					// Transition : decoupage
					List<LigneTest> lineTest = new ArrayList<>();

					String[] aa = transitions.get(l).getValues();
					// contient les operandes
					String[] opList = new String[2 + (aa.length - 1) * 2];
					int opPosition = 1;
					for (int j = 0; j < aa.length; j++) {
						LigneTest ligneTest = new LigneTest();
						aa[j] = aa[j].replaceAll(" +", " ");
						String[] b = aa[j].split(" ");
						if (j > 0) {
							int pos = b[0].indexOf('|');
							if (pos < 0)
								pos = b[0].indexOf('&');
							int temp = pos + 2;
							while (b[0].charAt(pos + 2) == ')') {
								temp++;
							}
							ligneTest.setOp(b[0].substring(0, temp));
							ligneTest.setDataBase(b[0].substring(temp) + " " + b[1]);
							opList[opPosition] = b[0].substring(0, temp);
							System.out.println("b[0]=" + b[0].substring(0, temp));
							if (j == aa.length - 1) {
								opPosition += 2;
								if (b[b.length - 1].contains(")"))
									opList[opPosition] = b[b.length - 1].substring(b[b.length - 1].indexOf(')'));
								else
									opList[opPosition] = "";

							}
							opPosition += 2;
						} else {
							ligneTest.setDataBase(b[0] + " " + b[1]);
						}
						ligneTest.setVariable(b[2]);
						ligneTest.setEquals(b[3]);
						List<String> valeurs = new ArrayList<String>();
						for (int k = 4; k < b.length; k = k + 2) {
							String val = b[k];
							// System.out.println("val = "+val);
							valeurs.add(val.replaceAll(" ", ""));
							System.out.println("===-------val--------======" + val);
							if (valeurs.get(k - 4 - (k - 4) / 2).indexOf(")") > 0) {
								valeurs.set(k - 4 - (k - 4) / 2, valeurs.get(k - 4 - (k - 4) / 2).substring(0,
										valeurs.get(k - 4 - (k - 4) / 2).indexOf(")")));
							}
						}
						ligneTest.setValeurs(valeurs);
						if (InputTransition.isBool(ligneTest.getVariable())) {
							ligneTest.setType("bool�en");
						} else {
							ligneTest.setType("int");
						}
						lineTest.add(ligneTest);
						listOpList.add(l, opList);
					}
					String c = "";
					String zeros = "";
					List<String> binaryList = new ArrayList<String>();
					int longTransi = transitions.get(l).getValues().length;
					for (int i = 0; i < longTransi; i++) // create binary with
															// all 1 digits
					{
						c += "1";
						zeros += "0";
					}
					int max = Integer.parseInt(c, 2); // contain the loop limit
					binaryList.add(zeros);
					for (int i = 1; i <= max; i++) {
						String x = Integer.toBinaryString(i);
						String binaryString = String.format("%" + Integer.toString(c.length()) + "s", new BigInteger(x))
								.replace(" ", "0");// replace with lengthT
						binaryList.add(binaryString); // contain all true/false
														// combinations
					}
					// binaryList.add(c);
					TestRow test = new TestRow();
					int n = 0;
					// Contient l'indice des lineTransition
					Map<Integer, List<Integer>> indices = new HashMap<Integer, List<Integer>>();
					for (int j = 0; j < longTransi; j++) {
						n += lineTest.get(j).getValeurs().size() - 1;
						int k = 0;
						if (lineTest.get(j).getValeurs().size() > 1) {
							List<Integer> ab = new ArrayList<Integer>();
							if (k > 0)
								ab = indices.get(k - 1);
							ab.add(j);
							indices.put(k, ab);
						}
					}
					List<IndiceRep> indiceRep = new ArrayList<>();
					for (int i = 0; i <= max; i++) {
						test = new TestRow();
						test.setPre(transitions.get(l).getSource());
						try {
							for (int j = 0; j < binaryList.get(i).length(); j++) {
								String[] tempArray = new String[listOpList.get(l).length];
								boolean in = false;
								tempArray = listOpList.get(l).clone();
								System.out.println(
										"-----------------------------------------------------------------------------");
								System.out.println(listOpList.get(l)[listOpList.get(l).length - 1]);
								if (listOpList.get(l)[listOpList.get(l).length - 1] == null)
									listOpList.get(l)[listOpList.get(l).length - 1] = "";
								if (j * 2 + 1 < tempArray.length - 1) {
									System.out.println("LIST" + i + "=" + tempArray[j * 2 + 1]);
									if (tempArray[j * 2 + 1].contains("(")) {
										in = true;
										String s = "";
										while (tempArray[j * 2 + 1].indexOf('(') >= 0) {
											s += tempArray[j * 2 + 1].charAt(tempArray[j * 2 + 1].indexOf('('));
											System.out.println("BEFORE" + listOpList.get(l)[j * 2 + 1]);
											tempArray[j * 2 + 1] = tempArray[j * 2 + 1].replaceFirst("\\(", "");
											System.out.println("AFTER" + listOpList.get(l)[j * 2 + 1]);
										}
										listOpList.get(l)[j * 2] = s + binaryList.get(i).charAt(j);

									}
									if (tempArray[j * 2 + 1].contains(")")) {
										in = true;
										String s = "";
										while (tempArray[j * 2 + 1].indexOf(')') >= 0) {
											s += tempArray[j * 2 + 1].charAt(tempArray[j * 2 + 1].indexOf(')'));
											tempArray[j * 2 + 1] = tempArray[j * 2 + 1].replaceFirst("\\)", "");
										}

										listOpList.get(l)[j * 2] = binaryList.get(i).charAt(j) + s;
									}
									if (!in) {
										listOpList.get(l)[j * 2] = binaryList.get(i).charAt(j) + "";
									}
								} else {
									System.out.println("WHAAT");
									listOpList.get(l)[j * 2] = binaryList.get(i).charAt(j) + "";
								}
								if (tempArray[tempArray.length - 1] != null
										&& tempArray[tempArray.length - 1].contains(")")) {
									System.out.println("ALL GOOOOOOOooooooooooooD");
								}
							}
						} catch (Exception exception) {
							JOptionPane.showMessageDialog(Frame.getFrames()[0],
									"Error at transition\n" + " try to delete transition if this persist");
							throw exception;
						}
						String[] arrayTemp2 = new String[listOpList.get(l).length];
						arrayTemp2 = listOpList.get(l).clone();
						for (int u = 0; 2 * u + 1 < listOpList.get(l).length - 1; u++) {
							arrayTemp2[u * 2 + 1] = listOpList.get(l)[u * 2 + 1].replace("(", "");
							arrayTemp2[u * 2 + 1] = arrayTemp2[u * 2 + 1].replace(")", "");
						}
						;
						System.out.println("|||||||||||||||||||---" + listOpList.get(l)[1]);
						// Ecriture du Post transition
						try {
							ScriptEngineManager sem = new ScriptEngineManager();
							ScriptEngine se = sem.getEngineByName("JavaScript");
							String myExpression = generateString(arrayTemp2);
							for (int m = 0; m < listOpList.get(l).length; m++) {
								System.out.println("********" + arrayTemp2[m]);
								System.out.println(m);
							}
							System.out.println("****ALL EXPRESSION***" + myExpression);
							// se.eval(myExpression))

							if ((int) se.eval(myExpression) == 1) {
								test.setPost(transitions.get(l).getTerminal());
							} else
								test.setPost(transitions.get(l).getSource());

						} catch (ScriptException exept) {

							System.out.println("Invalid Expression");
							exept.printStackTrace();

						}
						if (StateTrans.contains(test.getPre() + " " + test.getPost())) {
							StateTrans.remove(test.getPre() + " " + test.getPost());
							for (int j = 0; j < longTransi; j++) {
								String a = "";
								LigneTest b = lineTest.get(j);
								if (stillDidNotEnter && b.getValeurs().size() > 1) {
									VarValeurs v = new VarValeurs();
									v.setPos(j);
									List<String> valeurs = new ArrayList<>();
									for (int temp = 1; temp < b.getValeurs().size(); temp++)
										valeurs.add(b.getValeurs().get(temp));
									v.setValeurs(valeurs);
									v.setValInit(b.getValeurs().get(0));
									posOfMultiVars.add(v);
									stopEntering = true;
								}
								if (binaryList.get(i).charAt(j) == '1') {
									a = b.getDataBase() + " " + b.getVariable() + " " + b.getEquals() + " "
											+ b.getValeurs().get(0);
								} else {

									if (b.getType().contains("int")) {
										int[] minMax = new int[2];
										minMax = InputTransition.getMinMax(b.getVariable());
										if (b.getEquals().contains("<=")) {
											a = b.getDataBase() + " " + b.getVariable() + " > " + b.getValeurs().get(0);
											// a=b.getDataBase()+"
											// "+b.getVariable()+" == "+"99";
										} else {
											if (b.getEquals().contains(">=")) {
												a = b.getDataBase() + " " + b.getVariable() + " < "
														+ b.getValeurs().get(0);
											} else {
												int x=0;
												if(b.getVariable().equals("wait")){
												x = (int) (Math.random() * (Integer.parseInt(b.getValeurs().get(0)) - minMax[0]) + minMax[0]);
												while (b.getValeurs().contains(x + "")) {
													x = (int) (Math.random() * (Integer.parseInt(b.getValeurs().get(0)) - minMax[0]) + minMax[0]);
												}}
												else {
													 x = (int) (Math.random() * (minMax[1] - minMax[0]) + minMax[0]);
													while (b.getValeurs().contains(x + "")) {
														x = (int) (Math.random() * (minMax[1] - minMax[0]) + minMax[0]);
													}
												}
												
												a = b.getDataBase() + " " + b.getVariable() + " " + b.getEquals() + " "
														+ x;
											}
										}
									} else {
										a = b.getDataBase() + " " + b.getVariable() + " " + b.getEquals() + " "
												+ (1 - Integer.parseInt(b.getValeurs().get(0)));
									}

								}
								if (j == 0) {
									test.setProc(a);
								} else
									test.setProc(test.getProc() + '\n' + a);
							}
							if (stopEntering) {
								stillDidNotEnter = false;
							}

							t.add(test);
						}
					}

				}

				TestRow[] b = t.stream().toArray(TestRow[]::new);
				for (int m = 0; m < b.length; m++) {
					System.out.println(b[m]);
				}
				JFileChooser chooser = new JFileChooser();
				FileNameExtensionFilter filter = new FileNameExtensionFilter("Excel", "xlsx", "xls");
				chooser.setFileFilter(filter);
				int returnVal = chooser.showSaveDialog(Frame.getFrames()[0]);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					if (!chooser.getSelectedFile().getPath().contains(".")) {
						WriteExcel.write("State Coverage", b, chooser.getSelectedFile().getPath() + ".xlsx");
					} else {
						WriteExcel.write("State Coverage", b, chooser.getSelectedFile().getPath());
					}
				}
			}
		}
	}

	/*
	 * 
	 */
	@SuppressWarnings("serial")
	public static class TransitionCoverage extends AbstractAction {
		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e) {
			BasicGraphEditor editor = getEditor(e);
			TestRow uniqueEdge = new TestRow();
			mxGraph graph = editor.getGraphComponent().getGraph();
			System.out.println(graph.getModel().toString());
			// Selectionne le graph
			graph.selectAll();
			Object[] cells = graph.getSelectionCells();
			String type;
			List<Test> transitions = new ArrayList<Test>();
			Boolean stillDidNotEnter = true;
			Boolean stopEntering = false;
			mxIGraphModel model = graph.getModel();
			int lastCellIndex = 0;
			Boolean youShallPass = true;
			for (int i = 0; i < cells.length; i++) {
				type = "source";
				if (graph.getModel().isEdge(cells[i])) {			
					try{	if(!((String) model.getValue((model.getTerminal(cells[i], true)))).equals("S0: Initial State")) {
					Test interVar = new Test();
					type = "Edge";
					interVar.setEdgeValue((String) model.getValue(cells[i]));
					interVar.setSource((String) model.getValue(model.getTerminal(cells[i], true)));
					interVar.setTerminal((String) model.getValue(model.getTerminal(cells[i], false)));
					//try {System.out.println("ENTERED");
						transitions.add(interVar);
						if (interVar.getEdgeValue() == "") {
							JOptionPane.showMessageDialog(Frame.getFrames()[0],
									"You need to set all Transition Values.");
							youShallPass = false;
							break;
						}
//					} catch (Exception a) {
//						System.out.println("Error while adding the edge to the list(Editor Action 1621 approx) :"
//								+ a.getMessage());
//					}
				} else  {uniqueEdge.setProc("Demarrage du syst�me");
				uniqueEdge.setPre((String) model.getValue(model.getTerminal(cells[i], true)));
				uniqueEdge.setPost((String) model.getValue(model.getTerminal(cells[i], false)));}}catch(Exception a) {

					type = "Edge";
					//uniqueEdge.setProc((String) model.getValue(cells[i]));
					uniqueEdge.setProc("Demarrage du syst�me");
					uniqueEdge.setPre((String) model.getValue(model.getTerminal(cells[i], true)));
					uniqueEdge.setPost((String) model.getValue(model.getTerminal(cells[i], false)));

				}} else if (graph.getModel().isVertex(cells[i])) {
					if (((String) model.getValue(cells[i])).equals("S0: Initial State")
							&& graph.getModel().getEdgeCount(cells[i]) != 1) {
						JOptionPane.showMessageDialog(Frame.getFrames()[0],
								"Initial State Problem, Only 1 Edge allowed.");
						youShallPass = false;
						break;
					}
					if ((model.getValue(cells[i])).equals("")) {
						JOptionPane.showMessageDialog(Frame.getFrames()[0], "You need to set all State Values.");
						youShallPass = false;
						break;
					}
					// System.out.println(type+" = "+model.getValue(cells[i]));
				}
			}
			// Enleve la selection sur le graph
			graph.removeSelectionCells(cells);
			for (int i = 0; i < transitions.size(); i++) {
				transitions.get(i).expandParenthese();
				transitions.get(i).generate2Values();

			}
			// Contient list des operandes
			List<String[]> listOpList = new ArrayList<String[]>();

			if (youShallPass) {
				List<TestRow> t = new ArrayList<>();
				List<TestRow> tOfCurrentTransition = new ArrayList<>();

				TestRow test1 = new TestRow();
				test1.setPre("Precondition");
				test1.setPost("Expected result");
				test1.setProc("Procedure");
				t.add(test1);
				t.add(uniqueEdge);
				int maxNumberOfVal = 0;
				for (int l = 0; l < transitions.size(); l++) {
					List<TestRow> ta = new ArrayList<>();
					List<TestRow> tDelete = new ArrayList<>();
					List<VarValeurs> posOfMultiVars = new ArrayList<>();
					tOfCurrentTransition = new ArrayList<>();
					// Transition : decoupage
					List<LigneTest> lineTest = new ArrayList<>();

					String[] aa = transitions.get(l).getValues();
					// contient les operandes
					String[] opList = new String[2 + (aa.length - 1) * 2];
					int opPosition = 1;
					for (int j = 0; j < aa.length; j++) {
						LigneTest ligneTest = new LigneTest();
						aa[j] = aa[j].replaceAll(" +", " ");
						String[] b = aa[j].split(" ");
						if (j > 0) {
							int pos = b[0].indexOf('|');
							if (pos < 0)
								pos = b[0].indexOf('&');
							int temp = pos + 2;
							while (b[0].charAt(pos + 2) == ')') {
								temp++;
							}
							ligneTest.setOp(b[0].substring(0, temp));
							ligneTest.setDataBase(b[0].substring(temp) + " " + b[1]);
							opList[opPosition] = b[0].substring(0, temp);
							System.out.println("b[0]=" + b[0].substring(0, temp));
							if (j == aa.length - 1) {
								opPosition += 2;
								if (b[b.length - 1].contains(")"))
									opList[opPosition] = b[b.length - 1].substring(b[b.length - 1].indexOf(')'));
								else
									opList[opPosition] = "";

							}
							opPosition += 2;
						} else {
							ligneTest.setDataBase(b[0] + " " + b[1]);
						}
						ligneTest.setVariable(b[2]);
						ligneTest.setEquals(b[3]);
						List<String> valeurs = new ArrayList<String>();
						for (int k = 4; k < b.length; k = k + 2) {
							String val = b[k];
							// System.out.println("val = "+val);
							valeurs.add(val.replaceAll(" ", ""));
							System.out.println("===-------val--------======" + val);
							if (valeurs.get(k - 4 - (k - 4) / 2).indexOf(")") > 0) {
								valeurs.set(k - 4 - (k - 4) / 2, valeurs.get(k - 4 - (k - 4) / 2).substring(0,
										valeurs.get(k - 4 - (k - 4) / 2).indexOf(")")));
							}
						}
						ligneTest.setValeurs(valeurs);
						if (InputTransition.isBool(ligneTest.getVariable())) {
							ligneTest.setType("bool�en");
						} else {
							ligneTest.setType("int");
						}
						lineTest.add(ligneTest);
						listOpList.add(l, opList);
					}
					String c = "";
					String zeros = "";
					List<String> binaryList = new ArrayList<String>();
					int longTransi = transitions.get(l).getValues().length;
					for (int i = 0; i < longTransi; i++) // create binary with
															// all 1 digits
					{
						c += "1";
						zeros += "0";
					}
					int max = Integer.parseInt(c, 2); // contain the loop limit
					binaryList.add(zeros);
					for (int i = 1; i <= max; i++) {
						String x = Integer.toBinaryString(i);
						String binaryString = String.format("%" + Integer.toString(c.length()) + "s", new BigInteger(x))
								.replace(" ", "0");// replace with lengthT
						binaryList.add(binaryString); // contain all true/false
														// combinations
					}
					// binaryList.add(c);
					TestRow test = new TestRow();
					int n = 0;
					// Contient l'indice des lineTransition
					Map<Integer, List<Integer>> indices = new HashMap<Integer, List<Integer>>();
					for (int j = 0; j < longTransi; j++) {
						n += lineTest.get(j).getValeurs().size() - 1;
						int k = 0;
						if (lineTest.get(j).getValeurs().size() > 1) {
							List<Integer> ab = new ArrayList<Integer>();
							if (k > 0)
								ab = indices.get(k - 1);
							ab.add(j);
							indices.put(k, ab);
						}
					}
					List<IndiceRep> indiceRep = new ArrayList<>();
					for (int i = 0; i <= max; i++) {
						Boolean add1 = true;
						test = new TestRow();
						test.setPre(transitions.get(l).getSource());
						try {
							for (int j = 0; j < binaryList.get(i).length(); j++) {
								String[] tempArray = new String[listOpList.get(l).length];
								boolean in = false;
								tempArray = listOpList.get(l).clone();
								System.out.println(
										"-----------------------------------------------------------------------------");
								System.out.println(listOpList.get(l)[listOpList.get(l).length - 1]);
								if (listOpList.get(l)[listOpList.get(l).length - 1] == null)
									listOpList.get(l)[listOpList.get(l).length - 1] = "";
								if (j * 2 + 1 < tempArray.length - 1) {
									System.out.println("LIST" + i + "=" + tempArray[j * 2 + 1]);
									if (tempArray[j * 2 + 1].contains("(")) {
										in = true;
										String s = "";
										while (tempArray[j * 2 + 1].indexOf('(') >= 0) {
											s += tempArray[j * 2 + 1].charAt(tempArray[j * 2 + 1].indexOf('('));
											System.out.println("BEFORE" + listOpList.get(l)[j * 2 + 1]);
											tempArray[j * 2 + 1] = tempArray[j * 2 + 1].replaceFirst("\\(", "");
											System.out.println("AFTER" + listOpList.get(l)[j * 2 + 1]);
										}
										listOpList.get(l)[j * 2] = s + binaryList.get(i).charAt(j);

									}
									if (tempArray[j * 2 + 1].contains(")")) {
										in = true;
										String s = "";
										while (tempArray[j * 2 + 1].indexOf(')') >= 0) {
											s += tempArray[j * 2 + 1].charAt(tempArray[j * 2 + 1].indexOf(')'));
											tempArray[j * 2 + 1] = tempArray[j * 2 + 1].replaceFirst("\\)", "");
										}

										listOpList.get(l)[j * 2] = binaryList.get(i).charAt(j) + s;
									}
									if (!in) {
										listOpList.get(l)[j * 2] = binaryList.get(i).charAt(j) + "";
									}
								} else {
									System.out.println("WHAAT");
									listOpList.get(l)[j * 2] = binaryList.get(i).charAt(j) + "";
								}
								if (tempArray[tempArray.length - 1] != null
										&& tempArray[tempArray.length - 1].contains(")")) {
									System.out.println("ALL GOOOOOOOooooooooooooD");
								}
							}
						} catch (Exception exception) {
							JOptionPane.showMessageDialog(Frame.getFrames()[0],
									"Error at transition\n" + " try to delete transition if this persist");
							throw exception;
						}
						String[] arrayTemp2 = new String[listOpList.get(l).length];
						arrayTemp2 = listOpList.get(l).clone();
						for (int u = 0; 2 * u + 1 < listOpList.get(l).length - 1; u++) {
							arrayTemp2[u * 2 + 1] = listOpList.get(l)[u * 2 + 1].replace("(", "");
							arrayTemp2[u * 2 + 1] = arrayTemp2[u * 2 + 1].replace(")", "");
						}
						;
						System.out.println("|||||||||||||||||||---" + listOpList.get(l)[1]);
						// Ecriture du Post transition
						try {
							ScriptEngineManager sem = new ScriptEngineManager();
							ScriptEngine se = sem.getEngineByName("JavaScript");
							String myExpression = generateString(arrayTemp2);
							for (int m = 0; m < listOpList.get(l).length; m++) {
								System.out.println("********" + arrayTemp2[m]);
								System.out.println(m);
							}
							System.out.println("****ALL EXPRESSION***" + myExpression);
							// se.eval(myExpression))
							if ((int) se.eval(myExpression) == 1) {
								test.setPost(transitions.get(l).getTerminal());
								add1=true;
							} else{
								test.setPost(transitions.get(l).getSource());
							add1=false;}

						} catch (ScriptException exept) {

							System.out.println("Invalid Expression");
							exept.printStackTrace();

						}
						Boolean add = false;
						for (int j = 0; j < longTransi; j++) {
							String a = "";
							LigneTest b = lineTest.get(j);
							if (stillDidNotEnter && b.getValeurs().size() > 1) {
								VarValeurs v = new VarValeurs();
								v.setPos(j);
								List<String> valeurs = new ArrayList<>();
								for (int temp = 1; temp < b.getValeurs().size(); temp++)
									valeurs.add(b.getValeurs().get(temp));
								v.setValeurs(valeurs);
								v.setValInit(b.getValeurs().get(0));
								posOfMultiVars.add(v);
								stopEntering = true;
							}
							if (binaryList.get(i).charAt(j) == '1') {
								if (b.getValeurs().size() > 1) {
									add = true;
								}
								a = b.getDataBase() + " " + b.getVariable() + " " + b.getEquals() + " "
										+ b.getValeurs().get(0);
							} else {

								if (b.getType().contains("int")) {
									int[] minMax = new int[2];
									minMax = InputTransition.getMinMax(b.getVariable());
									if (b.getEquals().contains("<")) {
										a = b.getDataBase() + " " + b.getVariable() + " > " + b.getValeurs().get(0);
										// a=b.getDataBase()+"
										// "+b.getVariable()+" == "+"99";
									} else if (b.getEquals().contains(">")) {
										a = b.getDataBase() + " " + b.getVariable() + " < " + b.getValeurs().get(0);
									} else {
										int x=0;
										if(b.getVariable().equals("wait")){
										x = (int) (Math.random() * (Integer.parseInt(b.getValeurs().get(0)) - minMax[0]) + minMax[0]);
										while (b.getValeurs().contains(x + "")) {
											x = (int) (Math.random() * (Integer.parseInt(b.getValeurs().get(0)) - minMax[0]) + minMax[0]);
										}}
										else {
											 x = (int) (Math.random() * (minMax[1] - minMax[0]) + minMax[0]);
											while (b.getValeurs().contains(x + "")) {
												x = (int) (Math.random() * (minMax[1] - minMax[0]) + minMax[0]);
											}
										}
										a = b.getDataBase() + " " + b.getVariable() + " " + b.getEquals() + " " + x;
									}
								} else {
									a = b.getDataBase() + " " + b.getVariable() + " == "
											+ (1 - Integer.parseInt(b.getValeurs().get(0)));
								}

							}
							if (j == 0) {
								test.setProc(a);
							} else
								test.setProc(test.getProc() + '\n' + a);
						}
						if (stopEntering) {
							stillDidNotEnter = false;
						}

						if (add1) {
							if (add) {
								tOfCurrentTransition.add(test);
							}
							t.add(test);
						}
					}
					if (ta.size() < 1) {
						ta.addAll(tOfCurrentTransition);
						tDelete.clear();
						tDelete.addAll(tOfCurrentTransition);
					}
					// TODO: IF YOU WANNA SHOW MULTY VALUES PUT THIS CODE PART BACK
					
					
//					for (int h = 0; h < posOfMultiVars.size(); h++) {
//						System.out.println("values=" + posOfMultiVars.get(h).getValeurs());
//						for (int k = 0; k < posOfMultiVars.get(h).getValeurs().size(); k++) {
//							List<TestRow> tb = new ArrayList<>();
//							for (int p = 0; p < ta.size(); p++) {
//								Boolean notAdd1 = false;
//								Boolean notAdd2 = true;
//								TestRow te = new TestRow(ta.get(p));
//								String[] s = te.getProc().split("\n");
//								String[] sa = s[posOfMultiVars.get(h).getPos()].split(" +");
//								String chaineModif = "";
//								if (sa[sa.length - 1].contains(posOfMultiVars.get(h).getValInit())) {
//									notAdd1 = true;
//								}
//								for (int ko = 0; ko < posOfMultiVars.get(h).getValeurs().size(); ko++) {
//									if (sa[sa.length - 1].contains(posOfMultiVars.get(h).getValeurs().get(ko))) {
//										notAdd2 = false;
//									}
//								}
//								sa[sa.length - 1] = posOfMultiVars.get(h).getValeurs().get(k);
//								for (int j = 0; j < sa.length; j++) {
//									chaineModif += " "+sa[j];
//								}
//								s[posOfMultiVars.get(h).getPos()] = chaineModif;
//								// initialisation
//								te.setProc(s[0]);
//								for (int j = 1; j < s.length; j++) {
//									te.setProc(te.getProc() + '\n' + s[j]);
//								}
//								// end set
//								if (notAdd1 && notAdd2) {
//									tb.add(te);
//								}
//							}
//							ta.addAll(tb);
//						}
//					}
					ta.removeAll(tDelete);
					t.addAll(ta);
				}

				TestRow[] b = t.stream().toArray(TestRow[]::new);
				for (int m = 0; m < b.length; m++) {
					System.out.println(b[m]);
				}
				JFileChooser chooser = new JFileChooser();
				FileNameExtensionFilter filter = new FileNameExtensionFilter("Excel", "xlsx", "xls");
				chooser.setFileFilter(filter);
				int returnVal = chooser.showSaveDialog(Frame.getFrames()[0]);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					if (!chooser.getSelectedFile().getPath().contains(".")) {
						WriteExcel.write("Transition Coverage", b, chooser.getSelectedFile().getPath() + ".xlsx");
					} else {
						WriteExcel.write("Transition Coverage", b, chooser.getSelectedFile().getPath());
					}
				}
			}
		}
	}

	/*
	 * 
	 */
	@SuppressWarnings("serial")
	public static class Profile extends AbstractAction {
		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e) {
			BasicGraphEditor editor = getEditor(e);
			TestRow uniqueEdge = new TestRow();
			mxGraph graph = editor.getGraphComponent().getGraph();
			System.out.println(graph.getModel().toString());
			// Selectionne le graph
			graph.selectAll();
			Object[] cells = graph.getSelectionCells();
			List<Test> transitions = new ArrayList<Test>();
			Boolean stillDidNotEnter = true;
			Boolean stopEntering = false;
			mxIGraphModel model = graph.getModel();
			List<String> States = new ArrayList<String>();
			Boolean youShallPass = true;
			for (int i = 0; i < cells.length; i++) {
				if (graph.getModel().isEdge(cells[i])) {
					try{	if(!((String) model.getValue((model.getTerminal(cells[i], true)))).equals("S0: Initial State")) {
					Test interVar = new Test();
				//	type = "Edge";
					interVar.setEdgeValue((String) model.getValue(cells[i]));
					interVar.setSource((String) model.getValue(model.getTerminal(cells[i], true)));
					interVar.setTerminal((String) model.getValue(model.getTerminal(cells[i], false)));
					//try {System.out.println("ENTERED");
						transitions.add(interVar);
						if (interVar.getEdgeValue() == "") {
							JOptionPane.showMessageDialog(Frame.getFrames()[0],
									"You need to set all Transition Values.");
							youShallPass = false;
							break;
						}
//					} catch (Exception a) {
//						System.out.println("Error while adding the edge to the list(Editor Action 1621 approx) :"
//								+ a.getMessage());
//					}
				}else  {uniqueEdge.setProc("Demarrage du syst�me");
				uniqueEdge.setPre((String) model.getValue(model.getTerminal(cells[i], true)));
				uniqueEdge.setPost((String) model.getValue(model.getTerminal(cells[i], false)));} }catch(Exception a) {

			//		type = "Edge";
					//uniqueEdge.setProc((String) model.getValue(cells[i]));
					uniqueEdge.setProc("Demarrage du syst�me");
					uniqueEdge.setPre((String) model.getValue(model.getTerminal(cells[i], true)));
					uniqueEdge.setPost((String) model.getValue(model.getTerminal(cells[i], false)));

				}} else if (graph.getModel().isVertex(cells[i])) {
					States.add((String) model.getValue(cells[i]));
					if (((String) model.getValue(cells[i])).equals("S0: Initial State")
							&& graph.getModel().getEdgeCount(cells[i]) != 1) {
						JOptionPane.showMessageDialog(Frame.getFrames()[0],
								"Initial State Problem, Only 1 Edge allowed.");
						youShallPass = false;
						break;
					}
					if ((model.getValue(cells[i])).equals("")) {
						JOptionPane.showMessageDialog(Frame.getFrames()[0], "You need to set all State Values.");
						youShallPass = false;
						break;
					}
				}
			}
			// Enleve la selection sur le graph
			graph.removeSelectionCells(cells);
			for (int i = 0; i < transitions.size(); i++) {
				transitions.get(i).expandParenthese();
				transitions.get(i).generate2Values();

			}
			// Contient list des operandes
			List<String[]> listOpList = new ArrayList<String[]>();

			if (youShallPass) {
				List<TestRow> t = new ArrayList<>();

				TestRow test1 = new TestRow();
				test1.setPre("Precondition");
				test1.setPost("Expected result");
				test1.setProc("Procedure");
				t.add(test1);
				//t.add(uniqueEdge);
				List<String> StateTrans = new ArrayList<>();
				for (int l = 0; l < States.size(); l++) {
					for (int p = 0; p < States.size(); p++) {
						StateTrans.add(States.get(l) + " " + States.get(p));
					}
				}
				System.out.println(StateTrans);
				for (int l = 0; l < transitions.size(); l++) {
					List<VarValeurs> posOfMultiVars = new ArrayList<>();
					// Transition : decoupage
					List<LigneTest> lineTest = new ArrayList<>();

					String[] aa = transitions.get(l).getValues();
					// contient les operandes
					String[] opList = new String[2 + (aa.length - 1) * 2];
					int opPosition = 1;
					for (int j = 0; j < aa.length; j++) {
						LigneTest ligneTest = new LigneTest();
						aa[j] = aa[j].replaceAll(" +", " ");
						String[] b = aa[j].split(" ");
						if (j > 0) {
							int pos = b[0].indexOf('|');
							if (pos < 0)
								pos = b[0].indexOf('&');
							int temp = pos + 2;
							while (b[0].charAt(pos + 2) == ')') {
								temp++;
							}
							ligneTest.setOp(b[0].substring(0, temp));
							ligneTest.setDataBase(b[0].substring(temp) + " " + b[1]);
							opList[opPosition] = b[0].substring(0, temp);
							System.out.println("b[0]=" + b[0].substring(0, temp));
							if (j == aa.length - 1) {
								opPosition += 2;
								if (b[b.length - 1].contains(")"))
									opList[opPosition] = b[b.length - 1].substring(b[b.length - 1].indexOf(')'));
								else
									opList[opPosition] = "";

							}
							opPosition += 2;
						} else {
							ligneTest.setDataBase(b[0] + " " + b[1]);
						}
						ligneTest.setVariable(b[2]);
						ligneTest.setEquals(b[3]);
						List<String> valeurs = new ArrayList<String>();
						for (int k = 4; k < b.length; k = k + 2) {
							String val = b[k];
							// System.out.println("val = "+val);
							valeurs.add(val.replaceAll(" ", ""));
							System.out.println("===-------val--------======" + val);
							if (valeurs.get(k - 4 - (k - 4) / 2).indexOf(")") > 0) {
								valeurs.set(k - 4 - (k - 4) / 2, valeurs.get(k - 4 - (k - 4) / 2).substring(0,
										valeurs.get(k - 4 - (k - 4) / 2).indexOf(")")));
							}
						}
						ligneTest.setValeurs(valeurs);
						if (InputTransition.isBool(ligneTest.getVariable())) {
							ligneTest.setType("bool�en");
						} else {
							ligneTest.setType("int");
						}
						lineTest.add(ligneTest);
						listOpList.add(l, opList);
					}
					String c = "";
					String zeros = "";
					List<String> binaryList = new ArrayList<String>();
					int longTransi = transitions.get(l).getValues().length;
					for (int i = 0; i < longTransi; i++) // create binary with
															// all 1 digits
					{
						c += "1";
						zeros += "0";
					}
					int max = Integer.parseInt(c, 2); // contain the loop limit
					binaryList.add(zeros);
					for (int i = 1; i <= max; i++) {
						String x = Integer.toBinaryString(i);
						String binaryString = String.format("%" + Integer.toString(c.length()) + "s", new BigInteger(x))
								.replace(" ", "0");// replace with lengthT
						binaryList.add(binaryString); // contain all true/false
														// combinations
					}
					// binaryList.add(c);
					TestRow test = new TestRow();
					int n = 0;
					// Contient l'indice des lineTransition
					Map<Integer, List<Integer>> indices = new HashMap<Integer, List<Integer>>();
					for (int j = 0; j < longTransi; j++) {
						n += lineTest.get(j).getValeurs().size() - 1;
						int k = 0;
						if (lineTest.get(j).getValeurs().size() > 1) {
							List<Integer> ab = new ArrayList<Integer>();
							if (k > 0)
								ab = indices.get(k - 1);
							ab.add(j);
							indices.put(k, ab);
						}
					}
					List<IndiceRep> indiceRep = new ArrayList<>();
					for (int i = 0; i <= max; i++) {
						test = new TestRow();
						test.setPre(transitions.get(l).getSource());
						try {
							for (int j = 0; j < binaryList.get(i).length(); j++) {
								String[] tempArray = new String[listOpList.get(l).length];
								boolean in = false;
								tempArray = listOpList.get(l).clone();
								System.out.println(
										"-----------------------------------------------------------------------------");
								System.out.println(listOpList.get(l)[listOpList.get(l).length - 1]);
								if (listOpList.get(l)[listOpList.get(l).length - 1] == null)
									listOpList.get(l)[listOpList.get(l).length - 1] = "";
								if (j * 2 + 1 < tempArray.length - 1) {
									System.out.println("LIST" + i + "=" + tempArray[j * 2 + 1]);
									if (tempArray[j * 2 + 1].contains("(")) {
										in = true;
										String s = "";
										while (tempArray[j * 2 + 1].indexOf('(') >= 0) {
											s += tempArray[j * 2 + 1].charAt(tempArray[j * 2 + 1].indexOf('('));
											System.out.println("BEFORE" + listOpList.get(l)[j * 2 + 1]);
											tempArray[j * 2 + 1] = tempArray[j * 2 + 1].replaceFirst("\\(", "");
											System.out.println("AFTER" + listOpList.get(l)[j * 2 + 1]);
										}
										listOpList.get(l)[j * 2] = s + binaryList.get(i).charAt(j);

									}
									if (tempArray[j * 2 + 1].contains(")")) {
										in = true;
										String s = "";
										while (tempArray[j * 2 + 1].indexOf(')') >= 0) {
											s += tempArray[j * 2 + 1].charAt(tempArray[j * 2 + 1].indexOf(')'));
											tempArray[j * 2 + 1] = tempArray[j * 2 + 1].replaceFirst("\\)", "");
										}

										listOpList.get(l)[j * 2] = binaryList.get(i).charAt(j) + s;
									}
									if (!in) {
										listOpList.get(l)[j * 2] = binaryList.get(i).charAt(j) + "";
									}
								} else {
									System.out.println("WHAAT");
									listOpList.get(l)[j * 2] = binaryList.get(i).charAt(j) + "";
								}
								if (tempArray[tempArray.length - 1] != null
										&& tempArray[tempArray.length - 1].contains(")")) {
									System.out.println("ALL GOOOOOOOooooooooooooD");
								}
							}
						} catch (Exception exception) {
							JOptionPane.showMessageDialog(Frame.getFrames()[0],
									"Error at transition\n" + " try to delete transition if this persist");
							throw exception;
						}
						String[] arrayTemp2 = new String[listOpList.get(l).length];
						arrayTemp2 = listOpList.get(l).clone();
						for (int u = 0; 2 * u + 1 < listOpList.get(l).length - 1; u++) {
							arrayTemp2[u * 2 + 1] = listOpList.get(l)[u * 2 + 1].replace("(", "");
							arrayTemp2[u * 2 + 1] = arrayTemp2[u * 2 + 1].replace(")", "");
						}
						;
						System.out.println("|||||||||||||||||||---" + listOpList.get(l)[1]);
						// Ecriture du Post transition
						try {
							ScriptEngineManager sem = new ScriptEngineManager();
							ScriptEngine se = sem.getEngineByName("JavaScript");
							String myExpression = generateString(arrayTemp2);
							for (int m = 0; m < listOpList.get(l).length; m++) {
								System.out.println("********" + arrayTemp2[m]);
								System.out.println(m);
							}
							System.out.println("****ALL EXPRESSION***" + myExpression);
							// se.eval(myExpression))

							if ((int) se.eval(myExpression) == 1) {
								test.setPost(transitions.get(l).getTerminal());
							} else
								test.setPost(transitions.get(l).getSource());

						} catch (ScriptException exept) {

							System.out.println("Invalid Expression");
							exept.printStackTrace();

						}
						if (StateTrans.contains(test.getPre() + " " + test.getPost())) {
							StateTrans.remove(test.getPre() + " " + test.getPost());
							for (int j = 0; j < longTransi; j++) {
								String a = "";
								LigneTest b = lineTest.get(j);
								if (stillDidNotEnter && b.getValeurs().size() > 1) {
									VarValeurs v = new VarValeurs();
									v.setPos(j);
									List<String> valeurs = new ArrayList<>();
									for (int temp = 1; temp < b.getValeurs().size(); temp++)
										valeurs.add(b.getValeurs().get(temp));
									v.setValeurs(valeurs);
									v.setValInit(b.getValeurs().get(0));
									posOfMultiVars.add(v);
									stopEntering = true;
								}
								if (binaryList.get(i).charAt(j) == '1') {
									a = b.getDataBase() + " " + b.getVariable() + " " + b.getEquals() + " "
											+ b.getValeurs().get(0);
								} else {

									if (b.getType().contains("int")) {
										int[] minMax = new int[2];
										minMax = InputTransition.getMinMax(b.getVariable());
										if (b.getEquals().contains("<")) {
											a = b.getDataBase() + " " + b.getVariable() + " > " + b.getValeurs().get(0);
											// a=b.getDataBase()+"
											// "+b.getVariable()+" == "+"99";
										} else if (b.getEquals().contains(">")) {
											a = b.getDataBase() + " " + b.getVariable() + " < " + b.getValeurs().get(0);
										} else {
											int x=0;
											if(b.getVariable().equals("wait")){
											x = (int) (Math.random() * (Integer.parseInt(b.getValeurs().get(0)) - minMax[0]) + minMax[0]);
											while (b.getValeurs().contains(x + "")) {
												x = (int) (Math.random() * (Integer.parseInt(b.getValeurs().get(0)) - minMax[0]) + minMax[0]);
											}}
											else {
												 x = (int) (Math.random() * (minMax[1] - minMax[0]) + minMax[0]);
												while (b.getValeurs().contains(x + "")) {
													x = (int) (Math.random() * (minMax[1] - minMax[0]) + minMax[0]);
												}
											}
											a = b.getDataBase() + " " + b.getVariable() + " " + b.getEquals() + " " + x;
										}
									} else {
										a = b.getDataBase() + " " + b.getVariable() + " == "
												+ (1 - Integer.parseInt(b.getValeurs().get(0)));
									}

								}
								if (j == 0) {
									test.setProc(a);
								} else
									test.setProc(test.getProc() + '\n' + a);
							}
							if (stopEntering) {
								stillDidNotEnter = false;
							}

							t.add(test);
						}
					}

				}

				// TestRow[] b = t.stream().toArray(TestRow[]::new);
				List<TestRow> ta = new ArrayList<>();
				TestRow top = new TestRow();
				top.setPre(t.get(0).getPre());
				top.setProc(t.get(0).getPre());
				top.setPost(t.get(0).getPost());
				ta.add(top);
				top = new TestRow();
				top.setPre(t.get(1).getPre());
				for (int o = 1; o < t.size() - 1; o++) {
					if (o == 1) {
						top.setProc(o + ". " + t.get(o).getProc());
						top.setPost(o + ". " + t.get(o).getPost());
					} else {
						top.setProc(top.getProc() + "\n" + o + ". " + t.get(o).getProc());
						top.setPost(top.getPost() + "\n" + o + ". " + t.get(o).getPost());
					}
					for (int i = 1; i < t.get(o).getProc().split("\n").length; i++) {
						top.setPost(top.getPost() + "\n");
					}
				}
				top.setProc(top.getProc() + "\n" + (t.size() - 1) + ". " + t.get(t.size() - 1).getProc());
				top.setPost(top.getPost() + "\n" + (t.size() - 1) + ". " + t.get(t.size() - 1).getPost());
				ta.add(top);
				TestRow[] c = ta.stream().toArray(TestRow[]::new);
				JFileChooser chooser = new JFileChooser();
				FileNameExtensionFilter filter = new FileNameExtensionFilter("Excel", "xlsx", "xls");
				chooser.setFileFilter(filter);
				int returnVal = chooser.showSaveDialog(Frame.getFrames()[0]);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					if (!chooser.getSelectedFile().getPath().contains(".")) {
						WriteExcel.write("Profil Coverage", c, chooser.getSelectedFile().getPath() + ".xlsx");
					} else {
						WriteExcel.write("Profil Coverage", c, chooser.getSelectedFile().getPath());
					}
				}
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class NewAction extends AbstractAction {
		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e) {
			BasicGraphEditor editor = getEditor(e);

			if (editor != null) {
				if (!editor.isModified() || JOptionPane.showConfirmDialog(editor,
						mxResources.get("loseChanges")) == JOptionPane.YES_OPTION) {
					mxGraph graph = editor.getGraphComponent().getGraph();

					// Check modified flag and display save dialog
					mxCell root = new mxCell();
					root.insert(new mxCell());
					graph.getModel().setRoot(root);

					editor.setModified(false);
					editor.setCurrentFile(null);
					editor.getGraphComponent().zoomAndCenter();
				}
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class ImportAction extends AbstractAction {
		/**
		 * 
		 */
		protected String lastDir;

		/**
		 * Loads and registers the shape as a new shape in mxGraphics2DCanvas
		 * and adds a new entry to use that shape in the specified palette
		 * 
		 * @param palette
		 *            The palette to add the shape to.
		 * @param nodeXml
		 *            The raw XML of the shape
		 * @param path
		 *            The path to the directory the shape exists in
		 * @return the string name of the shape
		 */
		public static String addStencilShape(EditorPalette palette, String nodeXml, String path) {

			// Some editors place a 3 byte BOM at the start of files
			// Ensure the first char is a "<"
			int lessthanIndex = nodeXml.indexOf("<");
			nodeXml = nodeXml.substring(lessthanIndex);
			mxStencilShape newShape = new mxStencilShape(nodeXml);
			String name = newShape.getName();
			ImageIcon icon = null;

			if (path != null) {
				String iconPath = path + newShape.getIconPath();
				icon = new ImageIcon(iconPath);
			}

			// Registers the shape in the canvas shape registry
			mxGraphics2DCanvas.putShape(name, newShape);

			if (palette != null && icon != null) {
				palette.addTemplate(name, icon, "shape=" + name, 80, 80, "");
			}

			return name;
		}

		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e) {
			BasicGraphEditor editor = getEditor(e);

			if (editor != null) {
				String wd = (lastDir != null) ? lastDir : System.getProperty("user.dir");

				JFileChooser fc = new JFileChooser(wd);

				fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);

				// Adds file filter for Dia shape import
				fc.addChoosableFileFilter(
						new DefaultFileFilter(".shape", "Dia Shape " + mxResources.get("file") + " (.shape)"));

				int rc = fc.showDialog(null, mxResources.get("importStencil"));

				if (rc == JFileChooser.APPROVE_OPTION) {
					lastDir = fc.getSelectedFile().getParent();

					try {
						if (fc.getSelectedFile().isDirectory()) {
							EditorPalette palette = editor.insertPalette(fc.getSelectedFile().getName());

							for (File f : fc.getSelectedFile().listFiles(new FilenameFilter() {
								public boolean accept(File dir, String name) {
									return name.toLowerCase().endsWith(".shape");
								}
							})) {
								String nodeXml = mxUtils.readFile(f.getAbsolutePath());
								addStencilShape(palette, nodeXml, f.getParent() + File.separator);
							}

							JComponent scrollPane = (JComponent) palette.getParent().getParent();
							editor.getLibraryPane().setSelectedComponent(scrollPane);

							// FIXME: Need to update the size of the palette to
							// force a layout
							// update. Re/in/validate of palette or parent does
							// not work.
							// editor.getLibraryPane().revalidate();
						} else {
							String nodeXml = mxUtils.readFile(fc.getSelectedFile().getAbsolutePath());
							String name = addStencilShape(null, nodeXml, null);

							JOptionPane.showMessageDialog(editor,
									mxResources.get("stencilImported", new String[] { name }));
						}
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class OpenAction extends AbstractAction {
		/**
		 * 
		 */
		protected String lastDir;

		/**
		 * 
		 */
		protected void resetEditor(BasicGraphEditor editor) {
			editor.setModified(false);
			editor.getUndoManager().clear();
			editor.getGraphComponent().zoomAndCenter();
		}

		/**
		 * Reads XML+PNG format.
		 */
		protected void openXmlPng(BasicGraphEditor editor, File file) throws IOException {
			Map<String, String> text = mxPngTextDecoder.decodeCompressedText(new FileInputStream(file));

			if (text != null) {
				String value = text.get("mxGraphModel");

				if (value != null) {
					Document document = mxXmlUtils.parseXml(URLDecoder.decode(value, "UTF-8"));
					mxCodec codec = new mxCodec(document);
					codec.decode(document.getDocumentElement(), editor.getGraphComponent().getGraph().getModel());
					editor.setCurrentFile(file);
					resetEditor(editor);

					return;
				}
			}

			JOptionPane.showMessageDialog(editor, mxResources.get("imageContainsNoDiagramData"));
		}

		/**
		 * @throws IOException
		 *
		 */
		protected void openGD(BasicGraphEditor editor, File file, String gdText) {
			mxGraph graph = editor.getGraphComponent().getGraph();

			// Replaces file extension with .mxe
			String filename = file.getName();
			filename = filename.substring(0, filename.length() - 4) + ".mxe";

			if (new File(filename).exists() && JOptionPane.showConfirmDialog(editor,
					mxResources.get("overwriteExistingFile")) != JOptionPane.YES_OPTION) {
				return;
			}

			((mxGraphModel) graph.getModel()).clear();
			mxGdCodec.decode(gdText, graph);
			editor.getGraphComponent().zoomAndCenter();
			editor.setCurrentFile(new File(lastDir + "/" + filename));
		}

		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e) {
			BasicGraphEditor editor = getEditor(e);

			if (editor != null) {
				if (!editor.isModified() || JOptionPane.showConfirmDialog(editor,
						mxResources.get("loseChanges")) == JOptionPane.YES_OPTION) {
					mxGraph graph = editor.getGraphComponent().getGraph();

					if (graph != null) {
						String wd = (lastDir != null) ? lastDir : System.getProperty("user.dir");

						JFileChooser fc = new JFileChooser(wd);

						// Adds file filter for supported file format
						DefaultFileFilter defaultFilter = new DefaultFileFilter(".mxe",
								mxResources.get("allSupportedFormats") + " (.mxe, .png, .vdx)") {

							public boolean accept(File file) {
								String lcase = file.getName().toLowerCase();

								return super.accept(file) || lcase.endsWith(".png") || lcase.endsWith(".vdx");
							}
						};
						fc.addChoosableFileFilter(defaultFilter);

						fc.addChoosableFileFilter(
								new DefaultFileFilter(".mxe", "mxGraph Editor " + mxResources.get("file") + " (.mxe)"));
						fc.addChoosableFileFilter(
								new DefaultFileFilter(".png", "PNG+XML  " + mxResources.get("file") + " (.png)"));

						// Adds file filter for VDX import
						fc.addChoosableFileFilter(
								new DefaultFileFilter(".vdx", "XML Drawing  " + mxResources.get("file") + " (.vdx)"));

						// Adds file filter for GD import
						fc.addChoosableFileFilter(
								new DefaultFileFilter(".txt", "Graph Drawing  " + mxResources.get("file") + " (.txt)"));

						fc.setFileFilter(defaultFilter);

						int rc = fc.showDialog(null, mxResources.get("openFile"));

						if (rc == JFileChooser.APPROVE_OPTION) {
							lastDir = fc.getSelectedFile().getParent();

							try {
								if (fc.getSelectedFile().getAbsolutePath().toLowerCase().endsWith(".png")) {
									openXmlPng(editor, fc.getSelectedFile());
								} else if (fc.getSelectedFile().getAbsolutePath().toLowerCase().endsWith(".txt")) {
									openGD(editor, fc.getSelectedFile(),
											mxUtils.readFile(fc.getSelectedFile().getAbsolutePath()));
								} else {
									Document document = mxXmlUtils
											.parseXml(mxUtils.readFile(fc.getSelectedFile().getAbsolutePath()));

									mxCodec codec = new mxCodec(document);
									codec.decode(document.getDocumentElement(), graph.getModel());
									editor.setCurrentFile(fc.getSelectedFile());

									resetEditor(editor);
								}
							} catch (IOException ex) {
								ex.printStackTrace();
								JOptionPane.showMessageDialog(editor.getGraphComponent(), ex.toString(),
										mxResources.get("error"), JOptionPane.ERROR_MESSAGE);
							}
						}
					}
				}
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class ToggleAction extends AbstractAction {
		/**
		 * 
		 */
		protected String key;

		/**
		 * 
		 */
		protected boolean defaultValue;

		/**
		 * 
		 * @param key
		 */
		public ToggleAction(String key) {
			this(key, false);
		}

		/**
		 * 
		 * @param key
		 */
		public ToggleAction(String key, boolean defaultValue) {
			this.key = key;
			this.defaultValue = defaultValue;
		}

		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e) {
			mxGraph graph = mxGraphActions.getGraph(e);

			if (graph != null) {
				graph.toggleCellStyles(key, defaultValue);
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class SetLabelPositionAction extends AbstractAction {
		/**
		 * 
		 */
		protected String labelPosition, alignment;

		/**
		 * 
		 * @param key
		 */
		public SetLabelPositionAction(String labelPosition, String alignment) {
			this.labelPosition = labelPosition;
			this.alignment = alignment;
		}

		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e) {
			mxGraph graph = mxGraphActions.getGraph(e);

			if (graph != null && !graph.isSelectionEmpty()) {
				graph.getModel().beginUpdate();
				try {
					// Checks the orientation of the alignment to use the
					// correct constants
					if (labelPosition.equals(mxConstants.ALIGN_LEFT) || labelPosition.equals(mxConstants.ALIGN_CENTER)
							|| labelPosition.equals(mxConstants.ALIGN_RIGHT)) {
						graph.setCellStyles(mxConstants.STYLE_LABEL_POSITION, labelPosition);
						graph.setCellStyles(mxConstants.STYLE_ALIGN, alignment);
					} else {
						graph.setCellStyles(mxConstants.STYLE_VERTICAL_LABEL_POSITION, labelPosition);
						graph.setCellStyles(mxConstants.STYLE_VERTICAL_ALIGN, alignment);
					}
				} finally {
					graph.getModel().endUpdate();
				}
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class SetStyleAction extends AbstractAction {
		/**
		 * 
		 */
		protected String value;

		/**
		 * 
		 * @param key
		 */
		public SetStyleAction(String value) {
			this.value = value;
		}

		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e) {
			mxGraph graph = mxGraphActions.getGraph(e);

			if (graph != null && !graph.isSelectionEmpty()) {
				graph.setCellStyle(value);
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class KeyValueAction extends AbstractAction {
		/**
		 * 
		 */
		protected String key, value;

		/**
		 * 
		 * @param key
		 */
		public KeyValueAction(String key) {
			this(key, null);
		}

		/**
		 * 
		 * @param key
		 */
		public KeyValueAction(String key, String value) {
			this.key = key;
			this.value = value;
		}

		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e) {
			mxGraph graph = mxGraphActions.getGraph(e);

			if (graph != null && !graph.isSelectionEmpty()) {
				graph.setCellStyles(key, value);
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class PromptValueAction extends AbstractAction {
		/**
		 * 
		 */
		protected String key, message;

		/**
		 * 
		 * @param key
		 */
		public PromptValueAction(String key, String message) {
			this.key = key;
			this.message = message;
		}

		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() instanceof Component) {
				mxGraph graph = mxGraphActions.getGraph(e);

				if (graph != null && !graph.isSelectionEmpty()) {
					String value = (String) JOptionPane.showInputDialog((Component) e.getSource(),
							mxResources.get("value"), message, JOptionPane.PLAIN_MESSAGE, null, null, "");

					if (value != null) {
						if (value.equals(mxConstants.NONE)) {
							value = null;
						}

						graph.setCellStyles(key, value);
					}
				}
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class AlignCellsAction extends AbstractAction {
		/**
		 * 
		 */
		protected String align;

		/**
		 * 
		 * @param key
		 */
		public AlignCellsAction(String align) {
			this.align = align;
		}

		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e) {
			mxGraph graph = mxGraphActions.getGraph(e);

			if (graph != null && !graph.isSelectionEmpty()) {
				graph.alignCells(align);
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class AutosizeAction extends AbstractAction {
		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e) {
			mxGraph graph = mxGraphActions.getGraph(e);

			if (graph != null && !graph.isSelectionEmpty()) {
				Object[] cells = graph.getSelectionCells();
				mxIGraphModel model = graph.getModel();

				model.beginUpdate();
				try {
					for (int i = 0; i < cells.length; i++) {
						graph.updateCellSize(cells[i]);
					}
				} finally {
					model.endUpdate();
				}
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class ColorAction extends AbstractAction {
		/**
		 * 
		 */
		protected String name, key;

		/**
		 * 
		 * @param key
		 */
		public ColorAction(String name, String key) {
			this.name = name;
			this.key = key;
		}

		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() instanceof mxGraphComponent) {
				mxGraphComponent graphComponent = (mxGraphComponent) e.getSource();
				mxGraph graph = graphComponent.getGraph();

				if (!graph.isSelectionEmpty()) {
					Color newColor = JColorChooser.showDialog(graphComponent, name, null);

					if (newColor != null) {
						graph.setCellStyles(key, mxUtils.hexString(newColor));
					}
				}
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class BackgroundImageAction extends AbstractAction {
		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() instanceof mxGraphComponent) {
				mxGraphComponent graphComponent = (mxGraphComponent) e.getSource();
				String value = (String) JOptionPane.showInputDialog(graphComponent, mxResources.get("backgroundImage"),
						"URL", JOptionPane.PLAIN_MESSAGE, null, null,
						"http://www.callatecs.com/images/background2.JPG");

				if (value != null) {
					if (value.length() == 0) {
						graphComponent.setBackgroundImage(null);
					} else {
						Image background = mxUtils.loadImage(value);
						// Incorrect URLs will result in no image.
						// TODO provide feedback that the URL is not correct
						if (background != null) {
							graphComponent.setBackgroundImage(new ImageIcon(background));
						}
					}

					// Forces a repaint of the outline
					graphComponent.getGraph().repaint();
				}
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class BackgroundAction extends AbstractAction {
		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() instanceof mxGraphComponent) {
				mxGraphComponent graphComponent = (mxGraphComponent) e.getSource();
				Color newColor = JColorChooser.showDialog(graphComponent, mxResources.get("background"), null);

				if (newColor != null) {
					graphComponent.getViewport().setOpaque(true);
					graphComponent.getViewport().setBackground(newColor);
				}

				// Forces a repaint of the outline
				graphComponent.getGraph().repaint();
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class PageBackgroundAction extends AbstractAction {
		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() instanceof mxGraphComponent) {
				mxGraphComponent graphComponent = (mxGraphComponent) e.getSource();
				Color newColor = JColorChooser.showDialog(graphComponent, mxResources.get("pageBackground"), null);

				if (newColor != null) {
					graphComponent.setPageBackgroundColor(newColor);
				}

				// Forces a repaint of the component
				graphComponent.repaint();
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class StyleAction extends AbstractAction {
		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() instanceof mxGraphComponent) {
				mxGraphComponent graphComponent = (mxGraphComponent) e.getSource();
				mxGraph graph = graphComponent.getGraph();
				String initial = graph.getModel().getStyle(graph.getSelectionCell());
				String value = (String) JOptionPane.showInputDialog(graphComponent, mxResources.get("style"),
						mxResources.get("style"), JOptionPane.PLAIN_MESSAGE, null, null, initial);

				if (value != null) {
					graph.setCellStyle(value);
				}
			}
		}
	}

	public static String generateString(String[] strings) {
		String r = "";
		for (int i = 0; i < strings.length; i++)
			r += strings[i];
		return r;
	}

	public static int randomValue(int max, int min, String equals, String val) {
		try {
			int vale = Integer.parseInt(val);

			if (equals.contains(">")) {
				return ((int) (Math.random() * (max - min - vale) + min + vale));
			} else if (equals.contains("<")) {
				return ((int) (Math.random() * (vale - min) + min));
			} else
				return ((int) (Math.random() * (max - min) + min));
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}
}
