package com.focus.enicar2017.testCase;

public class IndiceRep {
	int i;
	int j;
	String valInit;
	String valFinal;
	TestRow testRow;
	
	public TestRow getTestRow() {
		return testRow;
	}
	public void setTestRow(TestRow testRow) {
		this.testRow = testRow;
	}
	public int getI() {
		return i;
	}
	public void setI(int i) {
		this.i = i;
	}
	public int getJ() {
		return j;
	}
	public void setJ(int j) {
		this.j = j;
	}
	public String getValInit() {
		return valInit;
	}
	public void setValInit(String valInit) {
		this.valInit = valInit;
	}
	public String getValFinal() {
		return valFinal;
	}
	public void setValFinal(String valFinal) {
		this.valFinal = valFinal;
	}

}
