package com.focus.enicar2017.testCase;

import java.util.ArrayList;
import java.util.List;

public class LigneTest {

	String dataBase;
	String variable;
	String type;
	List<String> valeurs = new ArrayList<String>();
	String Op=null;
	String Equals ;
	public String getEquals() {
		return Equals;
	}
	public void setEquals(String equals) {
		Equals = equals;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getOp() {
		return Op;
	}
	public void setOp(String op) {
		Op = op;
	}
	public String getDataBase() {
		return dataBase;
	}
	public void setDataBase(String dataBase) {
		this.dataBase = dataBase;
	}
	public String getVariable() {
		return variable;
	}
	public void setVariable(String variable) {
		this.variable = variable;
	}
	public List<String> getValeurs() {
		return valeurs;
	}
	public void setValeurs(List<String> valeurs) {
		this.valeurs = valeurs;
	}
	@Override
	public String toString() {
		return "ligneTest [dataBase=" + dataBase + ", variable=" + variable + ", type=" + type + ", valeurs=" + valeurs
				+ ", Op=" + Op + "]";
	}

}
