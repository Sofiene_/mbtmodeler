package com.focus.enicar2017.testCase;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.mxgraph.examples.swing.editor.EditorActions;

public class Test {

/* 
 * Contient la valeur de la transi
 */
private String edgeValue;

/* 
 * Contient le noeud source
 */
private String source;

/* 
 * Contient le noeud terminal
 */
private String terminal;

/* 
 * Contient la valeur de la transi apr�s D�coupage
 */
private String[] values;

public String getEdgeValue() {
	return edgeValue;
}

public void setEdgeValue(String edgeValue) {
	this.edgeValue = edgeValue;
}

public String getSource() {
	return source;
}

public void setSource(String source) {
	this.source = source;
}

public String getTerminal() {
	return terminal;
}

public void setTerminal(String terminal) {
	this.terminal = terminal;
}

public String[] getValues() {
	return values;
}

public void setValues(String[] values) {
	this.values = values;
}

public void generateValues() {

	int i;
	System.out.println("********GENERETE 1 Value  ********** Affichage des donn�s de la transi "+this.getSource()+" -> "+this.getTerminal()+" **********************");  
	String regex1;
	//String parenthese="([\\s]*\\([^==]*\\)[\\s]*)|([\\s]*\\([\\s]*)|([\\s]*\\)[\\s]*)|([\\s]*\\([^>=]*\\)[\\s]*)|([\\s]*\\([\\s]*)|([\\s]*\\)[\\s]*)|([\\s]*\\([^<=]*\\)[\\s]*)|([\\s]*\\([\\s]*)|([\\s]*\\)[\\s]*)";
	String parenthese="([\\s]*\\([^==]*\\)[\\s]*)|([\\s]*\\([\\s]*)|([\\s]*\\)[\\s]*)";
	
	String and="([\\s]+AND[\\s]+)";
	String or="([\\s]+OR[\\s]+)";
	regex1=parenthese+"|"+and+"|"+or;
    Pattern regex = Pattern.compile(regex1);//,Pattern.CASE_INSENSITIVE);
    
    Matcher m = regex.matcher(edgeValue);
    StringBuffer b= new StringBuffer();
    while (m.find()) 
    	{
    	if(m.group(1) != null)  // Parentheses that must not split
    		{
    		m.appendReplacement(b, m.group(1));//when he find it he puts it back to ignore it ;D
    		}
//    		else if(m.group(2) != null) // Parentheses ( that must split
//    		m.appendReplacement(b, "SplitHere(SplitHere");
//    		else if(m.group(3) != null) // Parentheses ) that must split
//        		m.appendReplacement(b, "SplitHere)SplitHere");
        	else if(m.group(4) != null) //And
    		m.appendReplacement(b, "SplitHere&&SplitHere");
        	else if(m.group(5) != null) //or
        		m.appendReplacement(b, "SplitHere||SplitHere");
        	else 
    		m.appendReplacement(b, m.group(0));
    	}
    
    m.appendTail(b);
    
    String replaced = b.toString();
    
    this.values= replaced.split("SplitHere");
    
	for (i=0;i<values.length;i++)
		{
			System.out.println("---------"+i+" -----------");
			System.out.println(values[i]);
		}
}
public void generate2Values() {
	System.out.println("**********GENERATE 2 VALUE******** Affichage des donn�s de la transi "+this.getSource()+" -> "+this.getTerminal()+" **********************");
	String regex1;
	//String parenthese="([\\s]*\\([^==]*\\)[\\s]*)|([\\s]*\\([\\s]*)|([\\s]*\\)[\\s]*)|([\\s]*\\([^>=]*\\)[\\s]*)|([\\s]*\\([\\s]*)|([\\s]*\\)[\\s]*)|([\\s]*\\([^<=]*\\)[\\s]*)|([\\s]*\\([\\s]*)|([\\s]*\\)[\\s]*)";
	String parenthese="([\\s]*\\{#[^==]*\\}#[\\s]*)|([\\s]*\\{#[\\s]*)|([\\s]*\\}#[\\s]*)";
	String and="([\\s]+\\(*AND\\)*[\\s]+)";
	String or="([\\s]+\\(*OR\\)*[\\s]+)";
	String lastPar="([\\s]+\\)+[\\s]*)";
	regex1=parenthese+"|"+and+"|"+or+"|"+lastPar;
    Pattern regex = Pattern.compile(regex1);//,Pattern.CASE_INSENSITIVE);
    
    Matcher m = regex.matcher(edgeValue);
    StringBuffer b= new StringBuffer();
    while (m.find()) 
    	{
    	if(m.group(1) != null)  // Parentheses that must not split
    		{
    		m.appendReplacement(b, m.group(1));//when he find it he puts it back to ignore it ;D
    		}
//    		else if(m.group(2) != null) // Parentheses ( that must split
//    		m.appendReplacement(b, "(");
//    		else if(m.group(3) != null) // Parentheses ) that must split
//        		m.appendReplacement(b, ")");
        	else if(m.group(4) != null) //And
        	{	String s="";
        	for (int i=0;i<m.group(4).length();i++){
        		if(m.group(4).charAt(i)=='('||m.group(4).charAt(i)==')'){
        			s+=m.group(4).charAt(i);
        		}
        	}
        	m.appendReplacement(b, "SplitHere"+s+"&&");
        	System.out.println("B =  "+b);
        	System.out.println(b+"++++++++++++++++++++++4+"+m.group(4));}
        	else if(m.group(5) != null) //or
        		{
        		String s="";
            	for (int i=0;i<m.group(5).length();i++){
            		if(m.group(5).charAt(i)=='('||m.group(5).charAt(i)==')'){
            			s+=m.group(5).charAt(i);
            		}
            	}
            	m.appendReplacement(b, "SplitHere"+s+"||");
    	System.out.println(b+"++++++++++++++++++++++5+"+m.group(5));}
        	else if(m.group(6) != null) //last parenthese
        	{	
        	String s="";
        	for (int i=0;i<m.group(6).length();i++){
        		if(m.group(6).charAt(i)=='('||m.group(6).charAt(i)==')'){
        			s+=m.group(6).charAt(i);
        		}
        	}m.appendReplacement(b,s);
        	System.out.println("++++++++++++++++++++++6+"+m.group(6));}
        	else 
    		m.appendReplacement(b, m.group(0));
    	}
    
    m.appendTail(b);
    
    String replaced = b.toString();
    
    this.values= replaced.split("SplitHere");
    System.out.println("************************ VALUES *****************");
    for (int i=0;i<values.length;i++)
    System.out.println(values[i]);
    
}

@Override
public String toString() {
	String a="";
	int i=0;
	if(values!= null)
	for (String s: values) {
		a= a +"\n Value "+ i +" = "+ s ;
		i++;
	}
	else a=null;
	
	return "\n******* Transition "+source+" -> "+terminal +"***********\n------------[edgeValue ------------ \n" + edgeValue + "\n------------END edgeValue ------------  \n------------"
			+ "values----------------\n "
			+ a + "\n------------END Values ------------ \n";
}

public void expandParenthese() {
	
	
}



}
