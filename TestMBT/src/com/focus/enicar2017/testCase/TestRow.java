package com.focus.enicar2017.testCase;

public class TestRow {
String pre;
String proc;
String post;

public TestRow(TestRow test) {
	super();
	this.setPre(test.getPre());
	this.setPost(test.getPost());
	this.setProc(test.getProc());
}
public TestRow() {
	super();
}
public String getPre() {
	return pre;
}
public void setPre(String pre) {
	this.pre = pre;
}
public String getProc() {
	return proc;
}
public void setProc(String proc) {
	this.proc = proc;
}
public String getPost() {
	return post;
}
public void setPost(String post) {
	this.post = post;
}
@Override
public String toString() {
	return "testRow [pre=" + pre + ", proc=" + proc + ", post=" + post + "]";
}
}
