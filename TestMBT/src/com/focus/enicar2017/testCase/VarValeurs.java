package com.focus.enicar2017.testCase;

import java.util.List;

public class VarValeurs {
int pos ;
String valInit;
List<String> valeurs;
public int getPos() {
	return pos;
}
public void setPos(int pos) {
	this.pos = pos;
}
public List<String> getValeurs() {
	return valeurs;
}
public void setValeurs(List<String> valeurs) {
	this.valeurs = valeurs;
}
public String getValInit() {
	return valInit;
}
public void setValInit(String valInit) {
	this.valInit = valInit;
}
}
