package com.focus.enicar2017.Transition.Dao;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;

import com.focus.enicar2017.Transition.model.Variables;
import com.focus.enicar2017.Transition.util.HibernateUtil;

/**
* Home object for domain model class Variables.
* @see com.focus.enicar2017.Transition.model.Variables
* @author Hibernate Tools
*/
public class VariablesDao {

	private static final Log log = LogFactory.getLog(VariablesDao.class);

	private final SessionFactory sessionFactory = getSessionFactory();

	protected SessionFactory getSessionFactory() {
		try {
			return (SessionFactory) HibernateUtil.sessionFactory;
		} catch (Exception e) {
			log.error("Could not locate SessionFactory ", e);
			throw new IllegalStateException(
					"Could not locate SessionFactory");
		}
	}


	public void persist(Variables transientInstance) {
		log.debug("persisting Variables instance");
		try {
			openTransaction();
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			System.out.println("persist failed: "+re);
			throw re;
		}
		finally{
		closeTransaction();}
	}

	public void attachDirty(Variables instance) {
		log.debug("attaching dirty Variables instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Variables instance) {
		log.debug("attaching clean Variables instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(Variables persistentInstance) {
		log.debug("deleting Variables instance");
		try {
			openTransaction();
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			System.out.println("delete failed"+ re);
			throw re;
		}
		
		finally{
			closeTransaction();}
	}

	public Variables merge(Variables detachedInstance) {
		log.debug("merging Variables instance");
		try {
			openTransaction();
			Variables result = (Variables) sessionFactory
					.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
		finally{
			closeTransaction();}
	}

	public Variables findById(java.lang.String id) {
		log.debug("getting Variables instance with id: " + id);
		try {
			openTransaction();
			Variables instance = (Variables) sessionFactory
					.getCurrentSession().get(
							"com.focus.enicar2017.Transition.model.Variables", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
		finally{
			closeTransaction();}
	}


	public List<?> findByExample(Variables instance) {
		log.debug("finding Variables instance by example");
		try {openTransaction();
			List<?> results = sessionFactory.getCurrentSession()
					.createCriteria("com.focus.enicar2017.Transition.Dao.VariablesDao")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
		finally{
			closeTransaction();}
	}
	

    public List<?> findAll() {
        List<?> objects = null;
        try {
            openTransaction();
            Query query = sessionFactory.getCurrentSession().createQuery("from Variables");
            objects = query.list();
        } catch (HibernateException e) {
            System.out.println("Error at VariablesDao "+e.getMessage());
        } finally {
            closeTransaction();
        }
        return objects;
    }
    public List<?> findByCat(int cat) {
        List<?> objects = null;
        try {
            openTransaction();
            Query query = sessionFactory.getCurrentSession().createQuery("from Variables WHERE categorie LIKE '"+cat+"'");
            objects = query.list();
        } catch (HibernateException e) {
            System.out.println("Error at VariablesDao"+e.getMessage());
        } finally {
            closeTransaction();
        }
        return objects;
    }
    
	private void openTransaction(){
		sessionFactory.getCurrentSession().beginTransaction();
	}
	
	private void closeTransaction(){
		sessionFactory		.getCurrentSession().getTransaction().commit();
	}

}
