package com.focus.enicar2017.Transition.Dao;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;

import com.focus.enicar2017.Transition.model.Categorie;
import com.focus.enicar2017.Transition.util.HibernateUtil;

/**
* Home object for domain model class Categorie.
* @see com.focus.enicar2017.Transition.model.Categorie
* @author Hibernate Tools
*/
public class CategorieDao {

	private static final Log log = LogFactory.getLog(CategorieDao.class);

	private final SessionFactory sessionFactory = getSessionFactory();

	protected SessionFactory getSessionFactory() {
		try {
			return (SessionFactory) HibernateUtil.sessionFactory;
		} catch (Exception e) {
			log.error("Could not locate SessionFactory ", e);
			throw new IllegalStateException(
					"Could not locate SessionFactory");
		}
	}


	public void persist(Categorie transientInstance) {
		log.debug("persisting Categorie instance");
		try {
			openTransaction();
			getSessionFactory().getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (Exception re) {
			
			log.error("persist failed", re);
			throw re;
		}
		finally{
		closeTransaction();}
	}

	public void attachDirty(Categorie instance) {
		log.debug("attaching dirty Categorie instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Categorie instance) {
		log.debug("attaching clean Categorie instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(Categorie persistentInstance) {
		log.debug("deleting Categorie instance");
		try {
			openTransaction();
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			System.out.println("ERROR "+re);
			throw re;
		}
		finally{
			closeTransaction();}
	}

	public Categorie merge(Categorie detachedInstance) {
		log.debug("merging Categorie instance");
		try {
			openTransaction();
			Categorie result = (Categorie) sessionFactory
					.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
		finally{
			closeTransaction();}
	}

	public Categorie findById(int z) {
		log.debug("getting Categorie instance with id: " + z);
		try {
			openTransaction();
			Categorie instance = (Categorie) sessionFactory
					.getCurrentSession().get(
							"com.focus.enicar2017.Transition.model.Categorie", z);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			System.out.println("get failed : "+ re);
			log.error("get failed", re);
			throw re;
		}
		finally{
			closeTransaction();}
	}


	public List<?> findByExample(Categorie instance) {
		log.debug("finding Categorie instance by example");
		try {
			List<?> results = sessionFactory.getCurrentSession()
					.createCriteria("com.focus.enicar2017.Transition.dao.Categorie")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
	public List<Categorie> findAll() {
        List<Categorie> objects = new ArrayList<Categorie>();
        try {
            openTransaction();
            Query query = sessionFactory.getCurrentSession().createQuery("from Categorie");
     //       System.out.println("QUERY = "+query);
            objects = query.list();
      //      System.out.println("OBJECTS = \n"+objects);
        } catch (HibernateException e) {
            System.out.println("Error at VariablesDao"+e.getMessage());
        } finally {
            closeTransaction();
        }
        return objects;
    }
	private void openTransaction(){
		sessionFactory.getCurrentSession().beginTransaction();
	}
	
	private void closeTransaction(){
		sessionFactory.		 getCurrentSession().flush();
		sessionFactory		.getCurrentSession().getTransaction().commit();
	}

}
