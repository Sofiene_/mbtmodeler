package com.focus.enicar2017.Transition.model;
// Generated 23 mars 2017 14:28:48 by Hibernate Tools 4.3.1.Final

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Variables generated by hbm2java
 */
@Entity
@Table(name = "variables")
public class Variables implements java.io.Serializable {

	private int id;
	private Categorie categorie;
	private String nom;
	private String type;
	private String description;
	private int min;
	private int max;

	public Variables() {
	}

	public Variables(Categorie categorie, String nom, String type, int min , int max) {
		this.categorie = categorie;
		this.nom = nom;
		this.type = type;
		this.min = min;
		this.max = max;
	}

	public Variables(String x, String y, int z, String a, String b) {
		this.nom=x;
		this.type=y;
		this.min=Integer.parseInt(a);
		this.max=Integer.parseInt(b);
		this.categorie = new Categorie(z);
	}
	public Variables(Object object,String x, String y, Object object2, int i, int j) {
		
		this.id=(int) object;
		this.nom=x;
		this.type=y;
		this.min=i;
		this.max=j;
		this.categorie = new Categorie((int)object2);
	}



	public Variables(Object object,String x, String y, Object object2, int i, int j, Object object3) {
		this.id=(int) object;
		this.nom=x;
		this.type=y;
		this.min=i;
		this.max=j;
		this.categorie = new Categorie((int)object2);
		this.description = (String)object3;
	}

	public Variables(String x, String y, int z, String a, String b, String c) {
		this.nom=x;
		this.type=y;
		this.min=Integer.parseInt(a);
		this.max=Integer.parseInt(b);
		this.categorie = new Categorie(z);
		this.description = c;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true , nullable = false)
	public int getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "categorie", nullable = true)
	public Categorie getCategorie() {
		return this.categorie;
	}

	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}

	@Column(name = "nom", nullable = false, length = 70)
	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	@Column(name = "type", nullable = false, length = 10)
	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	@Column(name = "description", nullable = true, length = 255)
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
	@Column(name = "min", nullable = true)
	public int getMin() {
		return min;
	}

	public void setMin(int min) {
		this.min = min;
	}
	@Column(name = "max", nullable = false)
	public int getMax() {
		return max;
	}

	public void setMax(int max) {
		this.max = max;
	}

	@Override
	public String toString() {
		return "Variables [id=" + id + ", categorie=" + categorie.getNom() + ", nom=" + nom + ", type=" + type + ", min =" + min + ", max=" + max + "]\n";
	}

}
