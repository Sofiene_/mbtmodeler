package com.focus.enicar2017.Transition.util;



import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public class HibernateUtil {

	public static SessionFactory sessionFactory;
	public static String CONFIG_FILE_LOCATION = "hibernate.cfg.xml";
	static {
		try {
			Configuration cfg = new Configuration();
			sessionFactory = cfg.configure(CONFIG_FILE_LOCATION)
					.buildSessionFactory();
		} catch (Throwable ex) {
			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

}
