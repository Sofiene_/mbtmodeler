package com.focus.enicar2017.excel;

import java.awt.Frame;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.util.SystemOutLogger;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.focus.enicar2017.testCase.TestRow;

public class WriteExcel
{
    public static void write(String sheetName,TestRow[] data1,String filename )
    {
        //Blank workbook
        XSSFWorkbook workbook = new XSSFWorkbook(); 
         
        //Create a blank sheet
        XSSFSheet sheet = workbook.createSheet(sheetName);

          
        //This data needs to be written (Object[])
        Map<Integer, TestRow> data = new TreeMap<Integer, TestRow>();
        for (int i =0 ; i < data1.length ; i++) {
        data.put(i, data1[i]);
        }
        //Iterate over data and write to sheet
        Set<Integer> keyset = data.keySet();
        int rownum = 0;
        XSSFCellStyle style = workbook.createCellStyle();
        style.setWrapText(true);
       
        int i = 0;
        for (Integer key : keyset)
        {
            Row row = sheet.createRow(rownum++);
            row.setRowStyle(style);
            TestRow objArr = data.get(key);
            int cellnum = 0;
            Cell cell0 = row.createCell(cellnum++);
            if(i==0) {
            		cell0.setCellValue((String)"ID");
            		i++;}
            else {cell0.setCellValue((int)i++);}
               Cell cell = row.createCell(cellnum++);
                    cell.setCellValue((String)objArr.getPre());
               Cell cell1 = row.createCell(cellnum++);
                    cell1.setCellValue((String)objArr.getProc());
               Cell cell2 = row.createCell(cellnum++);
                    cell2.setCellValue((String)objArr.getPost());
                    Cell cell3 = row.createCell(cellnum++);
                    if(i==1){
                    cell3.setCellValue((String)"Test Result");}
                    else {
                        cell3.setCellValue((String)"not_done");}
                    cell.setCellStyle(style);
                    cell1.setCellStyle(style);
                    cell2.setCellStyle(style);
                    cell3.setCellStyle(style);
            
        }
        try
        {       sheet.autoSizeColumn(0);
        		sheet.autoSizeColumn(1);
        		sheet.autoSizeColumn(2);
        		sheet.autoSizeColumn(3);
            //Write the workbook in file system
        		File f = new File(filename);
        		if(f.exists() && !f.isDirectory()){
        			int x = JOptionPane.showConfirmDialog(Frame.getFrames()[0], "File : "+filename+" Exist.\n Override the file? ", filename, JOptionPane.YES_NO_OPTION);
        			switch (x) {
					case 0:
						FileOutputStream out = new FileOutputStream(f);
			            workbook.write(out);
			            out.close();
			            System.out.println(filename+" written successfully on disk.");
						break;
					default:
						break;
					}
        		}
        		else {
            FileOutputStream out = new FileOutputStream(f);
            workbook.write(out);
            out.close();
            System.out.println(filename+" written successfully on disk.");
        } }
        catch (Exception e) 
        {
        	JOptionPane.showMessageDialog(Frame.getFrames()[0], e.getMessage());
        }
    }
}