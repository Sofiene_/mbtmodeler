package com.focus.enicar2017.excel;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.jpa.criteria.expression.function.SubstringFunction;

import com.focus.enicar2017.Transition.Dao.CategorieDao;
import com.focus.enicar2017.Transition.Dao.VariablesDao;
import com.focus.enicar2017.Transition.model.Categorie;
import com.focus.enicar2017.Transition.model.Variables;

public class ReadExcel
{
	public static HashMap<Integer,Categorie> bdData = new HashMap<Integer,Categorie>();


	public static void generateBD(String[] x) {
		
       for(int i=0;i<x.length;i++){
    	   bdData.put(i,getBdList(x[i],i));
       }
	}
	protected static  Categorie getBdList(String nom,int listNumber) {
		List<Variables> x = new ArrayList<Variables>();
		Categorie categorie = new Categorie();
		try
        {
    		
    		x.clear();
            FileInputStream file = new FileInputStream(new File(nom));
            
 
            //Create Workbook instance holding reference to .xlsx file
            XSSFWorkbook workbook = new XSSFWorkbook(file);
 
            //Get first/desired sheet from the workbook
            XSSFSheet sheet = workbook.getSheetAt(0);
 
            //Iterate through each rows one by one
            Iterator<Row> rowIterator = sheet.iterator();
            rowIterator.next();
            x = new ArrayList<Variables>();
            while (rowIterator.hasNext()) 
            {	
                Row row = rowIterator.next();
                Variables v = new Variables();
                v.setNom(row.getCell(0).toString());
                if (listNumber == 0 ){
                	v.setType(row.getCell(5).toString());
                	x.add(v);
                	//System.out.println(row.toString());
                }
                else if ( listNumber== 1) {
                	v.setType(row.getCell(1).toString());
                	x.add(v);}
                else if (listNumber == 2) {
                	if (row.getCell(1).toString().toLowerCase().contains("input"))
                	{
                		v.setType("Bool�en");
                		x.add(v);
                	}    
                }
                else System.out.println(listNumber);
                
                
 
                
               // System.out.println(v);
                //For each row, iterate through all the columns
//                Iterator<Cell> cellIterator = row.cellIterator();
//                 
//                while (cellIterator.hasNext()) 
//                {
//                    Cell cell = cellIterator.next();
//                    //Check the cell type and format accordingly
//                    switch (cell.getCellType()) 
//                    {
//                        case Cell.CELL_TYPE_NUMERIC:
//                            System.out.print(cell.getNumericCellValue() + "\t");
//                            break;
//                        case Cell.CELL_TYPE_STRING:
//                            System.out.print(cell.getStringCellValue() + "\t");
//                            break;
//                    }
//                }
            }
            file.close();
        } 
        catch (Exception e) 
        {
            System.out.println("Error at reading EXCEL ,"+e.getMessage());
        }
		nom=nom.substring(9, nom.length()-5);
		categorie.setNom(nom);
        categorie.setVariableses(x);
        CategorieDao c = new CategorieDao();
        VariablesDao v = new VariablesDao();
 //      c.persist(categorie);
        Iterator<Variables> i = categorie.getVariableses().iterator();
        while (i.hasNext()) {
        Variables va = new Variables();
        va= i.next();
        va.setCategorie(categorie);
    //    v.persist(va);
        }
		return (categorie);
	}
}