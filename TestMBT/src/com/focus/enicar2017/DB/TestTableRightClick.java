package com.focus.enicar2017.DB;
import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;

import com.mxgraph.examples.swing.GraphEditor;

public class TestTableRightClick {

    protected void initUI(JTable table, int flag) {
        final JPopupMenu popupMenu = new JPopupMenu();
        JMenuItem deleteItem = new JMenuItem("Delete");
        JMenuItem refreshItem = new JMenuItem("Refresh");
        deleteItem.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
            	int x = JOptionPane.showConfirmDialog(DatabaseGUI.frame, "Voulez vous supprimer cet element?","Delete row confirmation", JOptionPane.YES_NO_OPTION);
    			switch (x) {
				case 0:
					if (flag ==1){
					 ((DatabaseGUI.MyTableModel)table.getModel()).removeRow(table.getSelectedRow(),flag);}
					else {((DatabaseGUI.MyTableModel)table.getModel()).removeRowVar(table.getSelectedRow(),flag);}
					break;
				default:
					break;
				}
                      
                       
                        }
        });
        refreshItem.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
            	DatabaseGUI.frame.revalidate();
            	DatabaseGUI.frame.repaint();
            	DatabaseGUI.frame.pack();           	
				//	 table.setModel(new DatabaseGUI.MyTableModel(GraphEditor.to2ArrayCategories(),((DatabaseGUI.MyTableModel)table.getModel()).getColumnNames()));
            }
        });
        popupMenu.add(deleteItem);
        popupMenu.add(refreshItem);
        popupMenu.addPopupMenuListener(new PopupMenuListener() {

            @Override
            public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        int rowAtPoint = table.rowAtPoint(SwingUtilities.convertPoint(popupMenu, new Point(0, 0), table));
                        if (rowAtPoint > -1) {
                            table.setRowSelectionInterval(rowAtPoint, rowAtPoint);
                        }
                    }
                });
            }

            @Override
            public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
                // TODO Auto-generated method stub

            }

            @Override
            public void popupMenuCanceled(PopupMenuEvent e) {
                // TODO Auto-generated method stub

            }
        });
        table.setComponentPopupMenu(popupMenu);
    }

}