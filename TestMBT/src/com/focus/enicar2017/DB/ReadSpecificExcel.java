package com.focus.enicar2017.DB;


import java.awt.Frame;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.swing.JOptionPane;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.jpa.criteria.expression.function.SubstringFunction;

import com.focus.enicar2017.Transition.Dao.CategorieDao;
import com.focus.enicar2017.Transition.Dao.VariablesDao;
import com.focus.enicar2017.Transition.model.Categorie;
import com.focus.enicar2017.Transition.model.Variables;

public class ReadSpecificExcel
{

	protected static  Categorie read(String nom,Categorie categorie,String sheetNumber, String name ,String type, String min , String max,String description) {
		List<Variables> x = new ArrayList<Variables>();
		try
        {
    		
    		x.clear();
            FileInputStream file = new FileInputStream(new File(nom));
            
 
            //Create Workbook instance holding reference to .xlsx file
            XSSFWorkbook workbook = new XSSFWorkbook(file);
            //Get first/desired sheet from the workbook
            XSSFSheet sheet = workbook.getSheetAt(Integer.parseInt(sheetNumber));
 
            //Iterate through each rows one by one
            Iterator<Row> rowIterator = sheet.iterator();
            rowIterator.next();
            x = new ArrayList<Variables>();
            while (rowIterator.hasNext()) 
            {	
                Row row = rowIterator.next();
                Variables v = new Variables();
                v.setNom(row.getCell(Integer.parseInt(name)).toString().replaceAll(" ", "_"));
                if(type.contains("99")){v.setType("int");
                	v.setMin(0);
                	v.setMax(50);}
                else if(type.contains("100")){v.setType("Booleen");
                	v.setMin(0);
                	v.setMax(1);}
                else if(type.contains("101"))
                	{if((int)row.getCell(Integer.parseInt(max)).getNumericCellValue()>1){
                		v.setType("int");
                		v.setMin((int)row.getCell(Integer.parseInt(min)).getNumericCellValue());
                		v.setMax((int)row.getCell(Integer.parseInt(max)).getNumericCellValue());}
                	else {v.setType("Booleen");
                		v.setMin(0);
                		v.setMax(1);}}
                else {v.setType(row.getCell(Integer.parseInt(type)).toString());
                	v.setMin((int)row.getCell(Integer.parseInt(min)).getNumericCellValue());
                	v.setMax((int)row.getCell(Integer.parseInt(max)).getNumericCellValue());}
                if(Integer.parseInt(description)>0){
                	if(row.getCell(Integer.parseInt(description)).toString().isEmpty()||row.getCell(Integer.parseInt(description)).toString()==null)
                		v.setDescription("");
                	else v.setDescription(row.getCell(Integer.parseInt(description)).toString());}
                else
                	v.setDescription("");
                v.setCategorie(categorie);
                x.add(v);
                	
            }
            file.close();
        } 
        catch (Exception e) 
        {
        	JOptionPane.showMessageDialog(Frame.getFrames()[0],"Error at reading EXCEL : "+e.getMessage()+" Cause: "+e.getCause());
        	e.printStackTrace();
        }
        VariablesDao v = new VariablesDao();
        Iterator<Variables> i = x.iterator();
        while (i.hasNext()) {
        Variables va = new Variables();
        va= i.next();
        va.setCategorie(categorie);
       v.persist(va);
        }
		return (categorie);
	}
}