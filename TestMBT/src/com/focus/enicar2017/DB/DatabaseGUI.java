package com.focus.enicar2017.DB;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.RowFilter;
import javax.swing.SpringLayout;
import javax.swing.SwingConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableRowSorter;
import javax.swing.text.PlainDocument;

import com.focus.enicar2017.Transition.Dao.CategorieDao;
import com.focus.enicar2017.Transition.Dao.VariablesDao;
import com.focus.enicar2017.Transition.model.Categorie;
import com.focus.enicar2017.Transition.model.Variables;
import com.focus.enicar2017.input.InputTransition;
import com.focus.enicar2017.textFilters.IntFilter;
import com.mxgraph.examples.swing.GraphEditor;

public class DatabaseGUI extends JPanel {
    private boolean DEBUG = false;
    private JTable table;
    private JTextField filterText;
    private JTextField statusText;
    private JComboBox<String> comboBoxCat;
    private JTextArea descriptionText;
    private JComboBox typeText;
    private JButton addFromExcel;
    protected static JFrame fCategorie;
    protected static JFrame fVariable;
    private JTextField minText;
    Categorie categorie = new Categorie();
    private JButton returnButton;
    private JTextField maxText;
    private TableRowSorter<MyTableModel> sorter;
    protected static JFrame frame;
	String fileName =null;
	Boolean allowOk = true;
    public DatabaseGUI(List<Object[]> data, String[] columns, int flag,Categorie object) {
        super();
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        if(object!= null && flag!=1){
        categorie.setId(object.getId());
        categorie.setNom(object.getNom());
        categorie.setType( object.getType());}
        //Create a table with a sorter.
        MyTableModel model = new MyTableModel(data,columns);
        sorter = new TableRowSorter<MyTableModel>(model);
        table = new JTable(model);
        table.setRowSorter(sorter);
        table.setPreferredScrollableViewportSize(new Dimension(800, 700));
        table.setFillsViewportHeight(true);
        table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        table.getColumnModel().getColumn(0).setMinWidth(200);
        //For the purposes of this example, better to have a single
        //selection.
        table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

        //When selection changes, provide user with row numbers for
        //both view and model.
        if(flag==1){
        	
        table.getSelectionModel().addListSelectionListener(
                new ListSelectionListener() {
                    public void valueChanged(ListSelectionEvent event) {
                        int viewRow = table.getSelectedRow();   
                        if (viewRow < 0) {
                            //Selection got filtered away.
                            statusText.setText("");
                            comboBoxCat.setSelectedIndex(-1);
                        } else {
                        	Object a = table.getValueAt(viewRow, 0);
                        	Object a1 = table.getValueAt(viewRow, 1);
                            statusText.setText((String) a);
                            comboBoxCat.setSelectedItem(a1);
                        }
                    }
                }
        );}
        else {
        	DefaultTableCellRenderer centerRenderer3 = new DefaultTableCellRenderer();
        	centerRenderer3.setHorizontalAlignment( JLabel.CENTER );
        	table.getColumnModel().getColumn(3).setCellRenderer( centerRenderer3 );
        	DefaultTableCellRenderer centerRenderer4 = new DefaultTableCellRenderer();
        	centerRenderer4.setHorizontalAlignment( JLabel.CENTER );
        	table.getColumnModel().getColumn(4).setCellRenderer( centerRenderer4 );
        	table.getSelectionModel().addListSelectionListener(
                    new ListSelectionListener() {
                        public void valueChanged(ListSelectionEvent event) {
                            int viewRow = table.getSelectedRow();   
                            if (viewRow < 0) {
                                //Selection got filtered away.
                                statusText.setText("");
                               // typeText.setText("");
                                typeText.setSelectedIndex(0);
                                minText.setText("");
                                maxText.setText("");
                                descriptionText.setText("");
                            } else {
                            	Object a = table.getValueAt(viewRow, 0);
                            	Object b = table.getValueAt(viewRow, 1);
                            	Object c = table.getValueAt(viewRow, 3);
                            	Object d = table.getValueAt(viewRow, 4);
                            	Object e = table.getValueAt(viewRow, 6);
                                statusText.setText(""+a);
                           //     typeText.setText(""+b);
                                typeText.setSelectedItem(b);
                                minText.setText(c+"");
                                maxText.setText(d+"");
                                descriptionText.setText(e+"");
                            }
                        }
                    }
            );
        }
        if(flag == 1)
        table.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent me) {
                JTable table =(JTable) me.getSource();
                Point p = me.getPoint();
                int row = table.rowAtPoint(p);
                try{
                if (me.getClickCount() == 2) {
                	categorie.setNom((String) table.getValueAt(row, 0));
                	categorie.setType((String) table.getValueAt(row, 1));
                	categorie.setId(Integer.parseInt((String) table.getValueAt(row, 2)));
                	Categorie cat= new Categorie(categorie.getId(),categorie.getNom(),categorie.getType());
                    createAndShowGUI2(cat);
                }}
                catch(Exception e){System.out.println(e.getMessage());}
            }
        });
        //Create the scroll pane and add the table to it.
        JPanel p = new JPanel(new FlowLayout());
        JScrollPane scrollPane = new JScrollPane(table);
        TableColumn typeColumn = table.getColumnModel().getColumn(1);
        JComboBox<String> comboBox = new JComboBox<String>();
        comboBox.addItem("State et Transition");
        comboBox.addItem("State");
        comboBox.addItem("Transition");
        if(flag==1){
        }
        else{
        addFromExcel= new JButton("Add From Excel");

        addFromExcel.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				JPanel generalPanel = new JPanel();
				generalPanel.setLayout(new BoxLayout(generalPanel, BoxLayout.Y_AXIS));
				 JPanel panel1 = new JPanel(new GridLayout(1,2));
				 JPanel myPanel2 = new JPanel(new GridLayout(6,2));
				JButton FileChooser = new JButton("Choose File");
				JLabel j = new JLabel("");
				fileName =null;
				 allowOk = false;
				FileChooser.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						JFileChooser chooser = new JFileChooser();
					    FileNameExtensionFilter filter = new FileNameExtensionFilter(
					    		"Excel", "xlsx", "xls");
					    chooser.setFileFilter(filter);
					    int returnVal = chooser.showOpenDialog(frame);
					    if(returnVal == JFileChooser.APPROVE_OPTION) {
					    	j.setText(chooser.getSelectedFile().getPath());
					    	fileName=chooser.getSelectedFile().getPath();
					    	if(fileName!=null && fileName!="")
					    	allowOk=true;
						
					}}
				});
				myPanel2.add(FileChooser);
				myPanel2.add(j);
				JTextField sheetField = new JTextField(10);
			      PlainDocument doc5 = (PlainDocument) sheetField.getDocument();
			      doc5.setDocumentFilter(new IntFilter());
			     
			      myPanel2.add(new JLabel("Sheet number "));
			      myPanel2.add(sheetField);
			      //SpringUtilities.makeCompactGrid(panel1, 2, 2, 6, 6, 6, 6);
				JTextField nameField = new JTextField(10);
				JTextField typeField = new JTextField(10);
				JTextField minField = new JTextField(10);
				JTextField maxField = new JTextField(10);
				JTextField descriptionField = new JTextField(10);
				PlainDocument doc3 = (PlainDocument) nameField.getDocument();
			      doc3.setDocumentFilter(new IntFilter());

			      PlainDocument doc4 = (PlainDocument) typeField.getDocument();
			      doc4.setDocumentFilter(new IntFilter());
			      PlainDocument doc = (PlainDocument) minField.getDocument();
			      doc.setDocumentFilter(new IntFilter());
			      PlainDocument doc1 = (PlainDocument) maxField.getDocument();
			      doc1.setDocumentFilter(new IntFilter());
			      
			      myPanel2.add(new JLabel("Name column"));
			      myPanel2.add(nameField);
			      myPanel2.add(new JLabel("Type column (99:int,100:Bool,101:Auto)"));
			      myPanel2.add(typeField);
			      myPanel2.add(new JLabel("Min column (0 if none"));
			      myPanel2.add(minField);
			      myPanel2.add(new JLabel("Max column (0 if none)"));
			      myPanel2.add(maxField);
			      panel1.add(new JLabel("Description column (0 if none)"));
			      panel1.add(descriptionField);
			      generalPanel.add(myPanel2);
			      generalPanel.add(panel1);
			      
			//      SpringUtilities.makeCompactGrid(generalPanel, 2, 1, 6, 6, 6, 6);
			    //  myPanel.add(Box.createHorizontalStrut(15));
			       // a spacer
			      Boolean stop = false;
			      while (!stop){
			      int result = JOptionPane.showConfirmDialog(frame, generalPanel, 
			               "Choose Excel File", JOptionPane.OK_CANCEL_OPTION,JOptionPane.PLAIN_MESSAGE,null);
			      if (result == JOptionPane.OK_OPTION) {
			    	  if(typeField.getText().length()>0 && minField.getText().length()>0&& maxField.getText().length()>0 && nameField.getText().length()>0 && allowOk){
			    		  ReadSpecificExcel.read(fileName, categorie, sheetField.getText(), nameField.getText(), typeField.getText(), minField.getText(), maxField.getText(),descriptionField.getText());
			    		  ((DatabaseGUI.MyTableModel)table.getModel()).refresh();
						    stop=true;}
			    	  else {if (!allowOk) {JOptionPane.showMessageDialog(frame, "Please choose a file");}
			    	  		else JOptionPane.showMessageDialog(frame, "Please set all fields");}
			    	}
			      else stop=true;
				}}				
			});
            returnButton=new JButton("Return");
            p.add(returnButton);
            p.add(addFromExcel);
            add(p);
            returnButton.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent arg0) {
			    	if(frame!=null){
			        	frame.dispose();}
			    	frame=fCategorie;
			    	frame.setVisible(true);
				}
			});}

        add(scrollPane);
        //Add the scroll pane to this panel..add(scrollPane);
        //Create a separate form for filterText and statusText
        JPanel form = new JPanel(new SpringLayout());
        JLabel l1 = new JLabel("Filter Text:", SwingConstants.TRAILING);
        form.add(l1);
        filterText = new JTextField();
        //Whenever filterText changes, invoke newFilter.
        filterText.getDocument().addDocumentListener(
                new DocumentListener() {
                    public void changedUpdate(DocumentEvent e) {
                        newFilter();
                    }
                    public void insertUpdate(DocumentEvent e) {
                        newFilter();
                    }
                    public void removeUpdate(DocumentEvent e) {
                        newFilter();
                    }
                });
        l1.setLabelFor(filterText);
        form.add(filterText);
        
        JLabel l2;
        JLabel lNom;
        JLabel lType;
        JLabel lMin;
        JLabel lMax;
        JLabel lDescription;
        new TestTableRightClick().initUI(table,flag);
        
        JPanel form1 = new JPanel(new SpringLayout());
        JButton addLineButton = new JButton("Add Category");
        form1.add(addLineButton);
        JButton deleteButton = new JButton("Delete Selection");
        form1.add(deleteButton);
        JButton updateButton = new JButton("Update");
        form1.add(updateButton);
        
        
        
        if(flag==1){
        comboBoxCat = new JComboBox<String>();
        comboBoxCat.addItem("State et Transition");
        comboBoxCat.addItem("Transition");
        comboBoxCat.addItem("State");
        JLabel l3 = new JLabel("Type : ",SwingConstants.TRAILING);
        l2 = new JLabel("Categorie:", SwingConstants.TRAILING);
        form.add(l2);
        statusText = new JTextField();
        l2.setLabelFor(statusText);
        form.add(statusText);
        form.add(l3);
        l2.setLabelFor(comboBoxCat);
        form.add(comboBoxCat);
        SpringUtilities.makeCompactGrid(form, 3, 2, 6, 6, 6, 6);
        add(form);

        updateButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				try{
				int viewRow = table.getSelectedRow();
				MyTableModel data=((DatabaseGUI.MyTableModel)table.getModel());
				 String a = statusText.getText();
				 if(a.indexOf(" ")!= -1){
				 if(a.indexOf(" ")==a.lastIndexOf(" ")){
				 table.setValueAt(statusText.getText(),viewRow, 0);
				 table.setValueAt(comboBoxCat.getSelectedItem(),viewRow, 1);
				 data.updateCat(new Categorie(Integer.parseInt((String)data.getValueAt(viewRow, 2)),(String)data.getValueAt(viewRow, 0),(String)data.getValueAt(viewRow,1)));
				 }
				 else JOptionPane.showMessageDialog(frame, "Categorie must Contain 1 space");
				 }}
				catch(Exception e){
				System.out.println("Update Error : "+e.getMessage());e.printStackTrace();	
				}
			}
		});}
        else{
        	addLineButton.setText("Add Variable");
        	lNom = new JLabel("Variable:", SwingConstants.TRAILING);
            form.add(lNom);
            statusText = new JTextField();
            lNom.setLabelFor(statusText);
            form.add(statusText);
            lType = new JLabel("Type:", SwingConstants.TRAILING);
            form.add(lType);
          //  typeText = new JTextField();
            typeText = new JComboBox(new Object[]{"int", "bool�en" });
            lType.setLabelFor(typeText);
            form.add(typeText);
            lMin = new JLabel("Min:", SwingConstants.TRAILING);
            form.add(lMin);
            minText = new JTextField();
            lMin.setLabelFor(minText);
            form.add(minText);
            lMax = new JLabel("Max:", SwingConstants.TRAILING);
            form.add(lMax);
            maxText = new JTextField();
            lMax.setLabelFor(maxText);
            form.add(maxText);
            lDescription = new JLabel("Description:", SwingConstants.TRAILING);
            form.add(lDescription);
            descriptionText = new JTextArea(2,122);
            lDescription.setLabelFor(descriptionText);
            form.add(descriptionText);
            PlainDocument doc1 = (PlainDocument) minText.getDocument();
		      doc1.setDocumentFilter(new IntFilter());
		      PlainDocument doc2 = (PlainDocument) maxText.getDocument();
		      doc2.setDocumentFilter(new IntFilter());
            SpringUtilities.makeCompactGrid(form, 6, 2, 6, 6, 6, 6);
            add(form);
        	 updateButton.addActionListener(new ActionListener() {
     			
     			@Override
     			public void actionPerformed(ActionEvent arg0) {
     				try{
     				int viewRow = table.getSelectedRow();
     				 String a = statusText.getText();
     			//	String b = typeText.getText();
     				 String b = (String) typeText.getSelectedItem();
     				String c = minText.getText();
     				String d = maxText.getText();
     				if(Integer.parseInt(d)<Integer.parseInt(c)||Integer.parseInt(d)==0){
     					JOptionPane.showMessageDialog(frame, "Max doit �tre superieur au Min et Max doit �tre different de 0.");
     				}else{
     					if(!b.toLowerCase().contains("int") && !b.toLowerCase().contains("bool") && b.indexOf(" ")!=-1){
     						JOptionPane.showMessageDialog(frame, "Type must contain int or bool");
     					}
     					else
     						if(a.indexOf(" ")== -1){
     							int id = (int) ((DatabaseGUI.MyTableModel)table.getModel()).getValueAt(viewRow, 5);
     		     				 table.setValueAt(statusText.getText(),viewRow, 0);
     		     				//table.setValueAt(typeText.getText(),viewRow, 1);
     		     				table.setValueAt((String) typeText.getSelectedItem(),viewRow,1);
     		     				table.setValueAt(minText.getText(),viewRow, 3);
     		     				table.setValueAt(maxText.getText(),viewRow, 4);
     		     				table.setValueAt(descriptionText.getText(),viewRow, 6);
     		     			//	((DatabaseGUI.MyTableModel)table.getModel()).updateVar(new Variables(id,statusText.getText(),typeText.getText(),categorie.getId(),Integer.parseInt(minText.getText()),Integer.parseInt(maxText.getText())));
     		     				((DatabaseGUI.MyTableModel)table.getModel()).updateVar(new Variables(id,statusText.getText(),(String) typeText.getSelectedItem(),categorie.getId(),Integer.parseInt(minText.getText()),Integer.parseInt(maxText.getText()),descriptionText.getText()));
     						}
     		     				 else JOptionPane.showMessageDialog(frame, "Variable must not Contain any space.");
     		     				 }
     				}
     				 
     				catch(Exception e){
     					
     				}
     			}
     		});
        	}
deleteButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				try{
					int[] viewRow = table.getSelectedRows();
					int x = JOptionPane.showConfirmDialog(frame, "Vous allez supprimer "+viewRow.length +"Element(s)\nProceder � la suppression ?","Delete multiple rows confirmation", JOptionPane.YES_NO_OPTION);
        			switch (x) {
					case 0:if(flag==1){
						  ((DatabaseGUI.MyTableModel)table.getModel()).removeRows(viewRow);}
					else {((DatabaseGUI.MyTableModel)table.getModel()).removeRowsVar(viewRow);}
						  clearSel(flag);
						break;
					default:
						break;
					}

				}
				catch(Exception e){
					System.out.println("ERROR deleteButton "+e.getMessage());
				}
			}
		});
if(flag == 1)
addLineButton.addActionListener(new ActionListener() {
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		try{
			JTextField catField = new JTextField(20);

		      JPanel myPanel = new JPanel();
		      myPanel.add(new JLabel("Categorie :"));
		      myPanel.add(catField);
		      myPanel.add(Box.createHorizontalStrut(15)); // a spacer
		      myPanel.add(new JLabel("Type :"));
		      myPanel.add(comboBox);
		      Boolean stop = false;
		      while (!stop){
		      int result = JOptionPane.showConfirmDialog(frame, myPanel, 
		               "add Database", JOptionPane.OK_CANCEL_OPTION);
		      if (result == JOptionPane.OK_OPTION) {
		    	  if(catField.getText().indexOf(" ")==catField.getText().lastIndexOf(" ")&& catField.getText().indexOf(" ")!=-1){
		         ((MyTableModel)table.getModel()).addRow(catField.getText(),comboBox.getSelectedItem(),flag);
		    	  stop=true;}
		    	  else {
		    		  JOptionPane.showMessageDialog(frame, "Categorie must contain 1 space");
		    	  }
		      }
		      else stop=true;
			}}

		catch(Exception e){
			
		}
	}
});
else 
	addLineButton.addActionListener(new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			try{
				JTextField nomField = new JTextField(20);
				//JTextField typeField = new JTextField(20);
				JComboBox typeField = new JComboBox(new Object[]{"int", "bool�en",});
				JTextField minField = new JTextField(20);
				JTextField maxField = new JTextField(20);
				JTextArea descriptionField = new JTextArea(2,122);
			      PlainDocument doc = (PlainDocument) minField.getDocument();
			      doc.setDocumentFilter(new IntFilter());
			      PlainDocument doc1 = (PlainDocument) maxField.getDocument();
			      doc1.setDocumentFilter(new IntFilter());
			      JPanel myPanel = new JPanel(new SpringLayout());
			     
			      myPanel.add(new JLabel("Nom         :"));
			      myPanel.add(nomField);
			      myPanel.add(new JLabel("Type        :"));
			      myPanel.add(typeField);
			      myPanel.add(new JLabel("Min         :"));
			      myPanel.add(minField);
			      myPanel.add(new JLabel("Max         :"));
			      myPanel.add(maxField);
			      myPanel.add(new JLabel("Description :"));
			      myPanel.add(descriptionField);
			      SpringUtilities.makeCompactGrid(myPanel, 5, 2, 6, 6, 6, 6);
			      myPanel.add(Box.createHorizontalStrut(15));
			       // a spacer
			      Boolean stop = false;
			      while (!stop){
			      int result = JOptionPane.showConfirmDialog(frame, myPanel, 
			               "add Variable", JOptionPane.OK_CANCEL_OPTION);
			      if (result == JOptionPane.OK_OPTION) {
			    	//  if(!typeField.getText().toLowerCase().contains("int")&&!typeField.getText().toLowerCase().contains("bool") && typeField.getText().indexOf(" ")==-1){
			    	  if(!((String)typeField.getSelectedItem()).toLowerCase().contains("int")&&!((String)typeField.getSelectedItem()).toLowerCase().contains("bool") && ((String)typeField.getSelectedItem()).indexOf(" ")==-1){  
			    	  JOptionPane.showMessageDialog(frame, "type invalid");
			    	  }else
			    	  {
			    		  if(minField.getText().length()<1){JOptionPane.showMessageDialog(frame, "type invalid");}
			    		  else {
			    			  if(nomField.getText().indexOf(" ")==-1){
						         //((MyTableModel)table.getModel()).addRow(nomField.getText(),typeField.getText(),categorie.getId(),minField.getText(),maxField.getText(),flag);
			    				  ((MyTableModel)table.getModel()).addRow(nomField.getText(),((String)typeField.getSelectedItem()),categorie.getId(),minField.getText(),maxField.getText(),descriptionField.getText(),flag);
							   
			    				  stop=true;}
			    			  else {
						    		  JOptionPane.showMessageDialog(frame, "Variables must not contain any space");
						    	  }
			    			  }
			    	  }
			    	  
			      }
			      else stop=true;
				}}

			catch(Exception e){
				
			}
		}
	});
        add(form1);
        SpringUtilities.makeCompactGrid(form1, 1, 3, 6, 6, 6, 6);
    }
     void clearSel(int flag){
    	table.clearSelection();
    	if(flag ==1){
    	statusText.setText("");
    	comboBoxCat.setSelectedIndex(-1);}
    	else {maxText.setText("");
    	minText.setText("");
    	statusText.setText("");
    	descriptionText.setText("");
    //	typeText.setText("");
    	typeText.setSelectedIndex(0);
    	
    	}
    }
    /** 
     * Update the row filter regular expression from the expression in
     * the text box.
     */
    private void newFilter() {
        RowFilter<MyTableModel, Object> rf = null;
        //If current expression doesn't parse, don't update.
        try {
            rf = RowFilter.regexFilter(filterText.getText(), 0);
        } catch (java.util.regex.PatternSyntaxException e) {
            return;
        }
        sorter.setRowFilter(rf);
    }




    protected class MyTableModel extends AbstractTableModel {
        public String[] getColumnNames() {
			return columnNames;
		}


		public void setColumnNames(String[] columnNames) {
			this.columnNames = columnNames;
		}
		private String[] columnNames  ;
        private CategorieDao categorieDao = new CategorieDao ();
        private VariablesDao variableDao = new VariablesDao();
        protected List<Object[]> data = new ArrayList<Object[]>();
        
        public MyTableModel (List<Object[]> a,String[] s)
        {
        	columnNames = new String[s.length];
        	columnNames = s;
        	data=a;
        }

        public int getColumnCount() {
            return columnNames.length;
        }

        public int getRowCount() {
            return data.size();
        }

        public String getColumnName(int col) {
            return columnNames[col];
        }

        public Object getValueAt(int row, int col) {
            return data.get(row)[col];
        }

        /*
         * JTable uses this method to determine the default renderer/
         * editor for each cell.  If we didn't implement this method,
         * then the last column would contain text ("true"/"false"),
         * rather than a check box.
         */
        public Class getColumnClass(int c) {
        	try{
            return getValueAt(0, c).getClass();}
        	catch(Exception e){
        		return String.class;
        	}
        }

        /*
         * Don't need to implement this method unless your table's
         * editable.
         */
        public boolean isCellEditable(int row, int col) {
            //Note that the data/cell address is constant,
            //no matter where the cell appears onscreen.
//            if (col < 1) {
                return false;
//            } else {
//                return true;
//            }
        }

        /*
         * Don't need to implement this method unless your table's
         * data can change.
         */
        public void setValueAt(Object value, int row, int col) {
            if (DEBUG) {
                System.out.println("Setting value at " + row + "," + col
                                   + " to " + value
                                   + " (an instance of "
                                   + value.getClass() + ")");
            }

            data.get(row)[col] = value;
            fireTableCellUpdated(row, col);


            if (DEBUG) {
                System.out.println("New value of data:");
                printDebugData();
            }
        }

        private void printDebugData() {
            int numRows = getRowCount();
            int numCols = getColumnCount();

            for (int i=0; i < numRows; i++) {
                System.out.print("    row " + i + ":");
                for (int j=0; j < numCols; j++) {
                    System.out.print("  " + data.get(i)[j]);
                }
                System.out.println();
            }
            System.out.println("--------------------------");
        }

		public void removeRow(int i,int flag) {
			try{
			Categorie cat = new Categorie(Integer.parseInt((String)data.get(i)[2]),(String)data.get(i)[0],(String)data.get(i)[1]);
			

			if(variableDao.findByCat(cat.getId()).size()>0){
				JOptionPane.showMessageDialog(frame, "Categorie "+(String)data.get(i)[0]+" still contain Variables.Please delete them before proceeding");
			}
			else {
				categorieDao.delete(cat);
				data.clear();
				GraphEditor.refreshCategories();
				data=GraphEditor.to2ArrayCategories();
			}
            fireTableDataChanged();
			frame.revalidate();
			frame.repaint();
			frame.pack();
			clearSel(flag);}
			catch(Exception e){
				System.out.println("Error : "+e.getMessage());
			}
			}
		public void removeRowVar(int i,int flag) {
			Variables variable = new Variables((int)data.get(i)[5],(String)data.get(i)[0],(String)data.get(i)[1],(int)data.get(i)[2],(int)data.get(i)[3],(int)data.get(i)[4],(String)data.get(i)[6]);
			variableDao.delete(variable);

			data.clear();
			refresh();
            fireTableDataChanged();
			frame.revalidate();
			frame.repaint();
			frame.pack();
			clearSel(flag);
			
			}
		public void addRow(String x , Object y,int flag) {
			Categorie categorie = new Categorie(x,(String)y);
			categorieDao.persist(categorie);
			data.clear();
			GraphEditor.refreshCategories();
			data=GraphEditor.to2ArrayCategories();
            fireTableDataChanged();
			frame.revalidate();
			frame.repaint();
			frame.pack();
			clearSel(flag);
			
			}
		public void addRow(String x , String y, int z , String a , String b,String c, int flag) {
			Variables variable = new Variables(x,y,z,a,b,c);
			variableDao.persist(variable);
			data.clear();
			refresh();
            fireTableDataChanged();
			frame.revalidate();
			frame.repaint();
			frame.pack();

			clearSel(flag);
			
			}
		public void removeRows(int[] i) {
			for (int j=i.length-1;j>=0;j--){
				Categorie cat = new Categorie(Integer.parseInt((String)data.get(i[j])[2]),(String)data.get(i[j])[0],(String)data.get(i[j])[1]);
				if(variableDao.findByCat(cat.getId()).size()>0){
					JOptionPane.showMessageDialog(frame, "Categorie "+(String)data.get(i[j])[0]+" still contain Variables.Please delete them before proceeding");
				}
				else {
					categorieDao.delete(cat);
					data.clear();
					GraphEditor.refreshCategories();
					data=GraphEditor.to2ArrayCategories();
				}}
            fireTableDataChanged();
			frame.revalidate();
			frame.repaint();
			frame.pack();
			
			}
		public void removeRowsVar(int[] i) {
			for (int j=i.length-1;j>=0;j--){
				
			Variables variable = new Variables(data.get(i[j])[5],(String)data.get(i[j])[0],(String)data.get(i[j])[1],data.get(i[j])[2],(int)data.get(i[j])[3],(int)data.get(i[j])[4],data.get(i[j])[6]);
			variableDao.delete(variable);
			data.clear();
			refresh();}
            fireTableDataChanged();
			frame.revalidate();
			frame.repaint();
			frame.pack();
			
			}
		public void updateCat(Categorie cat)
		{
			categorieDao.merge(cat);
			data.clear();
			GraphEditor.refreshCategories();
			data=GraphEditor.to2ArrayCategories();
            fireTableDataChanged();
			frame.revalidate();
			frame.repaint();
			frame.pack();
		}
		public void updateVar(Variables variable)
		{
			variableDao.merge(variable);
			data.clear();
			refresh();
            fireTableDataChanged();
			frame.revalidate();
			frame.repaint();
			frame.pack();
			
		}
		public void refresh() {
			try {
			GraphEditor.refreshCategories();
			List<Object[]> b = new ArrayList<Object[]>();
			List<Variables> mo = InputTransition.getVariablesList(categorie.getNom());
	        if(mo!=null && mo.size()>0){
	        for (int i=0;i<mo.size();i++){
	        Object[] l = new Object[7];
	        	l[0]=mo.get(i).getNom();
	        	l[1]=mo.get(i).getType();
	        	l[2]=mo.get(i).getCategorie().getId();
	        	l[3]=mo.get(i).getMin();
	        	l[4]=mo.get(i).getMax();
	        	l[5]=mo.get(i).getId();
	        	l[6]=mo.get(i).getDescription();
	        			
	        b.add(l);
	        }}
	        ((MyTableModel)table.getModel()).data=b;
            fireTableDataChanged();
	        }
			catch (Exception e){e.printStackTrace();}
			finally {
			frame.revalidate();
			frame.repaint();
			frame.pack();}
			
		}
    }

    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     */
    public static void createAndShowGUI() {
        //Create and set up the window.
        //frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	fCategorie = new JFrame("Database");
    	frame=fCategorie;
        //Create and set up the content pane.
        String[] s = {"Categorie(Name must contain 1 space)","Type","ID"};
        DatabaseGUI newContentPane = new DatabaseGUI(GraphEditor.to2ArrayCategories(),s,1,null);
        newContentPane.setOpaque(true); //content panes must be opaque
        frame.setContentPane(newContentPane);

        //Display the window.
        frame.pack();
        frame.setVisible(true);
    }
    public static void createAndShowGUI2(Categorie categorie) {
    	//Hide previous frame
    	frame.setVisible(false);
        //Create and set up the window.
    	fVariable = new JFrame(categorie.getNom());
        frame=fVariable;
        System.out.println("ID = "+categorie.getId());
        //frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        String[] s = {"Nom","Type","Categorie","Min","Max","ID","Description"};
        //Create and set up the content pane.
        List<Object[]> b = new ArrayList<Object[]>();
        List<Variables> mo = InputTransition.getVariablesList(categorie.getNom());
        if(mo!=null && mo.size()>0){
        for (int i=0;i<mo.size();i++){
        Object[] l = new Object[s.length];
        	l[0]=mo.get(i).getNom();
        	l[1]=mo.get(i).getType();
        	l[2]=mo.get(i).getCategorie().getId();
        	l[3]=mo.get(i).getMin();
        	l[4]=mo.get(i).getMax();
        	l[5]=mo.get(i).getId();
        	l[6]=mo.get(i).getDescription();
        			
        b.add(l);
        }}
        DatabaseGUI newContentPane = new DatabaseGUI(b,s,0,categorie);
        newContentPane.setOpaque(true); //content panes must be opaque
        frame.setContentPane(newContentPane);
        //Display the window.
        frame.pack();
        frame.setVisible(true);

    }
}