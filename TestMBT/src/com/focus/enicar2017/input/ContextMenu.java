package com.focus.enicar2017.input;

import javax.swing.*;
import javax.swing.text.JTextComponent;
import javax.swing.undo.UndoManager;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class ContextMenu extends JPopupMenu
{
    private Clipboard clipboard;

    private UndoManager undoManager;

    private JMenuItem undo;
    private JMenuItem redo;
    private JMenuItem cut;
    private JMenuItem copy;
    private JMenuItem paste;
    private JMenuItem delete;
    private JMenuItem selectAll;
    private JMenuItem remove;

    private JTextComponent jTextComponent;

    public ContextMenu(JPanel panel , GridBagLayout layout)
    {
        undoManager = new UndoManager();
        clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();

        undo = new JMenuItem("Undo");
        undo.setEnabled(false);
        undo.setAccelerator(KeyStroke.getKeyStroke("control Z"));
        undo.addActionListener(event -> undoManager.undo());

        add(undo);

        redo = new JMenuItem("Redo");
        redo.setEnabled(false);
        redo.setAccelerator(KeyStroke.getKeyStroke("control Y"));
        redo.addActionListener(event -> undoManager.redo());

        add(redo);

        add(new JSeparator());

        cut = new JMenuItem("Cut");
        cut.setEnabled(false);
        cut.setAccelerator(KeyStroke.getKeyStroke("control X"));
        cut.addActionListener(event -> jTextComponent.cut());

        add(cut);

        copy = new JMenuItem("Copy");
        copy.setEnabled(false);
        copy.setAccelerator(KeyStroke.getKeyStroke("control C"));
        copy.addActionListener(event -> jTextComponent.copy());

        add(copy);

        paste = new JMenuItem("Paste");
        paste.setEnabled(false);
        paste.setAccelerator(KeyStroke.getKeyStroke("control V"));
        paste.addActionListener(event -> jTextComponent.paste());

        add(paste);

        delete = new JMenuItem("Delete");
        delete.setEnabled(false);
        delete.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0));
        delete.addActionListener(event -> jTextComponent.replaceSelection(""));

        add(delete);

        add(new JSeparator());

        selectAll = new JMenuItem("Select All");
        selectAll.setEnabled(false);
        selectAll.setAccelerator(KeyStroke.getKeyStroke("control A"));
        selectAll.addActionListener(event -> jTextComponent.selectAll());

        add(selectAll);
        
        add(new JSeparator());

        remove = new JMenuItem("remove");
        remove.setEnabled(true);
        remove.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_BACK_SPACE,0));
        remove.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				int index=getComponentIndex(jTextComponent);
				int  flag = 0;
				int number=0;
				Component target = panel.getComponent(index-1);
				Component[] c = panel.getComponents();
				if(target instanceof JLabel && ((JLabel) target).getText().contains("or")){
					flag=1;
				}
				else {
					for (int i =0; i<c.length;i++) {
					GridBagConstraints x = new GridBagConstraints();
					x=layout.getConstraints(c[i]);
					if(x.gridy==layout.getConstraints(jTextComponent).gridy && c[i] instanceof JTextField)
						number++;
					}
				
					if(number>1) flag =2; }
				if(flag==1){
					
					for (int i =0; i<c.length;i++) {
						GridBagConstraints x = new GridBagConstraints();
						x=layout.getConstraints(c[i]);
						if((x.gridx>layout.getConstraints(jTextComponent).gridx)&&(x.gridy==layout.getConstraints(jTextComponent).gridy)&& x.gridx<24)
						{
							x.gridx-=2;
							layout.setConstraints(c[i], x);				
						}
					}		
					panel.remove(jTextComponent);
					panel.remove(panel.getComponent(index-1));
					panel.revalidate();
					panel.repaint();
					((Window) panel.getParent().getParent().getParent().getParent()).pack();
					}	
				else if (flag==2){
					for (int i =0; i<c.length;i++) {
						GridBagConstraints x = new GridBagConstraints();
						x=layout.getConstraints(c[i]);
						if(c[i] instanceof JLabel && ((JLabel) c[i]).getText().contains("or") && x.gridy==layout.getConstraints(jTextComponent).gridy&&x.gridx<24)
							{panel.remove(c[i]);
							break;}
					}
					for (int i =0; i<c.length;i++) {
						GridBagConstraints x = new GridBagConstraints();
						x=layout.getConstraints(c[i]);
						if((x.gridx>layout.getConstraints(jTextComponent).gridx)&&(x.gridy==layout.getConstraints(jTextComponent).gridy))
						{
							x.gridx-=2;
							layout.setConstraints(c[i], x);				
						}
					}		
					panel.remove(jTextComponent);
					panel.revalidate();
					panel.repaint();
					((Window) panel.getParent().getParent().getParent().getParent()).pack();
				}
				}	
			
		});

        add(remove);
    }
    public final int getComponentIndex(Component component) {
        if (component != null && component.getParent() != null) {
          Container c = component.getParent();
          for (int i = 0; i < c.getComponentCount(); i++) {
            if (c.getComponent(i) == component)
              return i;
          }
        }

        return -1;
      }
    public void add(JTextComponent jTextComponent)
    {
        jTextComponent.addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyPressed(KeyEvent pressedEvent)
            {
                if ((pressedEvent.getKeyCode() == KeyEvent.VK_Z)
                        && ((pressedEvent.getModifiers() & KeyEvent.CTRL_MASK) != 0))
                {
                    if (undoManager.canUndo())
                    {
                        undoManager.undo();
                    }
                }

                if ((pressedEvent.getKeyCode() == KeyEvent.VK_Y)
                        && ((pressedEvent.getModifiers() & KeyEvent.CTRL_MASK) != 0))
                {
                    if (undoManager.canRedo())
                    {
                        undoManager.redo();
                    }
                }
            }
        });

        jTextComponent.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseReleased(MouseEvent releasedEvent)
            {
                if (releasedEvent.getButton() == MouseEvent.BUTTON3)
                {
                    processClick(releasedEvent);
                }
            }
        });

        jTextComponent.getDocument().addUndoableEditListener(event -> undoManager.addEdit(event.getEdit()));
    }

    private void processClick(MouseEvent event)
    {
        jTextComponent = (JTextComponent) event.getSource();
        jTextComponent.requestFocus();

        boolean enableUndo = undoManager.canUndo();
        boolean enableRedo = undoManager.canRedo();
        boolean enableCut = false;
        boolean enableCopy = false;
        boolean enablePaste = false;
        boolean enableDelete = false;
        boolean enableSelectAll = false;

        String selectedText = jTextComponent.getSelectedText();
        String text = jTextComponent.getText();

        if (text != null)
        {
            if (text.length() > 0)
            {
                enableSelectAll = true;
            }
        }

        if (selectedText != null)
        {
            if (selectedText.length() > 0)
            {
                enableCut = true;
                enableCopy = true;
                enableDelete = true;
            }
        }

        if (clipboard.isDataFlavorAvailable(DataFlavor.stringFlavor) && jTextComponent.isEnabled())
        {
            enablePaste = true;
        }

        undo.setEnabled(enableUndo);
        redo.setEnabled(enableRedo);
        cut.setEnabled(enableCut);
        copy.setEnabled(enableCopy);
        paste.setEnabled(enablePaste);
        delete.setEnabled(enableDelete);
        selectAll.setEnabled(enableSelectAll);

        show(jTextComponent, event.getX(), event.getY());
    }
}