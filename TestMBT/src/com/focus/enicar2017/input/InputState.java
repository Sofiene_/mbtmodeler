package com.focus.enicar2017.input;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JViewport;
import javax.swing.text.PlainDocument;

import org.apache.lucene.util.automaton.RegExp;

import com.focus.enicar2017.Transition.Dao.VariablesDao;
import com.focus.enicar2017.Transition.model.Variables;
import com.focus.enicar2017.textFilters.BoolFilter;
import com.focus.enicar2017.textFilters.IntFilter;
import com.mxgraph.examples.swing.GraphEditor;
import com.mxgraph.swing.view.mxCellEditor;

@SuppressWarnings("serial")
public class InputState extends JFrame {
	String Text ;
	
public String getText() {
		return Text;
}

public void setText(String text) {
		Text = text;
}


Map<Double, List<Component>> Elements = new HashMap<Double, List<Component>>();
GridBagLayout layout = new GridBagLayout();
JPanel panel  = new JPanel(layout);
JPanel panelBot  = new JPanel(new FlowLayout());
JPanel panelTop  = new JPanel(new FlowLayout());
JPanel panelMid  = new JPanel(new FlowLayout());
JPanel panelDebut  = new JPanel(new GridLayout(2,1));
String DescriptionText="";
public JButton save = new JButton("Save");
JButton addLine = new JButton("New Line");
JButton cancel = new JButton("Cancel");
JButton delete = new JButton("Delete");
JLabel labelOr = new JLabel(" or");
JList<Component> a = new JList<Component>();
List<Integer> lDelete = new ArrayList<Integer>();
GridBagConstraints cCheck = new GridBagConstraints();
GridBagConstraints cList1 = new GridBagConstraints();
GridBagConstraints cList2 = new GridBagConstraints();
GridBagConstraints cOp = new GridBagConstraints();
GridBagConstraints cBtAdd = new GridBagConstraints();
GridBagConstraints cText = new GridBagConstraints();
GridBagConstraints clabelEgal = new GridBagConstraints();
GridBagConstraints clabelOr = new GridBagConstraints();
int rowNumber=-1;
VariablesDao variablesDao = new VariablesDao();
String[] dbList ;
String[] opList ={"AND","OR"};
String[] difList ={"==",">=","<="};
String[] listItems;
JComboBox<?> theOpList;
JComboBox<?> theDifList;
JComboBox<?> theList;
JComboBox<String> theList2;
int chosen=0;
Boolean valAdded= false;
protected Boolean allTextFieldsAreSet= true;
protected Boolean nullValueSelected= false;
int newLineOpPosition=30;
public ActionListener action;
ActionListener action1;
int ChosenElement = 0;
JTextField Description = new JTextField();

public InputState (JTextArea textArea, mxCellEditor mxCellEditor) {
	mxCellEditor.numberOfInterfaces++;
	this.setLayout(new BorderLayout());
	this.add(panelDebut,BorderLayout.PAGE_START);
    this.add(panel,BorderLayout.CENTER);
    this.add(panelBot,BorderLayout.PAGE_END);

    dbList = GraphEditor.to1ArrayCategories("state");
    //System.out.println(dbList);
    theList = new JComboBox<Object>(dbList) ;
    delete.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent arg0) {
					Component[] c = panel.getComponents();
					//adds number of lines to delete
 		        	for (int i=0;i<c.length;i++){
		        	if(c[i] instanceof JCheckBox) {
		        		if (((JCheckBox) c[i]).isSelected()){
		        			lDelete.add(layout.getConstraints(c[i]).gridy);
		        		}
		        	}
					
				}//remove those lines
 		        	for (int i=0;i<c.length;i++){
 		        		Iterator<Integer> iterator = lDelete.iterator();
 		        		while (iterator.hasNext()) {
 		        			if(layout.getConstraints(c[i]).gridy==iterator.next())
 		        			{
 		        			panel.remove(c[i]);
 		        			}
 		        		}
 		        	}
 		        	Component[] c2 = panel.getComponents();
 		        	//remove last operand
 		        	for (int i=0;i<c2.length;i++){
 		        	if((layout.getConstraints(c2[i]).gridy == layout.getConstraints(c2[c2.length-1]).gridy)&&layout.getConstraints(c2[i]).gridx==33){
 		        		panel.remove(c2[i]);
 		        	}
 		        	}
					revalidate();
					repaint();
					pack();
				}
			});
		    cancel.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent arg0) {
					dispose();
					 mxCellEditor.numberOfInterfaces--;
					 mxCellEditor.stopEditing(false, true);
					
				}
			});
/*
 * Action1 for list 2
 */
			action1 = new ActionListener() {
			    @SuppressWarnings("unchecked")
				public void actionPerformed(ActionEvent e) {
			    	Component source = (Component) e.getSource();
			    	Component[] c = panel.getComponents();
			    	JScrollPane targetscroll = new JScrollPane();
			    	JComboBox<String> target = new JComboBox<String>();
			    	for (int i=0;i<c.length;i++)
		        	{
			    	if(c[i] instanceof JScrollPane)
					{target = new JComboBox<String>();
					targetscroll = new JScrollPane();
						targetscroll = (JScrollPane )c[i];
			        	JViewport targetview = targetscroll.getViewport();
			            target = (JComboBox<String>) targetview.getComponents()[0];
			            if (target.getSelectedItem()==((JComboBox<String>)source).getSelectedItem()&& target==source) {
			            	i=c.length;
			            	break;
			            }
			           			            
					}}
			    	
			    	if (isBool((String)((JComboBox)source).getSelectedItem()))
			    	{
			        	try
			        	{
				        	for (int i=0;i<c.length;i++)
				        	{
				        	if(layout.getConstraints(c[i]).gridy==layout.getConstraints(targetscroll).gridy) 
				        		{
				        		if(c[i] instanceof JTextField)
				        		{	((JTextField)c[i]).setText("0");
								    PlainDocument doc = (PlainDocument) ((JTextField)c[i]).getDocument();
								    doc.setDocumentFilter(new BoolFilter());
				        			}
				        		}
				        	}
			        	}
			        	catch(Exception exp) {
			        		exp.printStackTrace();
			        	}
			    	}
			    	
			    else {
			    	try
			    	{
			        	for (int i=0;i<c.length;i++)
			        	{
			        	if(layout.getConstraints(c[i]).gridy==layout.getConstraints(targetscroll).gridy) 
			        		{
			        		if(c[i] instanceof JTextField)
			        			{((JTextField)c[i]).setText("0");
							    PlainDocument doc = (PlainDocument) ((JTextField)c[i]).getDocument();
							    doc.setDocumentFilter(new IntFilter());
			        			}
			        		}
			        	}
			    	}
			    	catch(Exception exp) {
			    		exp.printStackTrace();
			    	}
			    }
			   }
			};
/*
 * Action for save
 */
		    action = new ActionListener() {
				
				@SuppressWarnings("unchecked")
				@Override
				public void actionPerformed(ActionEvent arg0) {
					Component[]c =panel.getComponents();
					Text="";
					if(Description.getText().length()>0){
					Text=Description.getText()+"\n";}
					int l = 0 ;
					while(l<c.length && allTextFieldsAreSet){
						if(c[l] instanceof JTextField) 
						{	if (((JTextField)c[l]).getText().length()<1){
							allTextFieldsAreSet = false;
						}
							
						}
						if(c[l] instanceof JComboBox) 
						{
							if(((JComboBox<?>) c[l]).getSelectedItem()==null)
								nullValueSelected = true;
						}
						if(c[l] instanceof JList) 
						{
							if(((JList<?>) c[l]).getSelectedValue()==null);
							nullValueSelected = true;
						}
						if(c[l] instanceof JScrollPane)
						{
							JScrollPane targetscroll = (JScrollPane )c[l];
				        	JViewport targetview = targetscroll.getViewport();
				            JComboBox<String> target = (JComboBox<String>) targetview.getComponents()[0];
				            if(target.getSelectedItem()==null){
				            	nullValueSelected=true;
				            }
				            
						}
						l++;
					}
					if(allTextFieldsAreSet && !nullValueSelected) {
					for(int j =0 ; j<=rowNumber;j++)
					{
						for(int k=1;k<=40;k++)
							{
							for (int i=0;i<c.length;i++)
							{
								if(layout.getConstraints(c[i]).gridx==k && layout.getConstraints(c[i]).gridy==j)
								{
									if(c[i] instanceof JLabel) 
									{
										Text+=((JLabel)c[i]).getText()+" ";
									}
									if(c[i] instanceof JComboBox) 
									{
										Text+=((JComboBox<?>)c[i]).getSelectedItem().toString()+" ";
									}
									if(c[i] instanceof JList) 
									{
										Text+=((JList<?>)c[i]).getSelectedValue().toString()+" ";
									}
									if(c[i] instanceof JTextField) 
									{
										Text+=((JTextField)c[i]).getText()+" ";
									}
									if(c[i] instanceof JScrollPane)
									{
										JScrollPane targetscroll = (JScrollPane )c[i];
							        	JViewport targetview = targetscroll.getViewport();
							            JComboBox<String> target = (JComboBox<String>) targetview.getComponents()[0];
							            if(target.getSelectedItem()=="OR" || target.getSelectedItem() =="AND")
							            {
							            Text+=target.getSelectedItem()+"\n";
							        //    System.out.println(layout.getConstraints(targetscroll).gridx);
							            }
							            else
							            Text+=target.getSelectedItem()+" ";
									}
		
								}
							}
						}
					}
			//		System.out.println(Text);
					textArea.setText(Text);
					
//					String textWithNoSpace = Text.replaceAll("\\s+", " ");
//					System.out.println(textWithNoSpace);
					dispose();
					 mxCellEditor.numberOfInterfaces--;
					 mxCellEditor.stopEditing(false, true);
					}
					else {
						if(nullValueSelected){
							nullValueSelected=false;
							JOptionPane.showMessageDialog(getFrames()[0], "null Variable selected before saving.");
						}else{
						allTextFieldsAreSet=true;
						JOptionPane.showMessageDialog(getFrames()[0], "You need to set all fields before saving.");
						
					}}
				}
			
		    };
    addLine.addActionListener(new ActionListener() {
		
			@Override
			public void actionPerformed(ActionEvent e) {
				addLine (++rowNumber,null);
			    newLineOpPosition=30;
				revalidate();
				repaint();
				pack();
				
			}
	});
    
    panelTop.add(delete);
    panelTop.add(addLine);
    panelDebut.add(panelTop);
    panelMid.add(new JLabel("Description :"));
    Description.setPreferredSize(new Dimension(600,30));
    panelMid.add(Description);
    panelDebut.add(panelMid);
    panelBot.add(cancel);
    panelBot.add(save);
   // panel.setPreferredSize(new Dimension(500,300));
   // ReadExcel.generateBD(dbList);
   // listItems= ((String[]) ReadExcel.bdData.get(chosen).nomVariables().stream().toArray(String[]::new));

    listItems= (String[]) getVariablesList(chosen,dbList).stream().toArray(String[]::new);
    Arrays.sort(listItems);
    setInterfaceText(textArea.getText());

    addWindowListener(new java.awt.event.WindowAdapter() {
        @Override
        public void windowClosing(java.awt.event.WindowEvent windowEvent) {
           dispose();
           mxCellEditor.numberOfInterfaces--;
           mxCellEditor.stopEditing(false, true);
        }
    });

}
/*
 * Adds the text to the interface 
 */
public void setInterfaceText(String text){
	
	int pos = text.indexOf('\n');
	System.out.println("-"+text+"-"+pos);
	if (pos<1){ pos=0;
	DescriptionText="";}
	else
	DescriptionText=text.substring(0,pos);
	Description.setText(DescriptionText);
	if(pos!=0) text=text.substring(pos+1);
	else
	text=text.substring(pos);
	String[] lines = text.split("\n");
	if(lines[0]==""){
		addLine(++rowNumber,null);
	}else {addLine(++rowNumber,lines[0]);
	for (int i=1;i<lines.length;i++){
		addLine(++rowNumber,lines[i]);
	}
	}
}


/*
 * Add a new line 
 * 
 */
public static List<String> getVariablesList(int i, String[] dbList2)
{	for (int j=0;j<GraphEditor.categories.size();j++){
		if(dbList2[i].contains(GraphEditor.categories.get(j).getNom()))
				return GraphEditor.categories.get(j).nomVariables();}
return(GraphEditor.categories.get(i).nomVariables());

}
public void addLine (int i , String line) {

 	newLineOpPosition=30;
	valAdded=false;
	theOpList = new JComboBox<Object>(opList);
	theDifList = new JComboBox<Object>(difList);
	listItems= (String[]) getVariablesList(0,dbList).stream().toArray(String[]::new);
	Arrays.sort(listItems);
	theList2 = new JComboBox<String>(listItems);
	theList = new JComboBox<Object>(dbList) ;
	theList.setSelectedItem(theList.getItemAt(0));
	theList2.setSelectedItem(theList2.getItemAt(0));
    theList.addActionListener(new ActionListener() {
        @SuppressWarnings("unchecked")
		public void actionPerformed(ActionEvent e) {
        	chosen = ((JComboBox<?>) e.getSource()).getSelectedIndex();
        //	listItems= ((String[]) ReadExcel.bdData.get(chosen).nomVariables().stream().toArray(String[]::new));
        	listItems= (String[]) getVariablesList(chosen,dbList).stream().toArray(String[]::new);
        	Arrays.sort(listItems);
        	DefaultComboBoxModel<String> a = new DefaultComboBoxModel<>(listItems);
        	try{
        		int place=0;
	        	Component source = (Component) e.getSource();
	        	Component[] c = panel.getComponents();
	        	for (int i=0;i<c.length;i++){
	        	if(c[i].equals(source)) 
	        		{
	        		place = i;
	        		break;
	        		}
	        	}
	        	JScrollPane targetscroll = (JScrollPane )c[place+1];
	        	JViewport targetview = targetscroll.getViewport();
	            JComboBox<String> target = (JComboBox<String>) targetview.getComponents()[0];
	            if(target!=null)
	            target.setSelectedIndex(0);
	        	target.removeAllItems();
	        	target.setModel(a);
	        	revalidate();
	        	pack();
        	}
        	catch(Exception exp) {
        		exp.printStackTrace();
        	}
        }
      });
	reglageLayout(i);
    theList2.setEditable(true);
    new MatchingInComboBox(theList2);
    JCheckBox  checkbox= new JCheckBox();
    JScrollPane scrollableList = new JScrollPane(theList);
    JScrollPane scrollableList2 = new JScrollPane(theList2);
    JTextField text = new JTextField();
    ContextMenu contextMenu = new ContextMenu(panel,layout);
    contextMenu.add(text);
    text.setMinimumSize(new Dimension(100,30));
    text.setPreferredSize(new Dimension(100,30));
    JScrollPane scrollableOp = new JScrollPane(theOpList);
    List<Component> l = new ArrayList<Component>();
    if(i>0 && !lDelete.contains(rowNumber-1)){
    	reglageColumn(rowNumber-1,newLineOpPosition);
    	panel.add(scrollableOp,cOp);
    	l.add(scrollableOp);
    	JViewport targetview1 = scrollableOp.getViewport();
    	JComboBox<String >target1 = (JComboBox<String>) targetview1.getComponents()[0];
    	target1.setSelectedIndex(ChosenElement);
    	reglageReset(); 	
    }
    
    panel.add(checkbox, cCheck);
    panel.add(theList,cList1);
    panel.add(scrollableList2,cList2);
    panel.add(theDifList, clabelEgal);
    panel.add(text, cText);
    
    l.add(checkbox);
    l.add(scrollableList);
    l.add(scrollableList2);
    l.add(text);
	JViewport targetview = scrollableList2.getViewport();
	JComboBox<String >target = (JComboBox<String>) targetview.getComponents()[0];
    PlainDocument doc = (PlainDocument) text.getDocument();
    if (isBool((String)(target).getSelectedItem()))
    doc.setDocumentFilter(new BoolFilter());
    else doc.setDocumentFilter(new IntFilter());
    Elements.put((double) i+1, l);
    if(line != null && line.length() != 0) {
    	line= line.replaceAll(" +", " ");
    	String[] vars = line.split(" ");
    	String dbase = vars[0]+" "+vars[1];
    	theList.setSelectedItem(dbase);
    	theList2.setSelectedItem(vars[2]);
    	if (isBool((String) theList2.getSelectedItem()))
    	    doc.setDocumentFilter(new BoolFilter());
    	    else doc.setDocumentFilter(new IntFilter());
    	theDifList.setSelectedItem(vars[3]);
    	text.setText(vars[4]);
    	if(vars[vars.length-1].toLowerCase().contains("o")){
        	ChosenElement=1;}
        	else {ChosenElement=0;}
    	
    }
    theList2.addActionListener(action1);
    reglageReset();
}
protected boolean isBool(String selectedItemVar) {
	boolean a = false;
	for (int i=0 ; i<GraphEditor.categories.size();i++) {
			List<String> v1 = new ArrayList<String>();
			v1=GraphEditor.categories.get(i).nomVariables();
			List<String> v2 = new ArrayList<String>();
			v2=GraphEditor.categories.get(i).typeVariables();
			if(selectedItemVar == null) {
				return a;
			}
			else {
			for (int j=0 ; j<v1.size();j++) {
				if(v1.get(j).contains(selectedItemVar)){
					if(v2.get(j).toLowerCase().contains("bool")){
						return true;
					}
					else return false;
				}
			}}
	}
	return a;
}

public void reglageColumn(int row,int column) {
	cText.gridx = column+1;
	cText.gridy = row;

	cOp.gridx = column+3;
	cOp.gridy = row;
	
	cBtAdd.gridx = column+2;
	cBtAdd.gridy = row;
	
	clabelOr.gridx = column;
	clabelOr.gridy = row;
}
public void reglageLastColumn(int row,int column) {
	cText.gridx = column+1;
	cText.gridy = row;

	cBtAdd.gridx = column+2;
	cBtAdd.gridy = row;
	
	clabelOr.gridx = column;
	clabelOr.gridy = row;
	
	cOp.gridx = column+3;
	cOp.gridy = row;
}
public void reglageReset() {
	cText.gridx = 4;
	cText.gridy = rowNumber;
	
	cOp.gridx = 6;
	cOp.gridy = rowNumber;
	
	cBtAdd.gridx = 5;
	cBtAdd.gridy = rowNumber;
	
    clabelOr.gridx = 5;
    clabelOr.gridy = rowNumber;
    
    

}
public void reglageLayout (int i) {
	/*c.gridwidth = 2;   //2 columns wide
	 * c.weighty = 1.0;   //request any extra vertical space
	c.anchor = GridBagConstraints.PAGE_END; //bottom of space
	c.insets = new Insets(10,0,0,0);  //top padding
	 */
	cCheck.fill = GridBagConstraints.HORIZONTAL;
	cCheck.weightx = 0.5;  // normal weight
	cCheck.gridx = 0;      // column number
	cCheck.gridy = i;	  // line number

	cList1.fill = GridBagConstraints.HORIZONTAL;
	cList1.weightx = 0.5;
	cList1.gridx = 1;
	cList1.gridy = i;
	
	cList2.fill = GridBagConstraints.HORIZONTAL;
	cList2.weightx = 0.5;
	cList2.gridx = 2;
	cList2.gridy = i;
	
	cOp.fill = GridBagConstraints.HORIZONTAL;
	cOp.weightx = 0.0;
	cOp.gridx = newLineOpPosition;
	cOp.gridy = i-1;
	
	cBtAdd.fill = GridBagConstraints.HORIZONTAL;
	cBtAdd.weightx = 0.0;
	cBtAdd.gridx = 5;
	//cBtAdd.insets = new Insets(0,20,0,0); // translation a droite
	cBtAdd.gridy = i;
	
	cText.fill = GridBagConstraints.HORIZONTAL;
	cText.weightx = 0.5;
	cText.gridx = 4;
	//cText.insets = new Insets(0,20,0,0); // translation a droite
	cText.gridy = i;
	
    clabelEgal.fill = GridBagConstraints.HORIZONTAL;
    clabelEgal.weightx = 0.0;
    clabelEgal.gridx = 3;
    clabelEgal.insets = new Insets(0,10,0,10); // translation a droite
    clabelEgal.gridy = i;
    
    clabelOr.fill = GridBagConstraints.HORIZONTAL;
    clabelOr.weightx = 0.0;
    clabelOr.gridx = 6;
    clabelOr.gridy = i;

	
}
/*
public static void main(String[] args) {
 
//    CategorieDao categorieDao = new CategorieDao();
//
//    for (int i=0; i<inputTransition.dbList.length;i++){
//        Categorie categorie = new Categorie();
//        categorie=ReadExcel.bdData.get(i);
//     //   System.out.println(categorie);
//    }
   final InputTransition inputTransition = new InputTransition();
    SwingUtilities.invokeLater(new Runnable() {

        @Override
        public void run() {
        	inputTransition.pack();
        	inputTransition.setVisible(true);
        }
    });
}
*/
}