package com.focus.enicar2017.input;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JViewport;
import javax.swing.text.PlainDocument;

import org.apache.lucene.util.BytesRefHash.MaxBytesLengthExceededException;
import org.apache.lucene.util.automaton.RegExp;

import com.focus.enicar2017.Transition.Dao.VariablesDao;
import com.focus.enicar2017.Transition.model.Variables;
import com.focus.enicar2017.textFilters.BoolFilter;
import com.focus.enicar2017.textFilters.IntFilter;
import com.focus.enicar2017.textFilters.ParentheseCloseFilter;
import com.focus.enicar2017.textFilters.ParentheseOpenFilter;
import com.mxgraph.examples.swing.GraphEditor;
import com.mxgraph.swing.view.mxCellEditor;

@SuppressWarnings("serial")
public class InputTransition extends JFrame {
	String Text;

	public String getText() {
		return Text;
	}

	public void setText(String text) {
		Text = text;
	}

	Map<Double, List<Component>> Elements = new HashMap<Double, List<Component>>();
	GridBagLayout layout = new GridBagLayout();
    JPanel panel = new JPanel(layout);
	JPanel panelBot = new JPanel(new FlowLayout());
	JPanel panelTop = new JPanel(new GridLayout(2,1));
	JPanel panelTop1 = new JPanel(new FlowLayout());
	JPanel panelTop2 = new JPanel(new FlowLayout());
	public JButton save = new JButton("Save");
	JButton addLine = new JButton("New Line");
	JButton cancel = new JButton("Cancel");
	JButton delete = new JButton("Delete");
	JLabel labelOr = new JLabel(" or");
	JLabel preview = new JLabel("Preview : ");
	JLabel thePreview = new JLabel("");
	JList<Component> a = new JList<Component>();
	List<Integer> lDelete = new ArrayList<Integer>();
	GridBagConstraints cCheck = new GridBagConstraints();
	GridBagConstraints cList1 = new GridBagConstraints();
	GridBagConstraints cList2 = new GridBagConstraints();
	GridBagConstraints cOp = new GridBagConstraints();
	GridBagConstraints cBtAdd = new GridBagConstraints();
	GridBagConstraints cText = new GridBagConstraints();
	GridBagConstraints clabelEgal = new GridBagConstraints();
	GridBagConstraints clabelOr = new GridBagConstraints();
	GridBagConstraints cParOpen = new GridBagConstraints();
	JTextField textParOpen0 = new JTextField();
	JTextField textParClose0 = new JTextField();
	GridBagConstraints cParClose = new GridBagConstraints();
	int rowNumber = -1;
	VariablesDao variablesDao = new VariablesDao();
	String[] dbList;
	String[] opList = { "AND", "OR" };
	String[] difList = { "==", ">=", "<=" };
	String[] listItems;
	JComboBox<?> theOpList;
	JComboBox<?> theDifList;
	JComboBox<?> theList;
	JComboBox<String> theList2;
	int chosen = 0;
	Boolean valAdded = false;
	int newLineOpPosition = 30;
	public ActionListener action;
	ActionListener action1;
	protected boolean allTextFieldsAreSet = true;
	protected boolean nullValueSelected = false;
	int ChosenElement = 0;
	int min = rowNumber;
	boolean deleted = true;

	public InputTransition(JTextArea textArea, mxCellEditor mxCellEditor) {
		mxCellEditor.numberOfInterfaces++;
		textParOpen0.setPreferredSize(new Dimension(50, 30));
		textParOpen0.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) {
				
				
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
				thePreview.setText(showInput());
				
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				
				
			}
		});
		textParClose0.setPreferredSize(new Dimension(50, 30));
textParClose0.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) {
				
				
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
				thePreview.setText(showInput());
				
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				
				
			}
		});
		PlainDocument docparclose0 = (PlainDocument) textParClose0.getDocument();
		docparclose0.setDocumentFilter(new ParentheseCloseFilter());
		PlainDocument docparopen0 = (PlainDocument) textParOpen0.getDocument();
		docparopen0.setDocumentFilter(new ParentheseOpenFilter());
		this.setLayout(new BorderLayout());
		this.add(panelTop, BorderLayout.PAGE_START);
		this.add(panel, BorderLayout.CENTER);
		this.add(panelBot, BorderLayout.PAGE_END);
		// dbList = (String[])
		// GraphEditor.nomCategories.stream().toArray(String[]::new);
		dbList = GraphEditor.to1ArrayCategories("trans");
		// System.out.println(dbList);
		theList = new JComboBox<Object>(dbList);
		delete.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				boolean yDeleteLastLine = false;
				Component[] c3 = panel.getComponents();
				min = 99;
				for (int i = 0; i < c3.length; i++) {
					if (0 <= layout.getConstraints(c3[i]).gridy && layout.getConstraints(c3[i]).gridy < min)
						min = layout.getConstraints(c3[i]).gridy;
				}
				int deleteElement = 0;
				Component[] c = panel.getComponents();
				lDelete.clear();
				// adds number of lines to delete
				for (int i = 0; i < c.length; i++) {
					if (c[i] instanceof JCheckBox) {
						if (((JCheckBox) c[i]).isSelected()) {
							lDelete.add(layout.getConstraints(c[i]).gridy);
							if(layout.getConstraints(c[i]).gridy==lastLine())yDeleteLastLine=true;
							deleteElement++;
						}
					}
				}
				if (deleteElement > 0) {
					System.out.println("LINES TO DELETE = " + deleteElement);
					// remove those lines
					for (int i = 0; i < c.length; i++) {
						Iterator<Integer> iterator = lDelete.iterator();
						while (iterator.hasNext()) {
							Integer a = iterator.next();
							if (layout.getConstraints(c[i]).gridy == a || layout.getConstraints(c[i]).gridy < 0) {
								panel.remove(c[i]);
							}
						}
					}
					// Collections.sort(lDelete);
					// Collections.reverse(lDelete);
					//
					// Iterator<Integer> iterator = lDelete.iterator();
					// while (iterator.hasNext()) {
					// Integer a = iterator.next();
					// System.out.println("START UPDATE FROM "+a);
					// System.out.println("END UPDATE TO "+rowNumber);
					// for(int p=a;p<rowNumber;p++)
					// {
					// for (int i=0;i<c.length;i++){
					// GridBagConstraints cst = new GridBagConstraints();
					// cst.gridx=layout.getConstraints(c[i]).gridx;
					// cst.gridy=a;
					// layout.setConstraints(c[i], cst);
					// }
					// }
					// }
					Component[] c2 = panel.getComponents();
					// remove last operand
					// rowNumber-=deleteElement;

					System.out.println("row+++" + rowNumber);
					System.out.println("min+++" + min);
					if(yDeleteLastLine){
						yDeleteLastLine=false;
						for (int i = 0; i < c2.length; i++) {
						if ((layout.getConstraints(c2[i]).gridy == layout.getConstraints(c2[c2.length - 1]).gridy)
								&& layout.getConstraints(c2[i]).gridx == 33) {
							deleted=true;
							panel.remove(c2[i]);
						}
					}}
					if(firstLine()==lastLine())
						
					for (int i = 0; i < c2.length; i++) {
						System.out.println("----------------+++++-----");
						// System.out.println(rowNumber==min);
						// if(rowNumber !=min+1 &&
						// rowNumber==layout.getConstraints(c2[i]).gridy&&(layout.getConstraints(c2[i]).gridy
						// ==
						// layout.getConstraints(c2[c2.length-1]).gridy)&&layout.getConstraints(c2[i]).gridx==33){//33
						// panel.remove(c2[i]);
						// }
						// if((rowNumber==min+1)&&(layout.getConstraints(c2[i]).gridy
						// ==
						// layout.getConstraints(c2[c2.length-1]).gridy)&&layout.getConstraints(c2[i]).gridx>25){//33
						// panel.remove(c2[i]);
						// deleted=true;
						// }
						// }for (int i=0;i<c2.length;i++){
						if ((layout.getConstraints(c2[i]).gridy == layout.getConstraints(c2[c2.length - 1]).gridy)
								&& layout.getConstraints(c2[i]).gridx > 25) {
							deleted=true;
							panel.remove(c2[i]);
						}
						if(layout.getConstraints(c2[i]).gridy==lastLine()&& layout.getConstraints(c2[i]).gridx > 25){
							deleted=true;
							panel.remove(c2[i]);
						}
					}
					
					System.out.println("new RowNumber = " + rowNumber);
					revalidate();
					repaint();
					pack();
				}
				thePreview.setText(showInput());
			}
		});
		cancel.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				dispose();
				mxCellEditor.numberOfInterfaces--;
				mxCellEditor.stopEditing(true, true);

			}
		});
		/*
		 * Action1 for list 2
		 */
		action1 = new ActionListener() {
			@SuppressWarnings("unchecked")
			public void actionPerformed(ActionEvent e) {
				Component source = (Component) e.getSource();
				Component[] c = panel.getComponents();
				JScrollPane targetscroll = new JScrollPane();
				JComboBox<String> target = new JComboBox<String>();
				for (int i = 0; i < c.length; i++) {
					if (c[i] instanceof JScrollPane) {
						target = new JComboBox<String>();
						targetscroll = new JScrollPane();
						targetscroll = (JScrollPane) c[i];
						JViewport targetview = targetscroll.getViewport();
						target = (JComboBox<String>) targetview.getComponents()[0];
						if (target.getSelectedItem() == ((JComboBox<String>) source).getSelectedItem()
								&& target == source) {
							i = c.length;
							break;
						}

					}
				}

				if (isBool((String) ((JComboBox) source).getSelectedItem())) {
					try {
						for (int i = 0; i < c.length; i++) {
							if (layout.getConstraints(c[i]).gridy == layout.getConstraints(targetscroll).gridy) {
								if (c[i] instanceof JTextField && layout.getConstraints(c[i]).gridx < 29) {
									((JTextField) c[i]).setText("0");
									PlainDocument doc = (PlainDocument) ((JTextField) c[i]).getDocument();
									doc.setDocumentFilter(new BoolFilter());
								}
							}
						}
					} catch (Exception exp) {
						exp.printStackTrace();
					}
				}

				else {
					try {
						for (int i = 0; i < c.length; i++) {
							if (layout.getConstraints(c[i]).gridy == layout.getConstraints(targetscroll).gridy) {
								if (c[i] instanceof JTextField && layout.getConstraints(c[i]).gridx < 29) {
									((JTextField) c[i]).setText("0");
									PlainDocument doc = (PlainDocument) ((JTextField) c[i]).getDocument();
									doc.setDocumentFilter(new IntFilter());
								}
							}
						}
					} catch (Exception exp) {
						exp.printStackTrace();
					}
				}
			}
		};
		/*
		 * Action for save
		 */
		action = new ActionListener() {

			@SuppressWarnings("unchecked")
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Component[] c = panel.getComponents();
				Text = "";
				boolean backToLine = false;
				boolean error = false;
				int verif = 0;
				int l = 0;
				while (l < c.length && allTextFieldsAreSet) {
					if (c[l] instanceof JTextField) {
						if (((JTextField) c[l]).getText().length() < 1 && layout.getConstraints(c[l]).gridx < 29) {
							allTextFieldsAreSet = false;
						} else {
							for (int p = 0; p < ((JTextField) c[l]).getText().length(); p++) {
								if (((JTextField) c[l]).getText().charAt(p) == '(') {
									verif++;
								} else if (((JTextField) c[l]).getText().charAt(p) == ')') {
									verif--;
									if (verif < 0)
										error = true;
								}
							}
						}
					}
					if (c[l] instanceof JComboBox) {
						if (((JComboBox<?>) c[l]).getSelectedItem() == null)
							nullValueSelected = true;
					}
					if (c[l] instanceof JList) {
						if (((JList<?>) c[l]).getSelectedValue() == null)
							;
						nullValueSelected = true;
					}

					l++;
				}
				if (allTextFieldsAreSet && !nullValueSelected && verif == 0 && !error) {
					for (int j = 0; j <= rowNumber; j++) {
							for (int k = 1; k <= 40; k++) {
								for (int i = 0; i < c.length; i++) {
									if (layout.getConstraints(c[i]).gridx == k
											&& layout.getConstraints(c[i]).gridy == j) {
										if (c[i] instanceof JLabel) {
											Text += ((JLabel) c[i]).getText() + " ";
										}
										if (c[i] instanceof JComboBox) {
											Text += ((JComboBox<?>) c[i]).getSelectedItem().toString() + " ";
										}
										if (c[i] instanceof JList) {
											Text += ((JList<?>) c[i]).getSelectedValue().toString() + " ";
										}
										if (c[i] instanceof JTextField) {
											if (backToLine || ((JTextField) c[i]).getText().contains(")")) {
												backToLine = false;
												Text += ((JTextField) c[i]).getText() + "\n";
												System.out.println("Text )=" + Text + "end");
												System.out.println("TEXT )=" + ((JTextField) c[i]).getText() + "end");
											} else {
												if (((JTextField) c[i]).getText().contains("("))
													Text += ((JTextField) c[i]).getText();
												else
													Text += ((JTextField) c[i]).getText() + " ";
													System.out.println("Text OTHER=" + Text);
													System.out.println("TEXT OTHER=" + ((JTextField) c[i]).getText());
												}
										}
										if (c[i] instanceof JScrollPane) {
											JScrollPane targetscroll = (JScrollPane) c[i];
											JViewport targetview = targetscroll.getViewport();
											JComboBox<String> target = (JComboBox<String>) targetview
													.getComponents()[0];
											if (target.getSelectedItem() == "OR" || target.getSelectedItem() == "AND") {
												System.out.println("////////////ENTERED++");
												Text += target.getSelectedItem();
												backToLine = true;
												// System.out.println(layout.getCo1nstraints(targetscroll).gridx);
											} else
												Text += target.getSelectedItem() + " ";
										}

									}
								}
							}
					}
					// System.out.println(Text);
					textArea.setText(Text);

					// String textWithNoSpace = Text.replaceAll("\\s+", " ");
					// System.out.println(textWithNoSpace);
					dispose();
					mxCellEditor.numberOfInterfaces--;
					mxCellEditor.stopEditing(false, true);
				}

				else {
					if (nullValueSelected) {
						nullValueSelected = false;
						JOptionPane.showMessageDialog(getFrames()[1], "null Variable selected before saving.");
					} else if (verif != 0 || error) {
						System.out.println("verif = " + verif);
						verif = 0;
						error = false;
						JOptionPane.showMessageDialog(getFrames()[1], "Parentheses doesn't match.");
					} else {
						allTextFieldsAreSet = true;
						JOptionPane.showMessageDialog(getFrames()[1], "You need to set all fields before saving.");

					}
				}
			}
		};
		addLine.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				addLine(++rowNumber, null);
				newLineOpPosition = 30;
				revalidate();
				repaint();
				pack();

			}
		});
		panelTop1.add(delete);
		panelTop1.add(addLine);
		panelTop2.add(preview);
		panelTop2.add(thePreview);
		panelTop.add(panelTop1);
		panelTop.add(panelTop2);
		panelBot.add(cancel);
		panelBot.add(save);

		// panel.setPreferredSize(new Dimension(500,300));
		// ReadExcel.generateBD(dbList);
		// listItems= ((String[])
		// ReadExcel.bdData.get(chosen).nomVariables().stream().toArray(String[]::new));

		listItems = (String[]) getVariablesList(chosen, dbList).stream().toArray(String[]::new);
		Arrays.sort(listItems);
		setInterfaceText(textArea.getText());

		addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(java.awt.event.WindowEvent windowEvent) {
				dispose();
				mxCellEditor.numberOfInterfaces--;
				mxCellEditor.stopEditing(false, true);

			}
		});
		thePreview.setText(showInput());
	}

	/*
	 * Adds the text to the interface
	 */
	public void setInterfaceText(String text) {

		String[] lines = text.split("\n");
		if (lines[0] == "") {
			addLine(++rowNumber, null);
		} else {
			addLine(++rowNumber, lines[0]);
			for (int i = 1; i < lines.length; i++) {
				addLine(++rowNumber, lines[i]);
			}
		}
	}

	/*
	 * Add a new line
	 * 
	 */
	public static List<String> getVariablesList(int i, String[] dbList2) {
		for (int j = 0; j < GraphEditor.categories.size(); j++) {
			if (dbList2[i].contains(GraphEditor.categories.get(j).getNom()))
				return GraphEditor.categories.get(j).nomVariables();
		}
		return (GraphEditor.categories.get(i).nomVariables());
	}

	public void addLine(int i, String line) {
		newLineOpPosition = 30;
		valAdded = false;
		theOpList = new JComboBox<Object>(opList);
		theDifList = new JComboBox<Object>(difList);
		listItems = (String[]) getVariablesList(0, dbList).stream().toArray(String[]::new);
		Arrays.sort(listItems);
		theList2 = new JComboBox<String>(listItems);
		theList = new JComboBox<Object>(dbList);
		theList.setSelectedItem(theList.getItemAt(0));
		theList2.setSelectedItem(theList2.getItemAt(0));
		theList.addActionListener(new ActionListener() {
			@SuppressWarnings("unchecked")
			public void actionPerformed(ActionEvent e) {
				chosen = ((JComboBox<?>) e.getSource()).getSelectedIndex();
				// listItems= ((String[])
				// ReadExcel.bdData.get(chosen).nomVariables().stream().toArray(String[]::new));
				listItems = (String[]) getVariablesList(chosen, dbList).stream().toArray(String[]::new);
				Arrays.sort(listItems);
				DefaultComboBoxModel<String> a = new DefaultComboBoxModel<>(listItems);
				try {
					int place = 0;
					Component source = (Component) e.getSource();
					Component[] c = panel.getComponents();
					for (int i = 0; i < c.length; i++) {
						if (c[i].equals(source)) {
							place = i;
							break;
						}
					}
					JScrollPane targetscroll = (JScrollPane) c[place + 1];
					JViewport targetview = targetscroll.getViewport();
					JComboBox<String> target = (JComboBox<String>) targetview.getComponents()[0];
					target.setSelectedIndex(0);
					target.removeAllItems();
					target.setModel(a);
					revalidate();
					pack();
				} catch (Exception exp) {
					exp.printStackTrace();
				}
			}
		});

		reglageLayout(i);
		JCheckBox checkbox = new JCheckBox();
		JScrollPane scrollableList = new JScrollPane(theList);
		JScrollPane scrollableList2 = new JScrollPane(theList2);
		theList2.setEditable(true);
		new MatchingInComboBox(theList2);
		JTextField text = new JTextField();
		JTextField textParOpen = new JTextField();
		JTextField textParClose = new JTextField();

		ContextMenu contextMenu = new ContextMenu(panel, layout);
		contextMenu.add(text);
		text.setMinimumSize(new Dimension(100, 30));
		text.setPreferredSize(new Dimension(100, 30));
		JScrollPane scrollableOp = new JScrollPane(theOpList);
		JButton bt = new JButton("+");
		PlainDocument docparopen = (PlainDocument) textParOpen.getDocument();
		docparopen.setDocumentFilter(new ParentheseOpenFilter());
		textParOpen.setPreferredSize(new Dimension(50, 30));
		textParClose.setPreferredSize(new Dimension(50, 30));

		PlainDocument docparclose = (PlainDocument) textParClose.getDocument();
		docparclose.setDocumentFilter(new ParentheseCloseFilter());

		bt.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				valAdded = true;
				Component source = null;
				GridBagConstraints c1 = new GridBagConstraints();
				try {
					source = (Component) e.getSource();
					// System.out.println("Source= "+source);
					c1 = layout.getConstraints(source);
					// System.out.println("source = "+c.gridx+"=c.gridx
					// c.gridy="+c.gridy);
					// if(rowNumber-1==c.gridy){
					// newLineOpPosition=c.gridx+1;
					// }
					if (rowNumber == c1.gridy) {
						reglageLastColumn(c1.gridy, c1.gridx);
					} else {
						reglageColumn(c1.gridy, c1.gridx);
					}
					layout.setConstraints(source, cBtAdd);
					JTextField textTemp = new JTextField();
					textTemp.setMinimumSize(new Dimension(100, 30));
					textTemp.setPreferredSize(new Dimension(100, 30));
					panel.add(new JLabel(" or "), clabelOr);
					panel.add(textTemp, cText);
					Component[] c = panel.getComponents();
					JComboBox<String> target = new JComboBox<>();
					JScrollPane targetscroll = new JScrollPane();
					for (int i = 0; i < c.length; i++) {
						GridBagConstraints c2 = new GridBagConstraints();
						target = new JComboBox<>();
						c2 = layout.getConstraints(c[i]);
						if (c2.gridx == 2 && c2.gridy == c1.gridy) {
							targetscroll = (JScrollPane) c[i];
							JViewport targetview = targetscroll.getViewport();
							target = (JComboBox<String>) targetview.getComponents()[0];
							break;
						}
					}
					if (isBool((String) (target).getSelectedItem())) {
						try {
							for (int i = 0; i < c.length; i++) {
								if (layout.getConstraints(c[i]).gridy == layout.getConstraints(targetscroll).gridy) {
									if (c[i] instanceof JTextField && layout.getConstraints(c[i]).gridx < 29) {
										PlainDocument doc = (PlainDocument) ((JTextField) c[i]).getDocument();
										doc.setDocumentFilter(new BoolFilter());
									}
								}
							}
						} catch (Exception exp) {
							exp.printStackTrace();
						}
					}

					else {
						try {
							for (int i = 0; i < c.length; i++) {
								if (layout.getConstraints(c[i]).gridy == layout.getConstraints(targetscroll).gridy) {
									if (c[i] instanceof JTextField && layout.getConstraints(c[i]).gridx < 29) {
										PlainDocument doc = (PlainDocument) ((JTextField) c[i]).getDocument();
										doc.setDocumentFilter(new IntFilter());
									}
								}
							}
						} catch (Exception exp) {
							exp.printStackTrace();
						}
					}
					ContextMenu contextMenu = new ContextMenu(panel, layout);
					contextMenu.add(textTemp);

					// if(c.gridy!=rowNumber){
					// panel.add(scrollableOp,cOp);
					// }

				} catch (Exception exp) {
					exp.printStackTrace();
				}

				reglageReset();
				revalidate();
				repaint();
				pack();

			}
		});
		List<Component> l = new ArrayList<Component>();
		if (i > 0) {// && !lDelete.contains(rowNumber-1)
			reglageColumn(previousLine(rowNumber), newLineOpPosition);
			System.out.println("ROW NUMBER" + rowNumber);
			System.out.println("COP X " + cOp.gridx);
			System.out.println("COP Y " + cOp.gridy);
			System.out.println("i ==" + i);
			System.out.println("min == " + min);
			System.out.println("first == " + firstLine());
			// if(i==firstLine()+1){
			if (deleted) {
				System.out.println("ADDED AT LINE " + firstLine());
				reglageLayout1(firstLine() + 1);
				panel.add(textParOpen0, cParOpen);
				panel.add(textParClose0, cParClose);
				deleted = false;
				reglageLayout1(i + 1);
			}
			System.out.println("ADDED AT " + i);
			panel.add(textParOpen, cParOpen);
			panel.add(textParClose, cParClose);
			panel.add(scrollableOp, cOp);
			l.add(scrollableOp);
			JViewport targetview1 = scrollableOp.getViewport();
			JComboBox<String> target1 = (JComboBox<String>) targetview1.getComponents()[0];
			target1.setSelectedIndex(ChosenElement);
			target1.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					thePreview.setText(showInput());
					
				}
			});
			textParOpen.addKeyListener(new KeyListener() {
				
				@Override
				public void keyTyped(KeyEvent e) {
					
					
				}
				
				@Override
				public void keyReleased(KeyEvent e) {
					thePreview.setText(showInput());
					
				}
				
				@Override
				public void keyPressed(KeyEvent e) {
					
					
				}
			});textParClose.addKeyListener(new KeyListener() {
				
				@Override
				public void keyTyped(KeyEvent e) {
					
					
				}
				
				@Override
				public void keyReleased(KeyEvent e) {
					thePreview.setText(showInput());
					
				}
				
				@Override
				public void keyPressed(KeyEvent e) {
					
					
				}
			});
			reglageReset();
		}

		panel.add(checkbox, cCheck);
		panel.add(theList, cList1);
		panel.add(scrollableList2, cList2);
		panel.add(theDifList, clabelEgal);
		panel.add(text, cText);

		l.add(checkbox);
		l.add(scrollableList);
		l.add(scrollableList2);
		l.add(text);
		JViewport targetview = scrollableList2.getViewport();
		JComboBox<String> target = (JComboBox<String>) targetview.getComponents()[0];
		PlainDocument doc = (PlainDocument) text.getDocument();
		if (isBool((String) (target).getSelectedItem()))
			doc.setDocumentFilter(new BoolFilter());
		else
			doc.setDocumentFilter(new IntFilter());
		l.add(bt);
		Elements.put((double) i + 1, l);
		if (line != null && line.length() != 0) {
			System.out.println("line =" + line);
			line = line.replaceAll(" +", " ");
			String[] vars = line.split(" ");
			String dbase = vars[0] + " " + vars[1];
			theList.setSelectedItem(dbase);
			theList2.setSelectedItem(vars[2]);
			if (isBool((String) theList2.getSelectedItem()))
				doc.setDocumentFilter(new BoolFilter());
			else
				doc.setDocumentFilter(new IntFilter());
			theDifList.setSelectedItem(vars[3]);
			text.setText(vars[4]);

			ContextMenu contextMenu1 = new ContextMenu(panel, layout);
			int posFirstPar;
			int posEndPar;
			posFirstPar = vars[vars.length - 1].toLowerCase().indexOf("o");
			if (posFirstPar < 0) {
				posFirstPar = vars[vars.length - 1].toLowerCase().indexOf("a");
				posEndPar = posFirstPar + 3;
			} else {
				posEndPar = posFirstPar + 2;
			}
			if (posFirstPar >= 0) {
				System.out.println(vars[vars.length - 1]);
				if (rowNumber == 0) {
					System.out.println("POS FIRST PAR +" + posFirstPar);
					System.out.println("POSENDPAR =" + posEndPar);
					textParOpen0.setText(vars[vars.length - 1].substring(0, posFirstPar));
					textParClose0.setText(vars[vars.length - 1].substring(posEndPar));
				} else {
					textParOpen.setText(vars[vars.length - 1].substring(0, posFirstPar));
					textParClose.setText(vars[vars.length - 1].substring(posEndPar));
				}
			} else {
				posEndPar = vars[vars.length - 1].indexOf(")");
				if (posEndPar >= 0)
					textParClose.setText(vars[vars.length - 1].substring(posEndPar));
			}
			if (vars[vars.length - 1].toLowerCase().contains("o")) {
				ChosenElement = 1;
			} else {
				ChosenElement = 0;
			}

			if (vars.length > 4) {
				for (int j = 6; j < vars.length; j += 2) {
					clabelOr.gridx = j - 1;
					cText.gridx = j;
					cBtAdd.gridx = j + 1;
					panel.add(new JLabel(" " + vars[j - 1] + " "), clabelOr);
					JTextField textTemp = new JTextField();
					textTemp.setMinimumSize(new Dimension(100, 30));
					textTemp.setPreferredSize(new Dimension(100, 30));
					panel.add(textTemp, cText);
					textTemp.setText(vars[j]);
					PlainDocument doc1 = (PlainDocument) textTemp.getDocument();
					if (isBool((String) theList2.getSelectedItem()))
						doc1.setDocumentFilter(new BoolFilter());
					else
						doc1.setDocumentFilter(new IntFilter());
					contextMenu1.add(textTemp);
				}
			}

		}
		theList2.addActionListener(action1);
		panel.add(bt, cBtAdd);
		reglageReset();
		thePreview.setText(showInput());
	}

	public static boolean isBool(String selectedItemVar) {
		boolean a = false;
		for (int i = 0; i < GraphEditor.categories.size(); i++) {
			List<String> v1 = new ArrayList<String>();
			v1 = GraphEditor.categories.get(i).nomVariables();
			List<String> v2 = new ArrayList<String>();
			v2 = GraphEditor.categories.get(i).typeVariables();
			if (selectedItemVar == null) {
				return a;
			} else {
				for (int j = 0; j < v1.size(); j++) {
					if (v1.get(j).contains(selectedItemVar)) {
						if (v2.get(j).toLowerCase().contains("bool")) {
							return true;
						} else
							return false;
					}
				}
			}
		}
		return a;
	}

	public static int[] getMinMax(String selectedItemVar) {
		int[] x = new int[2];
		for (int i = 0; i < GraphEditor.categories.size(); i++) {
			List<String> v1 = new ArrayList<String>();
			v1 = GraphEditor.categories.get(i).nomVariables();
			List<Variables> v2 = new ArrayList<Variables>();
			v2 = GraphEditor.categories.get(i).getVariableses();
			if (selectedItemVar == null) {
				x[0] = 0;
				x[1] = 50;
				return (x);
			} else {
				for (int j = 0; j < v1.size(); j++) {
					if (v1.get(j).contains(selectedItemVar)) {
						x[0] = v2.get(j).getMin();
						x[1] = v2.get(j).getMax();
						return (x);
					}
				}
			}
		}
		return null;
	}

	public int previousLine(int line) {
		Component[] c = panel.getComponents();
		int x = 0;
		int temp = 0;
		for (int i = 0; i < c.length; i++) {
			if (layout.getConstraints(c[i]).gridy < line && layout.getConstraints(c[i]).gridy >= 0) {
				temp = layout.getConstraints(c[i]).gridy;
				if (temp > x)
					x = temp;
			}
		}
		return x;
	}

	public int firstLine() {
		Component[] c = panel.getComponents();
		int x = 99;
		for (int i = 0; i < c.length; i++) {
			if (layout.getConstraints(c[i]).gridy < x && layout.getConstraints(c[i]).gridy >= 0) {
				x = layout.getConstraints(c[i]).gridy;
			}
		}
		return x;
	}
	public int lastLine() {
		Component[] c = panel.getComponents();
		int x = 0;
		for (int i = 0; i < c.length; i++) {
			if (layout.getConstraints(c[i]).gridy > x && layout.getConstraints(c[i]).gridy >= 0) {
				x = layout.getConstraints(c[i]).gridy;
			}
		}
		return x;
	}

	public void reglageColumn(int row, int column) {
		cText.gridx = column + 1;
		cText.gridy = row;

		cOp.gridx = column + 3;
		cOp.gridy = row;

		cBtAdd.gridx = column + 2;
		cBtAdd.gridy = row;

		clabelOr.gridx = column;
		clabelOr.gridy = row;
	}

	public void reglageLastColumn(int row, int column) {
		cText.gridx = column + 1;
		cText.gridy = row;

		cBtAdd.gridx = column + 2;
		cBtAdd.gridy = row;

		clabelOr.gridx = column;
		clabelOr.gridy = row;

		cOp.gridx = column + 3;
		cOp.gridy = row;
	}

	public void reglageReset() {
		cText.gridx = 4;
		cText.gridy = rowNumber;

		cOp.gridx = 6;
		cOp.gridy = rowNumber;

		cBtAdd.gridx = 5;
		cBtAdd.gridy = rowNumber;

		clabelOr.gridx = 5;
		clabelOr.gridy = rowNumber;

	}

	public void reglageLayout1(int i) {
		cParClose.gridx = newLineOpPosition + 5;
		cParClose.gridy = i - 1;

		cParOpen.gridx = newLineOpPosition - 1;
		cParOpen.gridy = i - 1;
	}

	public void reglageLayout(int i) {
		/*
		 * c.gridwidth = 2; //2 columns wide c.weighty = 1.0; //request any
		 * extra vertical space c.anchor = GridBagConstraints.PAGE_END; //bottom
		 * of space c.insets = new Insets(10,0,0,0); //top padding
		 */
		cCheck.fill = GridBagConstraints.HORIZONTAL;
		cCheck.weightx = 0.5; // normal weight
		cCheck.gridx = 0; // column number
		cCheck.gridy = i; // line number

		cList1.fill = GridBagConstraints.HORIZONTAL;
		cList1.weightx = 0.5;
		cList1.gridx = 1;
		cList1.gridy = i;

		cList2.fill = GridBagConstraints.HORIZONTAL;
		cList2.weightx = 0.5;
		cList2.gridx = 2;
		cList2.gridy = i;

		cOp.fill = GridBagConstraints.HORIZONTAL;
		cOp.weightx = 0.0;
		cOp.gridx = newLineOpPosition;
		cOp.gridy = i - 1;

		cParClose.fill = GridBagConstraints.HORIZONTAL;
		cParClose.weightx = 0.0;
		cParClose.gridx = newLineOpPosition + 5;
		cParClose.gridy = i;

		cParOpen.fill = GridBagConstraints.HORIZONTAL;
		cParOpen.weightx = 0.0;
		cParOpen.gridx = newLineOpPosition - 1;
		cParOpen.gridy = i;

		cBtAdd.fill = GridBagConstraints.HORIZONTAL;
		cBtAdd.weightx = 0.0;
		cBtAdd.gridx = 5;
		// cBtAdd.insets = new Insets(0,20,0,0); // translation a droite
		cBtAdd.gridy = i;

		cText.fill = GridBagConstraints.HORIZONTAL;
		cText.weightx = 0.5;
		cText.gridx = 4;
		// cText.insets = new Insets(0,20,0,0); // translation a droite
		cText.gridy = i;

		clabelEgal.fill = GridBagConstraints.HORIZONTAL;
		clabelEgal.weightx = 0.0;
		clabelEgal.gridx = 3;
		clabelEgal.insets = new Insets(0, 10, 0, 10); // translation a droite
		clabelEgal.gridy = i;

		clabelOr.fill = GridBagConstraints.HORIZONTAL;
		clabelOr.weightx = 0.0;
		clabelOr.gridx = 6;
		clabelOr.gridy = i;

	}
	/*
	 * public static void main(String[] args) {
	 * 
	 * // CategorieDao categorieDao = new CategorieDao(); // // for (int i=0;
	 * i<inputTransition.dbList.length;i++){ // Categorie categorie = new
	 * Categorie(); // categorie=ReadExcel.bdData.get(i); // //
	 * System.out.println(categorie); // } final InputTransition inputTransition
	 * = new InputTransition(); SwingUtilities.invokeLater(new Runnable() {
	 * 
	 * @Override public void run() { inputTransition.pack();
	 * inputTransition.setVisible(true); } }); }
	 */

	public static List<Variables> getVariablesList(Object object) {
		List<Variables> variables = new ArrayList<Variables>();
		int k = -1;
		for (int i = 0; i < GraphEditor.categories.size(); i++) {
			if (GraphEditor.categories.get(i).getNom().equals((String) object)) {
				k = i;
				break;
			}
		}
		if (k == -1) {
			return null;
		} else {
			variables = GraphEditor.categories.get(k).getVariableses();
			return variables;
		}

	}
	public String showInput() {
		Component[] c = panel.getComponents();
		int numberOfRows = 0;
		List<Integer> rows = new ArrayList<Integer>();
		String s = "";
		
		for (int i = 0; i < c.length; i++) {
			if (!rows.contains(layout.getConstraints(c[i]).gridy)) {
				numberOfRows++;
				rows.add(layout.getConstraints(c[i]).gridy);
			}
		}
		
		String[] arrayS= new String[numberOfRows*4];
		int position=2;
		for (int j = 0; j <= rowNumber; j++) {
			for (int k = 28; k <= 40; k++) {
				for (int i = 0; i < c.length; i++) {
					if (layout.getConstraints(c[i]).gridx == k && layout.getConstraints(c[i]).gridy == j) {
						if (c[i] instanceof JTextField) {
							if (((JTextField) c[i]).getText().contains("(")){
								arrayS[position-2] = ((JTextField) c[i]).getText();}
							else if (((JTextField) c[i]).getText().contains(")")) {
								if(j==lastLine()){
								arrayS[position] = ((JTextField) c[i]).getText();}
								else 
									arrayS[position-4] = ((JTextField) c[i]).getText();
								}
						}
						if (c[i] instanceof JScrollPane) {
							JScrollPane targetscroll = (JScrollPane) c[i];
							JViewport targetview = targetscroll.getViewport();
							JComboBox<String> target = (JComboBox<String>) targetview.getComponents()[0];
							if (target.getSelectedItem() == "OR" || target.getSelectedItem() == "AND"){
								arrayS[position+1] = " " + target.getSelectedItem() + " ";
							position+=4;}
						}

					}
				}
			}
		}

		int start =1;
		for (int i = 1 ; i<position+1;i=i+4){
			arrayS[i]=start+"";
			start++;
		}
		for (int i = 0 ; i<position+1;i++){
			if(arrayS[i]!=null){
			s+=arrayS[i]+" ";}
		}
		return s;
	}
	
}